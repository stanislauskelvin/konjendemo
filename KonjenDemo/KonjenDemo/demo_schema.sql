DROP TABLE IF EXISTS citizen;

CREATE TABLE citizen (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT,
	gender TEXT NOT NULL,
	email TEXT,
	main_phone TEXT,
	alt_phone TEXT,
	age INT,
	street_number INT,
	street_name TEXT,
	city TEXT,
	postal_code TEXT,
	province TEXT,
	occupation TEXT,
	occ_field TEXT,
	occ_location TEXT,
	passport_no TEXT,
	passport_exp TEXT,
	last_update TEXT  
);

INSERT INTO citizen VALUES
(NULL, "Sandi", "Simajuntak", "Male", "sandi332@hotmail.com", "+12268457789", NULL, 55, 3, "East Centennial Ave.", "Cambridge", "R3L2K4", "Ontario", "working", "Retail", "Cambridge", "A2875394", "2020-12-12", "2010-02-12"),
(NULL, "Agnes", "Halim", "Female", "agneshalimiii@yahoo.com", "+16478894432", NULL, 32, 22, "East West St.", "Oakville", "M2K2Y5", "Ontario", "student", "Business", "Oakville", "A3235846", "2022-05-03", "2018-05-03"),
(NULL, "Reynaldi", "Rochefort", "Male", "reyrochefort312@gmail.com", "+16045972458", NULL, 29, 3342, "Yorksville Cres.", "Iqaluit", "Z2R4T5", "Nuvavut", "Student", "Actuarial", "Iqaluit", "B5286791", "2021-09-05", "2015-05-13"),
(NULL, "Batman", "Supratman", "Male", "sigantengdarigoabatu@yahoo.com", "+14033235147", "+15879876543", 53, 225, "Varmoor Rd NW", "Saskatoon", "R4R3C4", "Saskatchewan", "working", "Finance", "Saskatoon", "A2598002", "2025-11-07", "2019-01-01"),
(NULL, "Robin", "Supratman", "Male", "anakdarigoabatu@yahoo.com", "+14032322323", "+15878901234", 23, 225, "Varmoor Rd NW", "Calgary", "R4R3C4", "Saskatchewan", "student", "Finance", "Saskatoon", "B2503876", "2019-08-22", "2018-04-25"),
(NULL, "Ratna", "Siahaan", "Female", "ratnagalih@gmail.com", "+12235585245", NULL, 20, 8552, "Indelnero St W", "Regina", "I8C2P2", "Saskatchewan", "unemployed", NULL, NULL, "B2057360", "2021-10-08", "2017-12-12"),
(NULL, "Tania", "Hidayat", "Female", "hidayatania@outlook.com", "+15162287795", NULL, 22, 5521, "Rainwater Dr.", "Oshawa", "K2K3P0", "Ontario", "working", "food", "Oshawa", "A2567880", "2023-01-22", "2016-10-05"),
(NULL, "Reza", "Adhitya", "Male", "rezadit@hotmail.com", "+15289764452", NULL, 55, 5, "Dragon Crest Cres.", "Brandon", "X0L3I0", "Manitoba", "student", "Mathematics", "Manitoba", "B5786432", "2019-03-15", "2107-03-28"),
(NULL, "Rafik", "Azhari", "Male", "rafikii@hotmail.com", "+15587645289", NULL, 38, 85, "Carver Den St", "Winnipeg", "Y2K4P0", "Manitoba", "working", "Lawyer", "Manitoba", "A2580034", "2022-05-18", "2018-05-22"),
(NULL, "Lilian", "Kusuma", "Female", "lilianhalim@yahoo.com", "+13235589438", NULL, 76, 228, "Robin Deker Blvd.", "Ottawa", "P9Y3A4", "Ontario", "retired", NULL, NULL, "A2586731", "2022-11-11", "2018-08-17"),
(NULL, "Marika", "Riyadi", "Female", "mariyadika@gmail.com", "+15568792346", NULL, 42, 1104, "Rue St. Catherine", "Westmount", "J3L5Y4", "Quebec", "student", "Computer", "Pointe-Claire", "A5003214", "2026-10-05", "2109-04-02"),
(NULL, "Reginald", "Simajuntak", "Male", "regisimajuntak@live.com", "+12586653289",  NULL, 28, 2807, "Rue d'Orion", "Levis", "K9Y2U6", "Quebec", "working", "Engineer", "Quebec City", "B2304830", "2023-07-16", "2018-02-15");