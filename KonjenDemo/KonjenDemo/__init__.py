"""
The flask application package.
"""

from flask import Flask
from . import db
import pip
import subprocess
import sys
import os


#reqs = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze'])
#installed_packages = [r.decode().split('==')[0] for r in reqs.split()]

#if 'twilio' not in installed_packages:
#    if hasattr(pip, 'main'):
#        pip.main(['install', 'twilio'])
#    else:
#        pip._internal.main(['install', 'twilio'])

#from pusher import Pusher
#pusher = Pusher(
#    app_id='PUSHER_APP_ID',
#    key='PUSHER_APP_KEY',
#    secret='PUSHER_APP_SECRET',
#    cluster='PUSHER_APP_CLUSTER',
#    ssl=True)





app = Flask(__name__)
app.config.from_mapping(
       SECRET_KEY='Kelvin',
       DATABASE = os.path.join(app.instance_path, 'KonjenDemo.sqlite')
    )

# ensure the instance folder exixsts
try:
    os.makedirs(app.instance_path)
except OSError:
    pass


with app.app_context():
    db.init_db()

server = Flask("server")
server.config.from_mapping(
        SECRET_KEY='Stanislaus',
        DATABASE = os.path.join(app.instance_path, 'KonjenDemo.sqlite')
    )

from . import dashboard

with server.app_context():
    dashboard.create(server)



db.init_app(app)

global ACCOUNT_SID
global AUTH_TOKEN
global SOURCE_NUMBER

ACCOUNT_SID = 'ACd80df773981c483d770d7662aae97ab5'
AUTH_TOKEN = 'f696ad828ac7aec577f843f171c59642'
SOURCE_NUMBER = '+12268873657'

from . import massMessage, table
app.register_blueprint(massMessage.messageBP)
app.register_blueprint(table.tableBP)

import KonjenDemo.views
