import functools

from KonjenDemo import ACCOUNT_SID, AUTH_TOKEN, SOURCE_NUMBER
from KonjenDemo.db import get_expired
from flask import (Blueprint, flash, g, redirect, render_template, request, url_for)
from datetime import datetime
from twilio.rest import Client
import twilio

messageBP = Blueprint('massMessage', __name__, url_prefix='/massMessage')

@messageBP.route('/send', methods=('GET','POST'))
def send():
    if request.method == 'POST':

        test = "this is a test " + ACCOUNT_SID

        recipients = request.form['recipients']
        numbers = [x.strip() for x in recipients.split(';')]
        message = request.form['message']
        failed = []

        client = Client(ACCOUNT_SID, AUTH_TOKEN)

        for number in numbers:
            try:
                createMessage = client.messages.create(
                    body = message,
                    from_ = SOURCE_NUMBER,
                    to = number
                    )

                if createMessage.status != 'sent':
                    failed.append(number + ", " + createMessage.status)

            except twilio.base.exceptions.TwilioRestException:
                failed.append(number)

        flash("Failed to send message to: \n" + ", ".join(failed))

    return render_template(
        'massMessage.html', 
        notification=get_expired(),
        title='Send Message to multiple people',
        year=datetime.now().year,)

