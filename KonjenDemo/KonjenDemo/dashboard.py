
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy as np
from KonjenDemo.db import get_db
from dash.dependencies import Input, Output

db = None


def create(server):
    np.random.seed(101)
    random_x = np.random.randint(1,21,20)
    random_y = np.random.randint(1,21,20)
    colors = {
        'background': '#111111',
        'text': '#7FDBFF'
    }

    import dash

    db = get_db()
    
    population = db.execute('SELECT province, COUNT(*) AS population FROM citizen GROUP BY province').fetchall()

    PROVINCES = []
    for row in population:
        PROVINCES.append(row['province'])

    data_male = db.execute('SELECT SUM(CASE WHEN age < 12 THEN 1 ELSE 0 END) AS [Under 12], \
                                SUM(CASE WHEN age BETWEEN 12 AND 18 THEN 1 ELSE 0 END) as [12-18], \
                                SUM(CASE WHEN age BETWEEN 19 AND 30 THEN 1 ELSE 0 END) as [19-30], \
                                SUM(CASE WHEN age BETWEEN 31 AND 40 THEN 1 ELSE 0 END) as [31-40], \
                                SUM(CASE WHEN age BETWEEN 41 AND 50 THEN 1 ELSE 0 END) as [41-50], \
                                SUM(CASE WHEN age BETWEEN 51 AND 60 THEN 1 ELSE 0 END) as [51-60], \
                                SUM(CASE WHEN age > 60 THEN 1 ELSE 0 END) as [Over 60] \
                                FROM citizen WHERE gender = "Male"').fetchone()



    data_female = db.execute('SELECT SUM(CASE WHEN ((age < 12) AND (gender = "Female")) THEN 1 ELSE 0 END) AS [Under 12], \
                                SUM(CASE WHEN ((age BETWEEN 12 AND 18) AND (gender = "Female")) THEN 1 ELSE 0 END) as [12-18], \
                                SUM(CASE WHEN ((age BETWEEN 19 AND 30) AND (gender = "Female")) THEN 1 ELSE 0 END) as [19-30], \
                                SUM(CASE WHEN ((age BETWEEN 31 AND 40) AND (gender = "Female")) THEN 1 ELSE 0 END) as [31-40], \
                                SUM(CASE WHEN ((age BETWEEN 41 AND 50) AND (gender = "Female")) THEN 1 ELSE 0 END) as [41-50], \
                                SUM(CASE WHEN ((age BETWEEN 51 AND 60) AND (gender = "Female")) THEN 1 ELSE 0 END) as [51-60], \
                                SUM(CASE WHEN ((age > 60) AND (gender = "Female")) THEN 1 ELSE 0 END) as [Over 60] \
                                FROM citizen').fetchone()
    

    #PROVINCES = db.execute('SELECT DISTINCT province FROM citizen').fetchall()

    dash = dash.Dash(__name__, server=server)
    dash.layout = html.Div(children=[
        html.Div([
            dcc.Dropdown(
                id='province',
                options=[{'label': i, 'value': i} for i in PROVINCES],
                multi=True
                )
            ]),
        

        html.Div([
            dcc.Graph(
                id='province-cityPie',
                
            )
        ], style={'width':'50%', 'display': 'inline-block'}),

        html.Div([
            dcc.Graph(
                id='genderStackedBar',
            )
        ], style={'width':'50%', 'display': 'inline-block'})
    ])

    @dash.callback( Output('province-cityPie', 'figure'), [Input('province', 'value')])
    def update_pie(provinces):
        if provinces is None or provinces == []:
            provinces = PROVINCES
            prov_group = 'province'
        else:
            prov_group = 'city'

        db = get_db()

        
        condition = 'province in (' + ', '.join('"{}"'.format(p) for p in provinces) + ')'

        population = ''
        if prov_group == 'city':
            prov_group = ' WHERE ' + condition
            population = db.execute('SELECT city AS area, COUNT(*) AS population FROM citizen' + prov_group + ' GROUP BY area').fetchall()
        else:
            population = db.execute('SELECT province AS area, COUNT(*) AS population FROM citizen GROUP BY area').fetchall()

        return {
                    'data': [
                        go.Pie(
                            labels = [row['area'] for row in population],
                            values = [row['population'] for row in population],
                            hoverinfo='label+percent',
                            textinfo='value+percent',
                            )
                    ],
                    'layout': 
                        go.Layout(
                            title = 'Population distribution'
                            )
                }

    @dash.callback( Output('genderStackedBar', 'figure'), [Input ('province', 'value')])
    def update_gender_bar(provinces):
        if provinces is None or provinces == []:
            provinces = PROVINCES

        condition = 'province in (' + ', '.join('"{}"'.format(p) for p in provinces) + ')'

        db = get_db()


        data_male = db.execute('SELECT SUM(CASE WHEN age < 12 THEN 1 ELSE 0 END) AS [Under 12], \
                                SUM(CASE WHEN age BETWEEN 12 AND 18 THEN 1 ELSE 0 END) as [12-18], \
                                SUM(CASE WHEN age BETWEEN 19 AND 30 THEN 1 ELSE 0 END) as [19-30], \
                                SUM(CASE WHEN age BETWEEN 31 AND 40 THEN 1 ELSE 0 END) as [31-40], \
                                SUM(CASE WHEN age BETWEEN 41 AND 50 THEN 1 ELSE 0 END) as [41-50], \
                                SUM(CASE WHEN age BETWEEN 51 AND 60 THEN 1 ELSE 0 END) as [51-60], \
                                SUM(CASE WHEN age > 60 THEN 1 ELSE 0 END) as [Over 60] \
                                FROM citizen WHERE gender = "Male"' + (' AND ' + condition) if condition != '' else '').fetchone()

        data_female = db.execute('SELECT SUM(CASE WHEN age < 12 THEN 1 ELSE 0 END) AS [Under 12], \
                                SUM(CASE WHEN age BETWEEN 12 AND 18 THEN 1 ELSE 0 END) as [12-18], \
                                SUM(CASE WHEN age BETWEEN 19 AND 30 THEN 1 ELSE 0 END) as [19-30], \
                                SUM(CASE WHEN age BETWEEN 31 AND 40 THEN 1 ELSE 0 END) as [31-40], \
                                SUM(CASE WHEN age BETWEEN 41 AND 50 THEN 1 ELSE 0 END) as [41-50], \
                                SUM(CASE WHEN age BETWEEN 51 AND 60 THEN 1 ELSE 0 END) as [51-60], \
                                SUM(CASE WHEN age > 60 THEN 1 ELSE 0 END) as [Over 60] \
                                FROM citizen WHERE gender = "Female"' + (' AND ' + condition) if condition != '' else '').fetchone()

        #data_male = db.execute('SELECT SUM(CASE WHEN ((age < 12) AND (gender = "Male")) THEN 1 ELSE 0 END) AS [Under 12], \
        #                        SUM(CASE WHEN ((age BETWEEN 12 AND 18) AND (gender = "Male")) THEN 1 ELSE 0 END) as [12-18], \
        #                        SUM(CASE WHEN ((age BETWEEN 19 AND 30) AND (gender = "Male")) THEN 1 ELSE 0 END) as [19-30], \
        #                        SUM(CASE WHEN ((age BETWEEN 31 AND 40) AND (gender = "Male")) THEN 1 ELSE 0 END) as [31-40], \
        #                        SUM(CASE WHEN ((age BETWEEN 41 AND 50) AND (gender = "Male")) THEN 1 ELSE 0 END) as [41-50], \
        #                        SUM(CASE WHEN ((age BETWEEN 51 AND 60) AND (gender = "Male")) THEN 1 ELSE 0 END) as [51-60], \
        #                        SUM(CASE WHEN ((age > 60) AND (gender = "Male")) THEN 1 ELSE 0 END) as [Over 60] \
        #                        FROM citizen' + condition).fetchone()

        #data_female = db.execute('SELECT SUM(CASE WHEN ((age < 12) AND (gender = "Female")) THEN 1 ELSE 0 END) AS [Under 12], \
        #                        SUM(CASE WHEN ((age BETWEEN 12 AND 18) AND (gender = "Female")) THEN 1 ELSE 0 END) as [12-18], \
        #                        SUM(CASE WHEN ((age BETWEEN 19 AND 30) AND (gender = "Female")) THEN 1 ELSE 0 END) as [19-30], \
        #                        SUM(CASE WHEN ((age BETWEEN 31 AND 40) AND (gender = "Female")) THEN 1 ELSE 0 END) as [31-40], \
        #                        SUM(CASE WHEN ((age BETWEEN 41 AND 50) AND (gender = "Female")) THEN 1 ELSE 0 END) as [41-50], \
        #                        SUM(CASE WHEN ((age BETWEEN 51 AND 60) AND (gender = "Female")) THEN 1 ELSE 0 END) as [51-60], \
        #                        SUM(CASE WHEN ((age > 60) AND (gender = "Female")) THEN 1 ELSE 0 END) as [Over 60] \
        #                        FROM citizen' + condition).fetchone()
        



        return {
                    'data': [
                        go.Bar(
                            x = data_male.keys(),
                            y = [data_male[i] for i in data_male.keys()],
                            name = 'Male'
                            ),
                        go.Bar(
                            x = data_female.keys(),
                            y = [data_female[i] for i in data_female.keys()], 
                            name = 'Female'
                            )
                    ],
                    'layout': 
                        go.Layout(
                            title = 'Gender and Age distribution',
                            barmode = 'stack')
                
                }
