from flask import (Blueprint, flash, g, redirect, render_template, request, url_for)
from KonjenDemo.db import get_db, get_expired
from datetime import datetime
from flask import render_template

tableBP = Blueprint('table', __name__, url_prefix='/table')

#@tableBP.route('/data/<personid>')
#def single(personid):
#    db = get_db()

#    province_list = [ row['province'] for row in db.execute('SELECT DISTINCT province FROM citizen').fetchall() ]
#    gender_list = [ row['gender'] for row in db.execute('SELECT DISTINCT gender FROM citizen').fetchall() ]

#    row = db.execute('SELECT * from citizen WHERE id = {};'.format(personid)).fetchone()

#    data = []
#    record = {}
#    main = {}
#    detail = {}
#    main['Name'] = row['last_name'] + ", " + row['first_name']
#    main['Gender'] = 'M' if row['gender'] == 'Male' else 'F'
#    detail['E-mail'] = row['email']
#    main['Phone Number'] = row['main_phone']
#    main['Age'] = row['age']
#    main['Address'] = str(row['street_number']) + ' ' + row['street_name'] + ', ' + row['city']
#    detail['Postal Code'] = row['postal_code']
#    main['Province'] = row['province']
#    detail['Occupation'] = row['occupation']
#    detail['Field'] = row['occ_field']
#    detail['Location'] = row['occ_location']
#    detail['Passport'] = row['passport_no']
#    detail['Passport Expiry'] = row['passport_exp']
#    detail['Last Updated'] = row['last_update']

#    record['main'] = main
#    record['detail'] = detail
#    data.append(record)

#    return render_template(
#        'data.html', 
#        data=data,
#        province_list=province_list,
#        gender_list=gender_list,
#        notification=get_expired(),
#        title='Data Table',
#        year=datetime.now().year,
#        message='Your application description page.')


@tableBP.route('/data', methods=('GET', 'POST'))
def data():

    personid = request.args.get('personid')

    db = get_db()

    province_list = [ row['province'] for row in db.execute('SELECT DISTINCT province FROM citizen').fetchall() ]
    gender_list = [ row['gender'] for row in db.execute('SELECT DISTINCT gender FROM citizen').fetchall() ]

    

    condition = ''
    genders = ''
    provinces = ''

    check_exp = ''
    check_update = ''

    if request.method == 'POST':

        genders = request.form.getlist('gender_select')
        provinces = request.form.getlist('province_select')

        check_exp = request.form.get('passport_exp_check')
        check_update = request.form.get('last_update_check')

        

        conditions = []
        if provinces != None and len(provinces) != 0:
            conditions.append('province in (' + ', '.join('"{}"'.format(p) for p in provinces) + ')')

        if genders != None and len(genders) != 0:
            conditions.append('gender in (' + ', '.join('"{}"'.format(g) for g in genders) + ')')

        if check_exp == '1':
            conditions.append('passport_exp < DATETIME("now", "+1 year")')

        if check_update == '1':
            conditions.append('last_update < DATETIME("now", "-1 year")')

        for c in conditions:
            print(c)
        if len(conditions) != 0:
            condition = 'WHERE ' + ' AND '.join(conditions)

    else:
        print (personid)
        if personid:
            condition = 'WHERE id = ' + personid + ';'


    #print(condition)
    response = db.execute('SELECT * FROM citizen ' + condition).fetchall()

    data = []
    for row in response:
        record = {}
        main = {}
        detail = {}
        main['Name'] = row['last_name'] + ", " + row['first_name']
        main['Gender'] = 'M' if row['gender'] == 'Male' else 'F'
        detail['E-mail'] = row['email']
        main['Phone Number'] = row['main_phone']
        main['Age'] = row['age']
        main['Address'] = str(row['street_number']) + ' ' + row['street_name'] + ', ' + row['city']
        detail['Postal Code'] = row['postal_code']
        main['Province'] = row['province']
        detail['Occupation'] = row['occupation']
        detail['Field'] = row['occ_field']
        detail['Location'] = row['occ_location']
        detail['Passport'] = row['passport_no']
        detail['Passport Expiry'] = row['passport_exp']
        detail['Last Updated'] = row['last_update']

        record['main'] = main
        record['detail'] = detail
        data.append(record)



    return render_template(
        'data.html', 
        data=data,
        province_list=province_list,
        gender_list=gender_list,
        genders=genders,
        check_exp=check_exp,
        check_update=check_update,
        provinces=provinces,
        notification=get_expired(),
        title='Data Table',
        year=datetime.now().year,
        message='Your application description page.')
