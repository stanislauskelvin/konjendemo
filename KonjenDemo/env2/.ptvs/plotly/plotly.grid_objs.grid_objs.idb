�}q (X   membersq}q(X   _jsonq}q(X   kindqX   dataqX   valueq}qX   typeq	NsuX   pdq
}q(hhh}qh	NsuX
   exceptionsq}q(hX	   modulerefqhX   plotly.exceptionsqX    q�quX   absolute_importq}q(hhh}qh	X
   __future__qX   _Featureq�qsuX   Gridq}q(hh	h}q(X   mroq]q(X   plotly.grid_objs.grid_objsqh�qX   _collections_abcq X   MutableSequenceq!�q"h X   Sequenceq#�q$h X
   Reversibleq%�q&h X
   Collectionq'�q(h X   Sizedq)�q*h X   Iterableq+�q,h X	   Containerq-�q.eX   basesq/]q0h"ah}q1(X   __repr__q2}q3(hX   functionq4h}q5(X   docq6NX   builtinq7�X   locationq8K�K	�q9X	   overloadsq:]q;}q<(X   argsq=}q>(X   nameq?X   selfq@h	hu�qAX   ret_typeqBNuauuX   __init__qC}qD(hh4h}qE(h6X�  
        Initialize a grid with an iterable of `plotly.grid_objs.Column`
        objects or a json/dict describing a grid. See second usage example
        below for the necessary structure of the dict.

        :param (str|bool) fid: should not be accessible to users. Default
            is 'None' but if a grid is retrieved via `py.get_grid()` then the
            retrieved grid response will contain the fid which will be
            necessary to set `self.id` and `self._columns.id` below.

        Example from iterable of columns:
        ```
        column_1 = Column([1, 2, 3], 'time')
        column_2 = Column([4, 2, 5], 'voltage')
        grid = Grid([column_1, column_2])
        ```
        Example from json grid
        ```
        grid_json = {
            'cols': {
                'time': {'data': [1, 2, 3], 'order': 0, 'uid': '4cd7fc'},
                'voltage': {'data': [4, 2, 5], 'order': 1, 'uid': u'2744be'}
            }
        }
        grid = Grid(grid_json)
        ```qFh7�h8K|K	�qGh:]qH}qI(h=}qJ(h?h@h	hu}qK(h?X   columns_or_jsonqLh	X   builtinsqMX   dictqN�qOu}qP(h?X   fidqQh	]qR(hMX   NoneTypeqS�qThMX   strqU�qVeX   default_valueqWX   NoneqXu�qYhBNuauuX   __getitem__qZ}q[(hh4h}q\(h6Nh7�h8K�K	�q]h:]q^}q_(h=}q`(h?h@h	hu}qa(h?X   indexqbh	hMX   intqc�qdu�qehBhX   Columnqf�qguauuX   insertqh}qi(hh4h}qj(h6Nh7�h8K�K	�qkh:]ql}qm(h=}qn(h?h@h	hu}qo(h?hbh	Nu}qp(h?X   columnqqh	Nu�qrhBNuauuX   get_column_referenceqs}qt(hh4h}qu(h6X�   
        Returns the column reference of given column in the grid by its name.

        Raises an error if the column name is not in the grid. Otherwise,
        returns the fid:uid pair, which may be the empty string.qvh7�h8MK	�qwh:]qx}qy(h=}qz(h?h@h	hu}q{(h?X   column_nameq|h	Nu�q}hB]q~(hThVeuauuX   _to_plotly_grid_jsonq}q�(hh4h}q�(h6Nh7�h8M K	�q�h:]q�}q�(h=}q�(h?h@h	hu�q�hBhOuauuX   __delitem__q�}q�(hh4h}q�(h6Nh7�h8K�K	�q�h:]q�}q�(h=}q�(h?h@h	hu}q�(h?hbh	Nu�q�hBNuauuX   __setitem__q�}q�(hh4h}q�(h6Nh7�h8K�K	�q�h:]q�}q�(h=}q�(h?h@h	hu}q�(h?hbh	Nu}q�(h?hqh	Nu�q�hBNuauuX   __len__q�}q�(hh4h}q�(h6Nh7�h8K�K	�q�h:]q�}q�(h=}q�(h?h@h	hu�q�hBhduauuX
   get_columnq�}q�(hh4h}q�(h6X{    Return the first column with name `column_name`.
        If no column with `column_name` exists in this grid, return None.q�h7�h8M	K	�q�h:]q�}q�(h=}q�(h?h@h	hu}q�(h?h|h	Nu�q�hBhguauuX   _validate_insertionq�}q�(hh4h}q�(h6XB   
        Raise an error if we're gonna add a duplicate column nameq�h7�h8K�K	�q�h:]q�}q�(h=}q�(h?h@h	hu}q�(h?hqh	Nu�q�hBNuauuX   _columnsq�}q�(hhh}q�h	hMX   listq��q�suX   idq�}q�(hX   multipleq�h}q�h}q�(hhh}q�h	hTsu}q�(hhh}q�h	hVsu}q�(hhh}q�h	hVsu�q�suuh6X�  
    Grid is Plotly's Python representation of Plotly Grids.
    Plotly Grids are tabular data made up of columns. They can be
    uploaded, appended to, and can source the data for Plotly
    graphs.

    A plotly.grid_objs.Grid object is essentially a list.

    Usage example 1: Upload a set of columns as a grid to Plotly
    ```
    from plotly.grid_objs import Grid, Column
    import plotly.plotly as py
    column_1 = Column([1, 2, 3], 'time')
    column_2 = Column([4, 2, 5], 'voltage')
    grid = Grid([column_1, column_2])
    py.grid_ops.upload(grid, 'time vs voltage')
    ```

    Usage example 2: Make a graph based with data that is sourced
                     from a newly uploaded Plotly columns
    ```
    import plotly.plotly as py
    from plotly.grid_objs import Grid, Column
    from plotly.graph_objs import Scatter
    # Upload a grid
    column_1 = Column([1, 2, 3], 'time')
    column_2 = Column([4, 2, 5], 'voltage')
    grid = Grid([column_1, column_2])
    py.grid_ops.upload(grid, 'time vs voltage')

    # Build a Plotly graph object sourced from the
    # grid's columns
    trace = Scatter(xsrc=grid[0], ysrc=grid[1])
    py.plot([trace], filename='graph from grid')
    ```q�h7�h8KWK�q�uuX   MutableSequenceq�}q�(hX   typerefq�h]q�h"auX   __all__q�}q�(hhh}q�h	hTsuX   optional_importsq�}q�(hhhX   plotly.optional_importsq�h�q�uhf}q�(hh	h}q�(h]q�(hghMX   objectqֆq�eh/]q�h�ah}q�(X   __str__q�}q�(hh4h}q�(h6Nh7�h8KFK	�q�h:]q�}q�(h=}q�(h?h@h	hgu�q�hBhVuauuhC}q�(hh4h}q�(h6X�   
        Initialize a Plotly column with `data` and `name`.
        `data` is an array of strings, numbers, or dates.
        `name` is the name of the column as it will apppear
               in the Plotly grid. Names must be unique to a grid.q�h7�h8K7K	�q�h:]q�}q�(h=}q�(h?h@h	hgu}q�(h?X   dataq�h	Nu}q�(h?X   nameq�h	Nu�q�hBNuauuh2}q�(hh4h}q�(h6Nh7�h8KPK	�q�h:]q�}q�(h=}q�(h?h@h	hgu�q�hBhVuauuX   to_plotly_jsonq�}q�(hh4h}q�(h6Nh7�h8KSK	�q�h:]q�}q�(h=}q�(h?h@h	hgu�q�hBhOuauuX   dataq�}q�(hhh}q�h	NsuX   nameq }r  (hhh}r  h	Nsuh�}r  (hhh}r  h	hVsuuh6X�  
    Columns make up Plotly Grids and can be the source of
    data for Plotly Graphs.
    They have a name and an array of data.
    They can be uploaded to Plotly with the `plotly.plotly.grid_ops`
    class.

    Usage example 1: Upload a set of columns as a grid to Plotly
    ```
    from plotly.grid_objs import Grid, Column
    import plotly.plotly as py
    column_1 = Column([1, 2, 3], 'time')
    column_2 = Column([4, 2, 5], 'voltage')
    grid = Grid([column_1, column_2])
    py.grid_ops.upload(grid, 'time vs voltage')
    ```

    Usage example 2: Make a graph based with data that is sourced
                     from a newly uploaded Plotly columns
    ```
    import plotly.plotly as py
    from plotly.grid_objs import Grid, Column
    from plotly.graph_objs import Scatter
    # Upload a grid
    column_1 = Column([1, 2, 3], 'time')
    column_2 = Column([4, 2, 5], 'voltage')
    grid = Grid([column_1, column_2])
    py.grid_ops.upload(grid, 'time vs voltage')

    # Build a Plotly graph object sourced from the
    # grid's columns
    trace = Scatter(xsrc=grid[0], ysrc=grid[1])
    py.plot([trace], filename='graph from grid')
    ```r  h7�h8KK�r  uuX   utilsr  }r  (hhhX   plotly.utilsr	  h�r
  uuh6X   
grid_objs
=========r  X   childrenr  ]r  X   filenamer  Xk   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\plotly\grid_objs\grid_objs.pyr  u.