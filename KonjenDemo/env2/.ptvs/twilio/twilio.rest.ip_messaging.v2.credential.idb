�}q (X   membersq}q(X   CredentialListq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X&   twilio.rest.ip_messaging.v2.credentialqX   CredentialListq�qX   twilio.base.list_resourceqX   ListResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docqX�   
        Initialize the CredentialList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.chat.v2.credential.CredentialList
        :rtype: twilio.rest.chat.v2.credential.CredentialListqX   builtinq�X   locationqKK	�qX	   overloadsq ]q!}q"(X   argsq#}q$(X   nameq%X   selfq&hhu}q'(h%X   versionq(hX   twilio.rest.ip_messaging.v2q)X   V2q*�q+u�q,X   ret_typeq-NuauuX   streamq.}q/(hhh}q0(hX�  
        Streams CredentialInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v2.credential.CredentialInstance]q1h�hK#K	�q2h ]q3}q4(h#}q5(h%h&hhu}q6(h%X   limitq7hhX   NoneTypeq8�q9X   default_valueq:X   Noneq;u}q<(h%X	   page_sizeq=hh9h:h;u�q>h-hX	   generatorq?�q@uauuX   listqA}qB(hhh}qC(hXW  
        Lists CredentialInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v2.credential.CredentialInstance]qDh�hK:K	�qEh ]qF}qG(h#}qH(h%h&hhu}qI(h%h7hh9h:h;u}qJ(h%h=hh9h:h;u�qKh-hX   listqL]qMNa�qNuauuX   getqO}qP(hhh}qQ(hX�   
        Constructs a CredentialContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.credential.CredentialContext
        :rtype: twilio.rest.chat.v2.credential.CredentialContextqRh�hK�K	�qSh ]qT}qU(h#}qV(h%h&hhu}qW(h%X   sidqXhNu�qYh-hX   CredentialContextqZ�q[uauuX   __repr__q\}q](hhh}q^(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq_h�hK�K	�q`h ]qa}qb(h#}qc(h%h&hhu�qdh-hX   strqe�qfuauuX   pageqg}qh(hhh}qi(hX�  
        Retrieve a single page of CredentialInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialPageqjh�hKLK	�qkh ]ql}qm(h#(}qn(h%h&hhu}qo(h%X
   page_tokenqphhh:X   values.unsetqqu}qr(h%X   page_numberqshhh:X   values.unsetqtu}qu(h%h=h]qv(hh9eh:X   values.unsetqwutqxh-hX   CredentialPageqy�qzuauuX   createq{}q|(hhh}q}(hXv  
        Create a new CredentialInstance

        :param CredentialInstance.PushService type: The type of push-notification service the credential is for
        :param unicode friendly_name: A string to describe the resource
        :param unicode certificate: [APN only] The URL encoded representation of the certificate
        :param unicode private_key: [APN only] The URL encoded representation of the private key
        :param bool sandbox: [APN only] Whether to send the credential to sandbox APNs
        :param unicode api_key: [GCM only] The API key for the project that was obtained from the Google Developer console for your GCM Service application credential
        :param unicode secret: [FCM only] The Server key of your project from Firebase console

        :returns: Newly created CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstanceq~h�hKtK	�qh ]q�}q�(h#(}q�(h%h&hhu}q�(h%X   typeq�hNu}q�(h%X   friendly_nameq�hhh:X   values.unsetq�u}q�(h%X   certificateq�hhh:X   values.unsetq�u}q�(h%X   private_keyq�hhh:X   values.unsetq�u}q�(h%X   sandboxq�hhh:X   values.unsetq�u}q�(h%X   api_keyq�hhh:X   values.unsetq�u}q�(h%X   secretq�hhh:X   values.unsetq�utq�h-hX   CredentialInstanceq��q�uauuX   get_pageq�}q�(hhh}q�(hX2  
        Retrieve a specific page of CredentialInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialPageq�h�hKcK	�q�h ]q�}q�(h#}q�(h%h&hhu}q�(h%X
   target_urlq�hNu�q�h-hzuauuX   __call__q�}q�(hhh}q�(hX�   
        Constructs a CredentialContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.credential.CredentialContext
        :rtype: twilio.rest.chat.v2.credential.CredentialContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hhu}q�(h%hXhNu�q�h-h[uauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hhfsuX   _versionq�}q�(hh�h}q�hh+suuhX    q�h�hKK�q�uuX   ListResourceq�}q�(hX   typerefq�h]q�hauX   InstanceResourceq�}q�(hh�h]q�X   twilio.base.instance_resourceq�X   InstanceResourceq͆q�auX   InstanceContextq�}q�(hh�h]q�X   twilio.base.instance_contextq�X   InstanceContextqӆq�auX   Pageq�}q�(hh�h]q�X   twilio.base.pageq�X   Pageqنq�auX   CredentialContextq�}q�(hhh}q�(h	]q�(h[h�heh]q�h�ah}q�(h}q�(hhh}q�(hX5  
        Initialize the CredentialContext

        :param Version version: Version that contains the resource
        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.credential.CredentialContext
        :rtype: twilio.rest.chat.v2.credential.CredentialContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hh[u}q�(h%h(hh+u}q�(h%hXhh9u�q�h-Nuauuh\}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hM,K	�q�h ]q�}q�(h#}q�(h%h&hh[u�q�h-hfuauuX   updateq�}q�(hhh}q�(hX�  
        Update the CredentialInstance

        :param unicode friendly_name: A string to describe the resource
        :param unicode certificate: [APN only] The URL encoded representation of the certificate
        :param unicode private_key: [APN only] The URL encoded representation of the private key
        :param bool sandbox: [APN only] Whether to send the credential to sandbox APNs
        :param unicode api_key: [GCM only] The API key for the project that was obtained from the Google Developer console for your GCM Service application credential
        :param unicode secret: [FCM only] The Server key of your project from Firebase console

        :returns: Updated CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstanceq�h�hMK	�q�h ]q�}q�(h#(}q�(h%h&hh[u}q�(h%h�hhh:X   values.unsetq�u}q�(h%h�hhh:X   values.unsetq�u}q�(h%h�hhh:X   values.unsetq u}r  (h%h�hhh:X   values.unsetr  u}r  (h%h�hhh:X   values.unsetr  u}r  (h%h�hhh:X   values.unsetr  utr  h-h�uauuX   deleter  }r	  (hhh}r
  (hXx   
        Deletes the CredentialInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr  h�hM#K	�r  h ]r  }r  (h#}r  (h%h&hh[u�r  h-NuauuX   fetchr  }r  (hhh}r  (hX�   
        Fetch a CredentialInstance

        :returns: Fetched CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstancer  h�hK�K	�r  h ]r  }r  (h#}r  (h%h&hh[u�r  h-h�uauuh�}r  (hh�h}r  h}r  (hh�h}r  hh�su�r  suh�}r  (hh�h}r   hhfsuh�}r!  (hh�h}r"  hh+suuhh�h�hK�K�r#  uuX   CredentialPager$  }r%  (hhh}r&  (h	]r'  (hzh�heh]r(  h�ah}r)  (h\}r*  (hhh}r+  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr,  h�hK�K	�r-  h ]r.  }r/  (h#}r0  (h%h&hhzu�r1  h-hfuauuX   get_instancer2  }r3  (hhh}r4  (hX�   
        Build an instance of CredentialInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.chat.v2.credential.CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstancer5  h�hK�K	�r6  h ]r7  }r8  (h#}r9  (h%h&hhzu}r:  (h%X   payloadr;  hNu�r<  h-h�uauuh}r=  (hhh}r>  (hX!  
        Initialize the CredentialPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.chat.v2.credential.CredentialPage
        :rtype: twilio.rest.chat.v2.credential.CredentialPager?  h�hK�K	�r@  h ]rA  }rB  (h#(}rC  (h%h&hhzu}rD  (h%h(hh+u}rE  (h%X   responserF  hNu}rG  (h%X   solutionrH  hh�utrI  h-Nuauuh�}rJ  (hh�h}rK  h(}rL  (hh�h}rM  hh�su}rN  (hh�h}rO  hh�su}rP  (hh�h}rQ  hh�su}rR  (hh�h}rS  hh�sutrT  suh�}rU  (hh�h}rV  hh+suX   _payloadrW  }rX  (hh�h}rY  h(}rZ  (hh�h}r[  hhX   floatr\  �r]  su}r^  (hh�h}r_  hhX   boolr`  �ra  su}rb  (hh�h}rc  hhX   intrd  �re  su}rf  (hh�h}rg  hh9sutrh  suX   _recordsri  }rj  (hh�h}rk  hNsuuhh�h�hK�K�rl  uuX   deserializerm  }rn  (hX	   modulerefro  hX   twilio.base.deserializerp  hÆrq  uX   valuesrr  }rs  (hjo  hX   twilio.base.valuesrt  hÆru  uX   CredentialInstancerv  }rw  (hhh}rx  (h	]ry  (h�h�heh]rz  h�ah}r{  (X   friendly_namer|  }r}  (hX   propertyr~  h}r  (hX`   
        :returns: The string that you assigned to describe the resource
        :rtype: unicoder�  hNhMvK	�r�  uuh}r�  (hhh}r�  (hX�   
        Initialize the CredentialInstance

        :returns: twilio.rest.chat.v2.credential.CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstancer�  h�hM?K	�r�  h ]r�  }r�  (h#(}r�  (h%h&hh�u}r�  (h%h(hh+u}r�  (h%j;  h]r�  (ja  h9j]  je  eu}r�  (h%hXhh9h:h;utr�  h-NuauuX   PushServicer�  }r�  (hh�h]r�  hX   PushServicer�  �r�  auX   sidr�  }r�  (hj~  h}r�  (hXY   
        :returns: The unique string that identifies the resource
        :rtype: unicoder�  hNhMfK	�r�  uuX   sandboxr�  }r�  (hj~  h}r�  (hXd   
        :returns: [APN only] Whether to send the credential to sandbox APNs
        :rtype: unicoder�  hNhM�K	�r�  uuX   urlr�  }r�  (hj~  h}r�  (hXV   
        :returns: The absolute URL of the Credential resource
        :rtype: unicoder�  hNhM�K	�r�  uuj  }r�  (hhh}r�  (hXx   
        Deletes the CredentialInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h-Nuauuh\}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h-hfuauuX   typer�  }r�  (hj~  h}r�  (hX}   
        :returns: The type of push-notification service the credential is for
        :rtype: CredentialInstance.PushServicer�  hNhM~K	�r�  uuj  }r�  (hhh}r�  (hX�   
        Fetch a CredentialInstance

        :returns: Fetched CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h-h�uauuX   date_createdr�  }r�  (hj~  h}r�  (hXk   
        :returns: The RFC 2822 date and time in GMT when the resource was created
        :rtype: datetimer�  hNhM�K	�r�  uuX   account_sidr�  }r�  (hj~  h}r�  (hX[   
        :returns: The SID of the Account that created the resource
        :rtype: unicoder�  hNhMnK	�r�  uuh�}r�  (hhh}r�  (hX�  
        Update the CredentialInstance

        :param unicode friendly_name: A string to describe the resource
        :param unicode certificate: [APN only] The URL encoded representation of the certificate
        :param unicode private_key: [APN only] The URL encoded representation of the private key
        :param bool sandbox: [APN only] Whether to send the credential to sandbox APNs
        :param unicode api_key: [GCM only] The API key for the project that was obtained from the Google Developer console for your GCM Service application credential
        :param unicode secret: [FCM only] The Server key of your project from Firebase console

        :returns: Updated CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#(}r�  (h%h&hh�u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  utr�  h-h�uauuX   _proxyr�  }r�  (hj~  h}r�  (hX(  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: CredentialContext for this CredentialInstance
        :rtype: twilio.rest.chat.v2.credential.CredentialContextr�  h]r�  (h9h[ehMYK	�r�  uuX   date_updatedr�  }r�  (hj~  h}r�  (hXp   
        :returns: The RFC 2822 date and time in GMT when the resource was last updated
        :rtype: datetimer�  hNhM�K	�r�  uuX   _propertiesr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh9su}r�  (hh�h}r�  hh[su�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hh+suuhh�h�hM7K�r�  uuuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r   X   filenamer  Xw   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\ip_messaging\v2\credential.pyr  u.