�}q (X   membersq}q(X   TaskQueuePageq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X.   twilio.rest.taskrouter.v1.workspace.task_queueqh�qX   twilio.base.pageqX   Pageq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __repr__q}q(hX   functionqh}q(X   docqXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strqX   builtinq�X   locationqMK	�qX	   overloadsq]q }q!(X   argsq"}q#(X   nameq$X   selfq%hhu�q&X   ret_typeq'hX   strq(�q)uauuX   get_instanceq*}q+(hhh}q,(hX  
        Build an instance of TaskQueueInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstanceq-h�hMK	�q.h]q/}q0(h"}q1(h$h%hhu}q2(h$X   payloadq3hNu�q4h'hX   TaskQueueInstanceq5�q6uauuX   __init__q7}q8(hhh}q9(hX�  
        Initialize the TaskQueuePage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param workspace_sid: The ID of the Workspace that owns this TaskQueue

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueuePage
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueuePageq:h�hK�K	�q;h]q<}q=(h"(}q>(h$h%hhu}q?(h$X   versionq@hX   twilio.rest.taskrouter.v1qAX   V1qB�qCu}qD(h$X   responseqEhNu}qF(h$X   solutionqGhhX   dictqH�qIutqJh'NuauuX	   _solutionqK}qL(hX   multipleqMh}qNh(}qO(hX   dataqPh}qQhhIsu}qR(hhPh}qShhIsu}qT(hhPh}qUhhIsu}qV(hhPh}qWhhIsutqXsuX   _versionqY}qZ(hhPh}q[hhCsuX   _payloadq\}q](hhMh}q^h(}q_(hhPh}q`hhX   floatqa�qbsu}qc(hhPh}qdhhX   boolqe�qfsu}qg(hhPh}qhhhX   intqi�qjsu}qk(hhPh}qlhhX   NoneTypeqm�qnsutqosuX   _recordsqp}qq(hhPh}qrhNsuuhX    qsh�hK�K�qtuuX    task_queue_cumulative_statisticsqu}qv(hX	   modulerefqwhXO   twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_cumulative_statisticsqxhs�qyuX   deserializeqz}q{(hhwhX   twilio.base.deserializeq|hs�q}uX   valuesq~}q(hhwhX   twilio.base.valuesq�hs�q�uX   TaskQueueListq�}q�(hhh}q�(h	]q�(hh��q�X   twilio.base.list_resourceq�X   ListResourceq��q�heh]q�h�ah}q�(h}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hh�u�q�h'h)uauuh7}q�(hhh}q�(hXU  
        Initialize the TaskQueueList

        :param Version version: Version that contains the resource
        :param workspace_sid: The ID of the Workspace that owns this TaskQueue

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueList
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueListq�h�hKK	�q�h]q�}q�(h"}q�(h$h%hh�u}q�(h$h@hhCu}q�(h$X   workspace_sidq�hhnu�q�h'NuauuX   streamq�}q�(hhh}q�(hX  
        Streams TaskQueueInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param unicode friendly_name: Filter by a human readable description of a TaskQueue
        :param unicode evaluate_worker_attributes: Provide a Worker attributes expression, and this will return the list of TaskQueues that would distribute tasks to a worker with these attributes.
        :param unicode worker_sid: The worker_sid
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstance]q�h�hK+K	�q�h]q�}q�(h"(}q�(h$h%hh�u}q�(h$X   friendly_nameq�hhX   default_valueq�X   values.unsetq�u}q�(h$X   evaluate_worker_attributesq�hhh�X   values.unsetq�u}q�(h$X
   worker_sidq�hhh�X   values.unsetq�u}q�(h$X   limitq�hhnh�X   Noneq�u}q�(h$X	   page_sizeq�hhnh�h�utq�h'hX	   generatorq��q�uauuX   listq�}q�(hhh}q�(hX�  
        Lists TaskQueueInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param unicode friendly_name: Filter by a human readable description of a TaskQueue
        :param unicode evaluate_worker_attributes: Provide a Worker attributes expression, and this will return the list of TaskQueues that would distribute tasks to a worker with these attributes.
        :param unicode worker_sid: The worker_sid
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstance]q�h�hKLK	�q�h]q�}q�(h"(}q�(h$h%hh�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhnh�h�u}q�(h$h�hhnh�h�utq�h'hX   listq�]q�Na�q�uauuX   getq�}q�(hhh}q�(hX�   
        Constructs a TaskQueueContext

        :param sid: The sid

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContext
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContextq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hh�u}q�(h$X   sidq�hNu�q�h'hX   TaskQueueContextq؆q�uauuX
   statisticsq�}q�(hX   propertyq�h}q�(hX�   
        Access the statistics

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.task_queues_statistics.TaskQueuesStatisticsList
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.task_queues_statistics.TaskQueuesStatisticsListq�h]q�(XE   twilio.rest.taskrouter.v1.workspace.task_queue.task_queues_statisticsq�X   TaskQueuesStatisticsListq�q�hnehK�K	�q�uuX   pageq�}q�(hhh}q�(hX  
        Retrieve a single page of TaskQueueInstance records from the API.
        Request is executed immediately

        :param unicode friendly_name: Filter by a human readable description of a TaskQueue
        :param unicode evaluate_worker_attributes: Provide a Worker attributes expression, and this will return the list of TaskQueues that would distribute tasks to a worker with these attributes.
        :param unicode worker_sid: The worker_sid
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueuePageq�h�hKiK	�q�h]q�}q�(h"(}q�(h$h%hh�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$X
   page_tokenq�hhh�X   values.unsetq�u}q�(h$X   page_numberq�hhh�X   values.unsetq�u}q�(h$h�h]q�(hhneh�X   values.unsetq�utq�h'huauuX   createq�}q�(hhh}q�(hXw  
        Create a new TaskQueueInstance

        :param unicode friendly_name: Human readable description of this TaskQueue
        :param unicode target_workers: A string describing the Worker selection criteria for any Tasks that enter this TaskQueue.
        :param unicode max_reserved_workers: The maximum amount of workers to create reservations for the assignment of a task while in this queue.
        :param TaskQueueInstance.TaskOrder task_order: TaskOrder will determine which order the Tasks will be assigned to Workers.
        :param unicode reservation_activity_sid: ActivitySID to assign workers once a task is reserved for them
        :param unicode assignment_activity_sid: ActivitySID to assign workers once a task is assigned for them

        :returns: Newly created TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstanceq�h�hK�K	�q h]r  }r  (h"(}r  (h$h%hh�u}r  (h$h�hNu}r  (h$X   target_workersr  hhh�X   values.unsetr  u}r  (h$X   max_reserved_workersr	  hhh�X   values.unsetr
  u}r  (h$X
   task_orderr  hhh�X   values.unsetr  u}r  (h$X   reservation_activity_sidr  hhh�X   values.unsetr  u}r  (h$X   assignment_activity_sidr  hhh�X   values.unsetr  utr  h'h6uauuX   get_pager  }r  (hhh}r  (hX?  
        Retrieve a specific page of TaskQueueInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueuePager  h�hK�K	�r  h]r  }r  (h"}r  (h$h%hh�u}r  (h$X
   target_urlr  hNu�r  h'huauuX   __call__r   }r!  (hhh}r"  (hX�   
        Constructs a TaskQueueContext

        :param sid: The sid

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContext
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContextr#  h�hK�K	�r$  h]r%  }r&  (h"}r'  (h$h%hh�u}r(  (h$h�hNu�r)  h'h�uauuhK}r*  (hhMh}r+  h}r,  (hhPh}r-  hhIsu}r.  (hhPh}r/  hhIsu}r0  (hhPh}r1  hhIsu�r2  suX   _urir3  }r4  (hhPh}r5  hh)suX   _statisticsr6  }r7  (hhMh}r8  h}r9  (hhPh}r:  hhnsu}r;  (hhPh}r<  hh�su�r=  suhY}r>  (hhPh}r?  hhCsuuhhsh�hKK�r@  uuX   ListResourcerA  }rB  (hX   typerefrC  h]rD  h�auh�}rE  (hhh}rF  (h	]rG  (h�X   twilio.base.instance_contextrH  X   InstanceContextrI  �rJ  heh]rK  jJ  ah}rL  (h7}rM  (hhh}rN  (hX[  
        Initialize the TaskQueueContext

        :param Version version: Version that contains the resource
        :param workspace_sid: The workspace_sid
        :param sid: The sid

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContext
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContextrO  h�hMK	�rP  h]rQ  }rR  (h"(}rS  (h$h%hh�u}rT  (h$h@hhCu}rU  (h$h�hhnu}rV  (h$h�hhnutrW  h'NuauuX   updaterX  }rY  (hhh}rZ  (hX�  
        Update the TaskQueueInstance

        :param unicode friendly_name: Human readable description of this TaskQueue
        :param unicode target_workers: A string describing the Worker selection criteria for any Tasks that enter this TaskQueue.
        :param unicode reservation_activity_sid: ActivitySID that will be assigned to Workers when they are reserved for a task from this TaskQueue.
        :param unicode assignment_activity_sid: ActivitySID that will be assigned to Workers when they are assigned a task from this TaskQueue.
        :param unicode max_reserved_workers: The maximum amount of workers to create reservations for the assignment of a task while in this queue.
        :param TaskQueueInstance.TaskOrder task_order: TaskOrder will determine which order the Tasks will be assigned to Workers.

        :returns: Updated TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstancer[  h�hMEK	�r\  h]r]  }r^  (h"(}r_  (h$h%hh�u}r`  (h$h�hhh�X   values.unsetra  u}rb  (h$j  hhh�X   values.unsetrc  u}rd  (h$j  hhh�X   values.unsetre  u}rf  (h$j  hhh�X   values.unsetrg  u}rh  (h$j	  hhh�X   values.unsetri  u}rj  (h$j  hhh�X   values.unsetrk  utrl  h'h6uauuX   real_time_statisticsrm  }rn  (hh�h}ro  (hX)  
        Access the real_time_statistics

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_real_time_statistics.TaskQueueRealTimeStatisticsList
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_real_time_statistics.TaskQueueRealTimeStatisticsListrp  h]rq  (XN   twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_real_time_statisticsrr  X   TaskQueueRealTimeStatisticsListrs  �rt  hnehM�K	�ru  uuh}rv  (hhh}rw  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strrx  h�hM�K	�ry  h]rz  }r{  (h"}r|  (h$h%hh�u�r}  h'h)uauuh�}r~  (hh�h}r  (hX�   
        Access the statistics

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_statistics.TaskQueueStatisticsList
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_statistics.TaskQueueStatisticsListr�  h]r�  (XD   twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_statisticsr�  X   TaskQueueStatisticsListr�  �r�  hnehMvK	�r�  uuX   cumulative_statisticsr�  }r�  (hh�h}r�  (hX0  
        Access the cumulative_statistics

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_cumulative_statistics.TaskQueueCumulativeStatisticsList
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.task_queue_cumulative_statistics.TaskQueueCumulativeStatisticsListr�  h]r�  (hxX!   TaskQueueCumulativeStatisticsListr�  �r�  hnehM�K	�r�  uuX   deleter�  }r�  (hhh}r�  (hXw   
        Deletes the TaskQueueInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h�hMlK	�r�  h]r�  }r�  (h"}r�  (h$h%hh�u�r�  h'NuauuX   fetchr�  }r�  (hhh}r�  (hX�   
        Fetch a TaskQueueInstance

        :returns: Fetched TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstancer�  h�hM/K	�r�  h]r�  }r�  (h"}r�  (h$h%hh�u�r�  h'h6uauuhK}r�  (hhMh}r�  h}r�  (hhPh}r�  hhIsu�r�  suj3  }r�  (hhPh}r�  hh)suj6  }r�  (hhMh}r�  h}r�  (hhPh}r�  hhnsu}r�  (hhPh}r�  hj�  su�r�  suX   _real_time_statisticsr�  }r�  (hhMh}r�  h}r�  (hhPh}r�  hhnsu}r�  (hhPh}r�  hjt  su�r�  suX   _cumulative_statisticsr�  }r�  (hhMh}r�  h}r�  (hhPh}r�  hhnsu}r�  (hhPh}r�  hj�  su�r�  suhY}r�  (hhPh}r�  hhCsuuhhsh�hMK�r�  uuh5}r�  (hhh}r�  (h	]r�  (h6X   twilio.base.instance_resourcer�  X   InstanceResourcer�  �r�  heh]r�  j�  ah}r�  (j	  }r�  (hh�h}r�  (hX�   
        :returns: The maximum amount of workers to create reservations for the assignment of a task while in this queue.
        :rtype: unicoder�  hNhMK	�r�  uuX   friendly_namer�  }r�  (hh�h}r�  (hX`   
        :returns: Filter by a human readable description of a TaskQueue
        :rtype: unicoder�  hNhMK	�r�  uuh7}r�  (hhh}r�  (hX�   
        Initialize the TaskQueueInstance

        :returns: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstancer�  h�hM�K	�r�  h]r�  }r�  (h"(}r�  (h$h%hh6u}r�  (h$h@hhCu}r�  (h$h3h]r�  (hfhnhbhjeu}r�  (h$h�hhnu}r�  (h$h�hhnh�h�utr�  h'NuauuX	   TaskOrderr�  }r�  (hjC  h]r�  hj�  �r�  aujm  }r�  (hh�h}r�  (hjp  h]r�  (jt  hnehM�K	�r�  uuX   sidr�  }r�  (hh�h}r�  (hXI   
        :returns: The unique ID of the TaskQueue
        :rtype: unicoder�  hNhM1K	�r�  uuj  }r�  (hh�h}r�  (hX�   
        :returns: TaskOrder will determine which order the Tasks will be assigned to Workers.
        :rtype: TaskQueueInstance.TaskOrderr�  hNhMAK	�r�  uuX   urlr�  }r�  (hh�h}r�  (hX2   
        :returns: The url
        :rtype: unicoder�  hNhMIK	�r�  uuX   assignment_activity_namer�  }r�  (hh�h}r�  (hXG   
        :returns: The assignment_activity_name
        :rtype: unicoder�  hNhM�K	�r�  uuX   linksr�  }r�  (hh�h}r�  (hX4   
        :returns: The links
        :rtype: unicoder�  hNhMYK	�r�  uuj  }r�  (hh�h}r   (hX�   
        :returns: A string describing the Worker selection criteria for any Tasks that enter this TaskQueue.
        :rtype: unicoder  hNhM9K	�r  uuh�}r  (hh�h}r  (hj�  h]r  (j�  hnehM�K	�r  uuh}r  (hhh}r  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr	  h�hM�K	�r
  h]r  }r  (h"}r  (h$h%hh6u�r  h'h)uauuj�  }r  (hh�h}r  (hj�  h]r  (j�  hnehM�K	�r  uuj�  }r  (hhh}r  (hX�   
        Fetch a TaskQueueInstance

        :returns: Fetched TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstancer  h�hM`K	�r  h]r  }r  (h"}r  (h$h%hh6u�r  h'h6uauuX   date_createdr  }r  (hh�h}r  (hX<   
        :returns: The date_created
        :rtype: datetimer  hNhMK	�r  uuj  }r   (hh�h}r!  (hXi   
        :returns: ActivitySID to assign workers once a task is reserved for them
        :rtype: unicoder"  hNhM!K	�r#  uuX   workspace_sidr$  }r%  (hh�h}r&  (hX[   
        :returns: The ID of the Workspace that owns this TaskQueue
        :rtype: unicoder'  hNhMQK	�r(  uuj  }r)  (hh�h}r*  (hXi   
        :returns: ActivitySID to assign workers once a task is assigned for them
        :rtype: unicoder+  hNhM�K	�r,  uuX   account_sidr-  }r.  (hh�h}r/  (hXY   
        :returns: The ID of the Account that owns this TaskQueue
        :rtype: unicoder0  hNhM�K	�r1  uuX   reservation_activity_namer2  }r3  (hh�h}r4  (hXH   
        :returns: The reservation_activity_name
        :rtype: unicoder5  hNhM)K	�r6  uujX  }r7  (hhh}r8  (hX�  
        Update the TaskQueueInstance

        :param unicode friendly_name: Human readable description of this TaskQueue
        :param unicode target_workers: A string describing the Worker selection criteria for any Tasks that enter this TaskQueue.
        :param unicode reservation_activity_sid: ActivitySID that will be assigned to Workers when they are reserved for a task from this TaskQueue.
        :param unicode assignment_activity_sid: ActivitySID that will be assigned to Workers when they are assigned a task from this TaskQueue.
        :param unicode max_reserved_workers: The maximum amount of workers to create reservations for the assignment of a task while in this queue.
        :param TaskQueueInstance.TaskOrder task_order: TaskOrder will determine which order the Tasks will be assigned to Workers.

        :returns: Updated TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueInstancer9  h�hMiK	�r:  h]r;  }r<  (h"(}r=  (h$h%hh6u}r>  (h$h�hhh�X   values.unsetr?  u}r@  (h$j  hhh�X   values.unsetrA  u}rB  (h$j  hhh�X   values.unsetrC  u}rD  (h$j  hhh�X   values.unsetrE  u}rF  (h$j	  hhh�X   values.unsetrG  u}rH  (h$j  hhh�X   values.unsetrI  utrJ  h'h6uauuj�  }rK  (hhh}rL  (hXw   
        Deletes the TaskQueueInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolrM  h�hM�K	�rN  h]rO  }rP  (h"}rQ  (h$h%hh6u�rR  h'NuauuX   _proxyrS  }rT  (hh�h}rU  (hX5  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: TaskQueueContext for this TaskQueueInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.task_queue.TaskQueueContextrV  h]rW  (h�hnehM�K	�rX  uuX   date_updatedrY  }rZ  (hh�h}r[  (hX<   
        :returns: The date_updated
        :rtype: datetimer\  hNhM	K	�r]  uuX   _propertiesr^  }r_  (hhMh}r`  h}ra  (hhPh}rb  hhIsu�rc  suX   _contextrd  }re  (hhMh}rf  h}rg  (hhPh}rh  hhnsu}ri  (hhPh}rj  hh�su�rk  suhK}rl  (hhMh}rm  h}rn  (hhPh}ro  hhIsu�rp  suhY}rq  (hhPh}rr  hhCsuuhhsh�hM�K�rs  uuX   task_queue_real_time_statisticsrt  }ru  (hhwhjr  hs�rv  uX   task_queues_statisticsrw  }rx  (hhwhh�hs�ry  uX   TaskQueuesStatisticsListrz  }r{  (hjC  h]r|  h�auX   task_queue_statisticsr}  }r~  (hhwhj�  hs�r  uX!   TaskQueueCumulativeStatisticsListr�  }r�  (hjC  h]r�  j�  auX   TaskQueueStatisticsListr�  }r�  (hjC  h]r�  j�  auX   InstanceResourcer�  }r�  (hjC  h]r�  j�  auX   Pager�  }r�  (hjC  h]r�  hauX   InstanceContextr�  }r�  (hjC  h]r�  jJ  auX   TaskQueueRealTimeStatisticsListr�  }r�  (hjC  h]r�  jt  auuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  (jw  hujt  j}  eX   filenamer�  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\taskrouter\v1\workspace\task_queue\__init__.pyr�  u.