�}q (X   membersq}q(X	   TrunkListq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X   twilio.rest.trunking.v1.trunkqh�qX   twilio.base.list_resourceqX   ListResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docqX�   
        Initialize the TrunkList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.trunking.v1.trunk.TrunkList
        :rtype: twilio.rest.trunking.v1.trunk.TrunkListqX   builtinq�X   locationqKK	�qX	   overloadsq]q }q!(X   argsq"}q#(X   nameq$X   selfq%hhu}q&(h$X   versionq'hX   twilio.rest.trunking.v1q(X   V1q)�q*u�q+X   ret_typeq,NuauuX   streamq-}q.(hhh}q/(hX�  
        Streams TrunkInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.trunking.v1.trunk.TrunkInstance]q0h�hKLK	�q1h]q2}q3(h"}q4(h$h%hhu}q5(h$X   limitq6hhX   NoneTypeq7�q8X   default_valueq9X   Noneq:u}q;(h$X	   page_sizeq<hh8h9h:u�q=h,hX	   generatorq>�q?uauuX   createq@}qA(hhh}qB(hX|  
        Create a new TrunkInstance

        :param unicode friendly_name: A human-readable name for the Trunk.
        :param unicode domain_name: The unique address you reserve on Twilio to which you route your SIP traffic.
        :param unicode disaster_recovery_url: The HTTP URL that Twilio will request if an error occurs while sending SIP traffic towards your configured Origination URL.
        :param unicode disaster_recovery_method: The HTTP method Twilio will use when requesting the DisasterRecoveryUrl.
        :param TrunkInstance.RecordingSetting recording: The recording settings for this trunk.
        :param bool secure: The Secure Trunking  settings for this trunk.
        :param bool cnam_lookup_enabled: The Caller ID Name (CNAM) lookup setting for this trunk.

        :returns: Newly created TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstanceqCh�hK(K	�qDh]qE}qF(h"(}qG(h$h%hhu}qH(h$X   friendly_nameqIhhh9X   values.unsetqJu}qK(h$X   domain_nameqLhhh9X   values.unsetqMu}qN(h$X   disaster_recovery_urlqOhhh9X   values.unsetqPu}qQ(h$X   disaster_recovery_methodqRhhh9X   values.unsetqSu}qT(h$X	   recordingqUhhh9X   values.unsetqVu}qW(h$X   secureqXhhh9X   values.unsetqYu}qZ(h$X   cnam_lookup_enabledq[hhh9X   values.unsetq\utq]h,hX   TrunkInstanceq^�q_uauuX   getq`}qa(hhh}qb(hX�   
        Constructs a TrunkContext

        :param sid: A 34 character string that uniquely identifies the SIP Trunk in Twilio.

        :returns: twilio.rest.trunking.v1.trunk.TrunkContext
        :rtype: twilio.rest.trunking.v1.trunk.TrunkContextqch�hK�K	�qdh]qe}qf(h"}qg(h$h%hhu}qh(h$X   sidqihNu�qjh,hX   TrunkContextqk�qluauuX   __repr__qm}qn(hhh}qo(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strqph�hK�K	�qqh]qr}qs(h"}qt(h$h%hhu�quh,hX   strqv�qwuauuX   pageqx}qy(hhh}qz(hX�  
        Retrieve a single page of TrunkInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkPageq{h�hKuK	�q|h]q}}q~(h"(}q(h$h%hhu}q�(h$X
   page_tokenq�hhh9X   values.unsetq�u}q�(h$X   page_numberq�hhh9X   values.unsetq�u}q�(h$h<h]q�(hh8eh9X   values.unsetq�utq�h,hX	   TrunkPageq��q�uauuX   listq�}q�(hhh}q�(hXL  
        Lists TrunkInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.trunking.v1.trunk.TrunkInstance]q�h�hKcK	�q�h]q�}q�(h"}q�(h$h%hhu}q�(h$h6hh8h9h:u}q�(h$h<hh8h9h:u�q�h,hX   listq�]q�Na�q�uauuX   get_pageq�}q�(hhh}q�(hX"  
        Retrieve a specific page of TrunkInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkPageq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hhu}q�(h$X
   target_urlq�hNu�q�h,h�uauuX   __call__q�}q�(hhh}q�(hX�   
        Constructs a TrunkContext

        :param sid: A 34 character string that uniquely identifies the SIP Trunk in Twilio.

        :returns: twilio.rest.trunking.v1.trunk.TrunkContext
        :rtype: twilio.rest.trunking.v1.trunk.TrunkContextq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hhu}q�(h$hihNu�q�h,hluauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hhwsuX   _versionq�}q�(hh�h}q�hh*suuhX    q�h�hKK�q�uuX   ip_access_control_listq�}q�(hX	   modulerefq�hX4   twilio.rest.trunking.v1.trunk.ip_access_control_listq�hÆq�uX   valuesq�}q�(hh�hX   twilio.base.valuesq�hÆq�uhk}q�(hhh}q�(h	]q�(hlX   twilio.base.instance_contextq�X   InstanceContextq҆q�heh]q�h�ah}q�(h}q�(hhh}q�(hX=  
        Initialize the TrunkContext

        :param Version version: Version that contains the resource
        :param sid: A 34 character string that uniquely identifies the SIP Trunk in Twilio.

        :returns: twilio.rest.trunking.v1.trunk.TrunkContext
        :rtype: twilio.rest.trunking.v1.trunk.TrunkContextq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hhlu}q�(h$h'hh*u}q�(h$hihh8u�q�h,NuauuX   ip_access_control_listsq�}q�(hX   propertyq�h}q�(hX�   
        Access the ip_access_control_lists

        :returns: twilio.rest.trunking.v1.trunk.ip_access_control_list.IpAccessControlListList
        :rtype: twilio.rest.trunking.v1.trunk.ip_access_control_list.IpAccessControlListListq�h]q�(h8h�X   IpAccessControlListListq�q�ehMUK	�q�uuX   origination_urlsq�}q�(hh�h}q�(hX�   
        Access the origination_urls

        :returns: twilio.rest.trunking.v1.trunk.origination_url.OriginationUrlList
        :rtype: twilio.rest.trunking.v1.trunk.origination_url.OriginationUrlListq�h]q�(X-   twilio.rest.trunking.v1.trunk.origination_urlq�X   OriginationUrlListq�q�h8ehM=K	�q�uuX   phone_numbersq�}q�(hh�h}q�(hX�   
        Access the phone_numbers

        :returns: twilio.rest.trunking.v1.trunk.phone_number.PhoneNumberList
        :rtype: twilio.rest.trunking.v1.trunk.phone_number.PhoneNumberListq�h]q�(h8X*   twilio.rest.trunking.v1.trunk.phone_numberq�X   PhoneNumberListq��q�ehMdK	�q�uuhm}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hM~K	�q�h]q�}q (h"}r  (h$h%hhlu�r  h,hwuauuX   fetchr  }r  (hhh}r  (hX�   
        Fetch a TrunkInstance

        :returns: Fetched TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstancer  h�hK�K	�r  h]r  }r	  (h"}r
  (h$h%hhlu�r  h,h_uauuX   terminating_sip_domainsr  }r  (hh�h}r  (hX�   
        Access the terminating_sip_domains

        :returns: twilio.rest.trunking.v1.trunk.terminating_sip_domain.TerminatingSipDomainList
        :rtype: twilio.rest.trunking.v1.trunk.terminating_sip_domain.TerminatingSipDomainListr  h]r  (X4   twilio.rest.trunking.v1.trunk.terminating_sip_domainr  X   TerminatingSipDomainListr  �r  h8ehMpK	�r  uuX   credentials_listsr  }r  (hh�h}r  (hX�   
        Access the credentials_lists

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListList
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListListr  h]r  (X-   twilio.rest.trunking.v1.trunk.credential_listr  X   CredentialListListr  �r  h8ehMIK	�r  uuX   updater  }r  (hhh}r   (hXt  
        Update the TrunkInstance

        :param unicode friendly_name: A human-readable name for the Trunk.
        :param unicode domain_name: The unique address you reserve on Twilio to which you route your SIP traffic.
        :param unicode disaster_recovery_url: The HTTP URL that Twilio will request if an error occurs while sending SIP traffic towards your configured Origination URL.
        :param unicode disaster_recovery_method: The HTTP method Twilio will use when requesting the DisasterRecoveryUrl.
        :param TrunkInstance.RecordingSetting recording: The recording settings for this trunk.
        :param bool secure: The Secure Trunking  settings for this trunk.
        :param bool cnam_lookup_enabled: The Caller ID Name (CNAM) lookup setting for this trunk.

        :returns: Updated TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstancer!  h�hMK	�r"  h]r#  }r$  (h"(}r%  (h$h%hhlu}r&  (h$hIhhh9X   values.unsetr'  u}r(  (h$hLhhh9X   values.unsetr)  u}r*  (h$hOhhh9X   values.unsetr+  u}r,  (h$hRhhh9X   values.unsetr-  u}r.  (h$hUhhh9X   values.unsetr/  u}r0  (h$hXhhh9X   values.unsetr1  u}r2  (h$h[hhh9X   values.unsetr3  utr4  h,h_uauuX   deleter5  }r6  (hhh}r7  (hXs   
        Deletes the TrunkInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr8  h�hMK	�r9  h]r:  }r;  (h"}r<  (h$h%hhlu�r=  h,Nuauuh�}r>  (hh�h}r?  h}r@  (hh�h}rA  hh�su�rB  suh�}rC  (hh�h}rD  hhwsuX   _origination_urlsrE  }rF  (hh�h}rG  h}rH  (hh�h}rI  hh8su}rJ  (hh�h}rK  hh�su�rL  suX   _credentials_listsrM  }rN  (hh�h}rO  h}rP  (hh�h}rQ  hh8su}rR  (hh�h}rS  hj  su�rT  suX   _ip_access_control_listsrU  }rV  (hh�h}rW  h}rX  (hh�h}rY  hh8su}rZ  (hh�h}r[  hh�su�r\  suX   _phone_numbersr]  }r^  (hh�h}r_  h}r`  (hh�h}ra  hh8su}rb  (hh�h}rc  hh�su�rd  suX   _terminating_sip_domainsre  }rf  (hh�h}rg  h}rh  (hh�h}ri  hh8su}rj  (hh�h}rk  hj  su�rl  suh�}rm  (hh�h}rn  hh*suuhh�h�hK�K�ro  uuX   phone_numberrp  }rq  (hh�hh�hÆrr  uX   ListResourcers  }rt  (hX   typerefru  h]rv  hauX   credential_listrw  }rx  (hh�hj  hÆry  uh�}rz  (hhh}r{  (h	]r|  (h�X   twilio.base.pager}  X   Pager~  �r  heh]r�  j  ah}r�  (hm}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hK�K	�r�  h]r�  }r�  (h"}r�  (h$h%hh�u�r�  h,hwuauuX   get_instancer�  }r�  (hhh}r�  (hX�   
        Build an instance of TrunkInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.trunking.v1.trunk.TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstancer�  h�hK�K	�r�  h]r�  }r�  (h"}r�  (h$h%hh�u}r�  (h$X   payloadr�  hNu�r�  h,h_uauuh}r�  (hhh}r�  (hX  
        Initialize the TrunkPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.trunking.v1.trunk.TrunkPage
        :rtype: twilio.rest.trunking.v1.trunk.TrunkPager�  h�hK�K	�r�  h]r�  }r�  (h"(}r�  (h$h%hh�u}r�  (h$h'hh*u}r�  (h$X   responser�  hNu}r�  (h$X   solutionr�  hh�utr�  h,Nuauuh�}r�  (hh�h}r�  h(}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�sutr�  suh�}r�  (hh�h}r�  hh*suX   _payloadr�  }r�  (hh�h}r�  h(}r�  (hh�h}r�  hhX   floatr�  �r�  su}r�  (hh�h}r�  hhX   boolr�  �r�  su}r�  (hh�h}r�  hhX   intr�  �r�  su}r�  (hh�h}r�  hh8sutr�  suX   _recordsr�  }r�  (hh�h}r�  hNsuuhh�h�hK�K�r�  uuX   IpAccessControlListListr�  }r�  (hju  h]r�  h�auX   terminating_sip_domainr�  }r�  (hh�hj  hÆr�  uX   deserializer�  }r�  (hh�hX   twilio.base.deserializer�  hÆr�  uX   PhoneNumberListr�  }r�  (hju  h]r�  h�auX   CredentialListListr�  }r�  (hju  h]r�  j  auX   origination_urlr�  }r�  (hh�hh�hÆr�  uX   InstanceResourcer�  }r�  (hju  h]r�  X   twilio.base.instance_resourcer�  X   InstanceResourcer�  �r�  auX   Pager�  }r�  (hju  h]r�  j  auX   InstanceContextr�  }r�  (hju  h]r�  h�auh^}r�  (hhh}r�  (h	]r�  (h_j�  heh]r�  j�  ah}r�  (h�}r�  (hh�h}r�  (hh�h]r�  (h�h8ehM�K	�r�  uuX   friendly_namer�  }r�  (hh�h}r�  (hXO   
        :returns: A human-readable name for the Trunk.
        :rtype: unicoder�  hNhM�K	�r�  uuj  }r�  (hhh}r�  (hX�   
        Fetch a TrunkInstance

        :returns: Fetched TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstancer�  h�hM6K	�r�  h]r�  }r�  (h"}r�  (h$h%hh_u�r�  h,h_uauuh}r�  (hhh}r�  (hX�   
        Initialize the TrunkInstance

        :returns: twilio.rest.trunking.v1.trunk.TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstancer�  h�hM�K	�r�  h]r�  }r�  (h"(}r   (h$h%hh_u}r  (h$h'hh*u}r  (h$j�  h]r  (j�  h8j�  j�  eu}r  (h$hihh8h9h:utr  h,Nuauuh�}r  (hh�h}r  (hh�h]r  (h�h8ehMyK	�r	  uuh[}r
  (hh�h}r  (hX`   
        :returns: The Caller ID Name (CNAM) lookup setting for this trunk.
        :rtype: boolr  hNhM�K	�r  uuj  }r  (hh�h}r  (hj  h]r  (j  h8ehM�K	�r  uuX   sidr  }r  (hh�h}r  (hXr   
        :returns: A 34 character string that uniquely identifies the SIP Trunk in Twilio.
        :rtype: unicoder  hNhMK	�r  uuX	   auth_typer  }r  (hh�h}r  (hXf   
        :returns: The types of authentication you have mapped to your domain.
        :rtype: unicoder  hNhM�K	�r  uuX   urlr  }r  (hh�h}r  (hXc   
        :returns: The URL for this resource, relative to https://trunking.
        :rtype: unicoder  hNhM'K	�r   uuX   auth_type_setr!  }r"  (hh�h}r#  (hX<   
        :returns: The auth_type_set
        :rtype: unicoder$  hNhMK	�r%  uuX   RecordingSettingr&  }r'  (hju  h]r(  hj&  �r)  auhL}r*  (hh�h}r+  (hXx   
        :returns: The unique address you reserve on Twilio to which you route your SIP traffic.
        :rtype: unicoder,  hNhM�K	�r-  uuh�}r.  (hh�h}r/  (hh�h]r0  (h�h8ehMeK	�r1  uuhR}r2  (hh�h}r3  (hXs   
        :returns: The HTTP method Twilio will use when requesting the DisasterRecoveryUrl.
        :rtype: unicoder4  hNhM�K	�r5  uuhm}r6  (hhh}r7  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr8  h�hM�K	�r9  h]r:  }r;  (h"}r<  (h$h%hh_u�r=  h,hwuauuhU}r>  (hh�h}r?  (hXN   
        :returns: The recording settings for this trunk.
        :rtype: dictr@  hNhM�K	�rA  uuhO}rB  (hh�h}rC  (hX�   
        :returns: The HTTP URL that Twilio will request if an error occurs while sending SIP traffic towards your configured Origination URL.
        :rtype: unicoderD  hNhM�K	�rE  uuX   date_createdrF  }rG  (hh�h}rH  (hXO   
        :returns: The date this Activity was created.
        :rtype: datetimerI  hNhMK	�rJ  uuj  }rK  (hh�h}rL  (hj  h]rM  (j  h8ehMoK	�rN  uuX   linksrO  }rP  (hh�h}rQ  (hX4   
        :returns: The links
        :rtype: unicoderR  hNhM/K	�rS  uuX   account_sidrT  }rU  (hh�h}rV  (hX]   
        :returns: The unique ID of the Account that owns this Trunk.
        :rtype: unicoderW  hNhM�K	�rX  uuhX}rY  (hh�h}rZ  (hXU   
        :returns: The Secure Trunking  settings for this trunk.
        :rtype: boolr[  hNhM�K	�r\  uuj  }r]  (hhh}r^  (hXt  
        Update the TrunkInstance

        :param unicode friendly_name: A human-readable name for the Trunk.
        :param unicode domain_name: The unique address you reserve on Twilio to which you route your SIP traffic.
        :param unicode disaster_recovery_url: The HTTP URL that Twilio will request if an error occurs while sending SIP traffic towards your configured Origination URL.
        :param unicode disaster_recovery_method: The HTTP method Twilio will use when requesting the DisasterRecoveryUrl.
        :param TrunkInstance.RecordingSetting recording: The recording settings for this trunk.
        :param bool secure: The Secure Trunking  settings for this trunk.
        :param bool cnam_lookup_enabled: The Caller ID Name (CNAM) lookup setting for this trunk.

        :returns: Updated TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkInstancer_  h�hMHK	�r`  h]ra  }rb  (h"(}rc  (h$h%hh_u}rd  (h$hIhhh9X   values.unsetre  u}rf  (h$hLhhh9X   values.unsetrg  u}rh  (h$hOhhh9X   values.unsetri  u}rj  (h$hRhhh9X   values.unsetrk  u}rl  (h$hUhhh9X   values.unsetrm  u}rn  (h$hXhhh9X   values.unsetro  u}rp  (h$h[hhh9X   values.unsetrq  utrr  h,h_uauuj5  }rs  (hhh}rt  (hXs   
        Deletes the TrunkInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolru  h�hM?K	�rv  h]rw  }rx  (h"}ry  (h$h%hh_u�rz  h,NuauuX   _proxyr{  }r|  (hh�h}r}  (hX  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: TrunkContext for this TrunkInstance
        :rtype: twilio.rest.trunking.v1.trunk.TrunkContextr~  h]r  (h8hlehM�K	�r�  uuX   date_updatedr�  }r�  (hh�h}r�  (hXO   
        :returns: The date this Activity was updated.
        :rtype: datetimer�  hNhMK	�r�  uuX   _propertiesr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh8su}r�  (hh�h}r�  hhlsu�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hh*suuhh�h�hM�K�r�  uuX   OriginationUrlListr�  }r�  (hju  h]r�  h�auX   TerminatingSipDomainListr�  }r�  (hju  h]r�  j  auuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  (jw  h�j�  jp  j�  eX   filenamer�  Xw   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\trunking\v1\trunk\__init__.pyr�  u.