�}q (X   membersq}q(X   ParticipantPageq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X*   twilio.rest.video.v1.room.room_participantqX   ParticipantPageq�qX   twilio.base.pageqX   Pageq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __repr__q}q(hX   functionqh}q(X   docqXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strqX   builtinq�X   locationqK�K	�qX	   overloadsq ]q!}q"(X   argsq#}q$(X   nameq%X   selfq&hhu�q'X   ret_typeq(hX   strq)�q*uauuX   get_instanceq+}q,(hhh}q-(hX  
        Build an instance of ParticipantInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantInstanceq.h�hK�K	�q/h ]q0}q1(h#}q2(h%h&hhu}q3(h%X   payloadq4hNu�q5h(hX   ParticipantInstanceq6�q7uauuX   __init__q8}q9(hhh}q:(hX�  
        Initialize the ParticipantPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param room_sid: A system-generated 34-character string that uniquely identifies.

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantPage
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantPageq;h�hK�K	�q<h ]q=}q>(h#(}q?(h%h&hhu}q@(h%X   versionqAhX   twilio.rest.video.v1qBX   V1qC�qDu}qE(h%X   responseqFhNu}qG(h%X   solutionqHhhX   dictqI�qJutqKh(NuauuX	   _solutionqL}qM(hX   multipleqNh}qOh(}qP(hX   dataqQh}qRhhJsu}qS(hhQh}qThhJsu}qU(hhQh}qVhhJsu}qW(hhQh}qXhhJsutqYsuX   _versionqZ}q[(hhQh}q\hhDsuX   _payloadq]}q^(hhNh}q_h(}q`(hhQh}qahhX   floatqb�qcsu}qd(hhQh}qehhX   boolqf�qgsu}qh(hhQh}qihhX   intqj�qksu}ql(hhQh}qmhhX   NoneTypeqn�qosutqpsuX   _recordsqq}qr(hhQh}qshNsuuhX    qth�hK�K�quuuX	   serializeqv}qw(hX	   modulerefqxhX   twilio.base.serializeqyht�qzuX   ListResourceq{}q|(hX   typerefq}h]q~X   twilio.base.list_resourceqX   ListResourceq��q�auX   deserializeq�}q�(hhxhX   twilio.base.deserializeq�ht�q�uX   InstanceContextq�}q�(hh}h]q�X   twilio.base.instance_contextq�X   InstanceContextq��q�auX!   room_participant_subscribed_trackq�}q�(hhxhXL   twilio.rest.video.v1.room.room_participant.room_participant_subscribed_trackq�ht�q�uX   PublishedTrackListq�}q�(hh}h]q�XK   twilio.rest.video.v1.room.room_participant.room_participant_published_trackq�X   PublishedTrackListq��q�auX    room_participant_published_trackq�}q�(hhxhh�ht�q�uX   ParticipantListq�}q�(hhh}q�(h	]q�(hX   ParticipantListq��q�h�heh]q�h�ah}q�(h8}q�(hhh}q�(hX^  
        Initialize the ParticipantList

        :param Version version: Version that contains the resource
        :param room_sid: A system-generated 34-character string that uniquely identifies.

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantList
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantListq�h�hKK	�q�h ]q�}q�(h#}q�(h%h&hh�u}q�(h%hAhhDu}q�(h%X   room_sidq�hhou�q�h(NuauuX   streamq�}q�(hhh}q�(hX�  
        Streams ParticipantInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param ParticipantInstance.Status status: Only show Participants with the given Status.
        :param unicode identity: Only show Participants that connected to the Room using the provided Identity.
        :param datetime date_created_after: Only show Participants that started after this date, given as an UTC ISO 8601 Timestamp.
        :param datetime date_created_before: Only show Participants that started before this date, given as an UTC ISO 8601 Timestamp.
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.video.v1.room.room_participant.ParticipantInstance]q�h�hK'K	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%X   statusq�hhX   default_valueq�X   values.unsetq�u}q�(h%X   identityq�hhh�X   values.unsetq�u}q�(h%X   date_created_afterq�hhh�X   values.unsetq�u}q�(h%X   date_created_beforeq�hhh�X   values.unsetq�u}q�(h%X   limitq�hhoh�X   Noneq�u}q�(h%X	   page_sizeq�hhoh�h�utq�h(hX	   generatorqǆq�uauuX   listq�}q�(hhh}q�(hXA  
        Lists ParticipantInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param ParticipantInstance.Status status: Only show Participants with the given Status.
        :param unicode identity: Only show Participants that connected to the Room using the provided Identity.
        :param datetime date_created_after: Only show Participants that started after this date, given as an UTC ISO 8601 Timestamp.
        :param datetime date_created_before: Only show Participants that started before this date, given as an UTC ISO 8601 Timestamp.
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.video.v1.room.room_participant.ParticipantInstance]q�h�hKJK	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhoh�h�u}q�(h%h�hhoh�h�utq�h(hX   listq�]q�Na�q�uauuX   getq�}q�(hhh}q�(hX�   
        Constructs a ParticipantContext

        :param sid: The sid

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantContext
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hh�u}q�(h%X   sidq�hNu�q�h(hX   ParticipantContextq�q�uauuh}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hh�u�q�h(h*uauuX   pageq�}q�(hhh}q�(hX�  
        Retrieve a single page of ParticipantInstance records from the API.
        Request is executed immediately

        :param ParticipantInstance.Status status: Only show Participants with the given Status.
        :param unicode identity: Only show Participants that connected to the Room using the provided Identity.
        :param datetime date_created_after: Only show Participants that started after this date, given as an UTC ISO 8601 Timestamp.
        :param datetime date_created_before: Only show Participants that started before this date, given as an UTC ISO 8601 Timestamp.
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantPageq�h�hKiK	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q (h%h�hhh�X   values.unsetr  u}r  (h%h�hhh�X   values.unsetr  u}r  (h%X
   page_tokenr  hhh�X   values.unsetr  u}r  (h%X   page_numberr  hhh�X   values.unsetr	  u}r
  (h%h�h]r  (hhoeh�X   values.unsetr  utr  h(huauuX   get_pager  }r  (hhh}r  (hXA  
        Retrieve a specific page of ParticipantInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantPager  h�hK�K	�r  h ]r  }r  (h#}r  (h%h&hh�u}r  (h%X
   target_urlr  hNu�r  h(huauuX   __call__r  }r  (hhh}r  (hX�   
        Constructs a ParticipantContext

        :param sid: The sid

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantContext
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantContextr  h�hK�K	�r  h ]r  }r  (h#}r   (h%h&hh�u}r!  (h%h�hNu�r"  h(h�uauuhL}r#  (hhNh}r$  h}r%  (hhQh}r&  hhJsu}r'  (hhQh}r(  hhJsu}r)  (hhQh}r*  hhJsu�r+  suX   _urir,  }r-  (hhQh}r.  hh*suhZ}r/  (hhQh}r0  hhDsuuhhth�hKK�r1  uuX   SubscribedTrackListr2  }r3  (hh}h]r4  h�X   SubscribedTrackListr5  �r6  auX   InstanceResourcer7  }r8  (hh}h]r9  X   twilio.base.instance_resourcer:  X   InstanceResourcer;  �r<  auX   valuesr=  }r>  (hhxhX   twilio.base.valuesr?  ht�r@  uX   ParticipantInstancerA  }rB  (hhh}rC  (h	]rD  (h7j<  heh]rE  j<  ah}rF  (h8}rG  (hhh}rH  (hX�   
        Initialize the ParticipantInstance

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantInstancerI  h�hM_K	�rJ  h ]rK  }rL  (h#(}rM  (h%h&hh7u}rN  (h%hAhhDu}rO  (h%h4h]rP  (hghohchkeu}rQ  (h%h�hhou}rR  (h%h�hhoh�h�utrS  h(NuauuX
   start_timerT  }rU  (hX   propertyrV  h}rW  (hX}   
        :returns: The time of Participant connected to the Room, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimerX  hNhM�K	�rY  uuX   sidrZ  }r[  (hjV  h}r\  (hXh   
        :returns: A 34 character string that uniquely identifies this resource.
        :rtype: unicoder]  hNhM�K	�r^  uuX   urlr_  }r`  (hjV  h}ra  (hXN   
        :returns: The absolute URL for this resource.
        :rtype: unicoderb  hNhM�K	�rc  uuX   linksrd  }re  (hjV  h}rf  (hX4   
        :returns: The links
        :rtype: unicoderg  hNhM�K	�rh  uuX   durationri  }rj  (hjV  h}rk  (hXf   
        :returns: Duration of time in seconds this Participant was connected.
        :rtype: unicoderl  hNhM�K	�rm  uuX   subscribed_tracksrn  }ro  (hjV  h}rp  (hX
  
        Access the subscribed_tracks

        :returns: twilio.rest.video.v1.room.room_participant.room_participant_subscribed_track.SubscribedTrackList
        :rtype: twilio.rest.video.v1.room.room_participant.room_participant_subscribed_track.SubscribedTrackListrq  h]rr  (j6  hoehMK	�rs  uuX   published_tracksrt  }ru  (hjV  h}rv  (hX  
        Access the published_tracks

        :returns: twilio.rest.video.v1.room.room_participant.room_participant_published_track.PublishedTrackList
        :rtype: twilio.rest.video.v1.room.room_participant.room_participant_published_track.PublishedTrackListrw  h]rx  (h�hoehMK	�ry  uuh}rz  (hhh}r{  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr|  h�hMK	�r}  h ]r~  }r  (h#}r�  (h%h&hh7u�r�  h(h*uauuX   statusr�  }r�  (hjV  h}r�  (hXr   
        :returns: A string representing the status of the Participant.
        :rtype: ParticipantInstance.Statusr�  hNhM�K	�r�  uuX   fetchr�  }r�  (hhh}r�  (hX�   
        Fetch a ParticipantInstance

        :returns: Fetched ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh7u�r�  h(h7uauuX   date_createdr�  }r�  (hjV  h}r�  (hXw   
        :returns: The date that this resource was created, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimer�  hNhM�K	�r�  uuX   end_timer�  }r�  (hjV  h}r�  (hX�   
        :returns: The time of Participant disconnected from the Room, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimer�  hNhM�K	�r�  uuX   account_sidr�  }r�  (hjV  h}r�  (hXb   
        :returns: The unique ID of the Account associated with this Room.
        :rtype: unicoder�  hNhM�K	�r�  uuX   room_sidr�  }r�  (hjV  h}r�  (hXk   
        :returns: A system-generated 34-character string that uniquely identifies.
        :rtype: unicoder�  hNhM�K	�r�  uuX   updater�  }r�  (hhh}r�  (hX  
        Update the ParticipantInstance

        :param ParticipantInstance.Status status: Set to disconnected to remove participant.

        :returns: Updated ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh7u}r�  (h%h�hhh�X   values.unsetr�  u�r�  h(h7uauuX   identityr�  }r�  (hjV  h}r�  (hXk   
        :returns: The unique name identifier that is assigned to this Participant.
        :rtype: unicoder�  hNhM�K	�r�  uuX   _proxyr�  }r�  (hjV  h}r�  (hX7  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: ParticipantContext for this ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantContextr�  h]r�  (hoh�ehM}K	�r�  uuX   Statusr�  }r�  (hh}h]r�  hX   Statusr�  �r�  auX   date_updatedr�  }r�  (hjV  h}r�  (hX|   
        :returns: The date that this resource was last updated, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimer�  hNhM�K	�r�  uuX   _propertiesr�  }r�  (hhNh}r�  h}r�  (hhQh}r�  hhJsu�r�  suX   _contextr�  }r�  (hhNh}r�  h}r�  (hhQh}r�  hhosu}r�  (hhQh}r�  hh�su�r�  suhL}r�  (hhNh}r�  h}r�  (hhQh}r�  hhJsu�r�  suhZ}r�  (hhQh}r�  hhDsuuhhth�hMXK�r�  uuX   ParticipantContextr�  }r�  (hhh}r�  (h	]r�  (h�h�heh]r�  h�ah}r�  (jn  }r�  (hjV  h}r�  (hjq  h]r�  (j6  hoehM>K	�r�  uuh8}r�  (hhh}r�  (hXO  
        Initialize the ParticipantContext

        :param Version version: Version that contains the resource
        :param room_sid: The room_sid
        :param sid: The sid

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantContext
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantContextr�  h�hK�K	�r�  h ]r�  }r�  (h#(}r�  (h%h&hh�u}r�  (h%hAhhDu}r�  (h%h�hhou}r�  (h%h�hhoutr�  h(Nuauuh}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hMMK	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h(h*uauujt  }r�  (hjV  h}r�  (hjw  h]r�  (hoh�ehM.K	�r�  uuj�  }r�  (hhh}r�  (hX  
        Update the ParticipantInstance

        :param ParticipantInstance.Status status: Set to disconnected to remove participant.

        :returns: Updated ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantInstancer�  h�hMK	�r�  h ]r�  }r   (h#}r  (h%h&hh�u}r  (h%h�hhh�X   values.unsetr  u�r  h(h7uauuj�  }r  (hhh}r  (hX�   
        Fetch a ParticipantInstance

        :returns: Fetched ParticipantInstance
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantInstancer  h�hK�K	�r  h ]r	  }r
  (h#}r  (h%h&hh�u�r  h(h7uauuhL}r  (hhNh}r  h}r  (hhQh}r  hhJsu�r  suj,  }r  (hhQh}r  hh*suX   _published_tracksr  }r  (hhNh}r  h}r  (hhQh}r  hhosu}r  (hhQh}r  hh�su�r  suX   _subscribed_tracksr  }r  (hhNh}r  h}r  (hhQh}r   hhosu}r!  (hhQh}r"  hj6  su�r#  suhZ}r$  (hhQh}r%  hhDsuuhhth�hK�K�r&  uuX   Pager'  }r(  (hh}h]r)  hauuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r*  X   childrenr+  ]r,  (h�h�eX   filenamer-  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\video\v1\room\room_participant\__init__.pyr.  u.