�}q (X   membersq}q(X   BindingContextq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X+   twilio.rest.ip_messaging.v2.service.bindingqh�qX   twilio.base.instance_contextqX   InstanceContextq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docqX�  
        Initialize the BindingContext

        :param Version version: Version that contains the resource
        :param service_sid: The SID of the Service to fetch the resource from
        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.service.binding.BindingContext
        :rtype: twilio.rest.chat.v2.service.binding.BindingContextqX   builtinq�X   locationqK�K	�qX	   overloadsq]q }q!(X   argsq"(}q#(X   nameq$X   selfq%hhu}q&(h$X   versionq'hX   twilio.rest.ip_messaging.v2q(X   V2q)�q*u}q+(h$X   service_sidq,hhX   NoneTypeq-�q.u}q/(h$X   sidq0hh.utq1X   ret_typeq2NuauuX   __repr__q3}q4(hhh}q5(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq6h�hMK	�q7h]q8}q9(h"}q:(h$h%hhu�q;h2hX   strq<�q=uauuX   deleteq>}q?(hhh}q@(hXu   
        Deletes the BindingInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolqAh�hK�K	�qBh]qC}qD(h"}qE(h$h%hhu�qFh2NuauuX   fetchqG}qH(hhh}qI(hX�   
        Fetch a BindingInstance

        :returns: Fetched BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingInstanceqJh�hK�K	�qKh]qL}qM(h"}qN(h$h%hhu�qOh2hX   BindingInstanceqP�qQuauuX	   _solutionqR}qS(hX   multipleqTh}qUh}qV(hX   dataqWh}qXhhX   dictqY�qZsu�q[suX   _uriq\}q](hhWh}q^hh=suX   _versionq_}q`(hhWh}qahh*suuhX    qbh�hK�K�qcuuX	   serializeqd}qe(hX	   modulerefqfhX   twilio.base.serializeqghb�qhuX   ListResourceqi}qj(hX   typerefqkh]qlX   twilio.base.list_resourceqmX   ListResourceqn�qoauX   InstanceResourceqp}qq(hhkh]qrX   twilio.base.instance_resourceqsX   InstanceResourceqt�quauX   InstanceContextqv}qw(hhkh]qxhauX   Pageqy}qz(hhkh]q{X   twilio.base.pageq|X   Pageq}�q~auX   BindingListq}q�(hhh}q�(h	]q�(hX   BindingListq��q�hoheh]q�hoah}q�(h}q�(hhh}q�(hXB  
        Initialize the BindingList

        :param Version version: Version that contains the resource
        :param service_sid: The SID of the Service that the resource is associated with

        :returns: twilio.rest.chat.v2.service.binding.BindingList
        :rtype: twilio.rest.chat.v2.service.binding.BindingListq�h�hKK	�q�h]q�}q�(h"}q�(h$h%hh�u}q�(h$h'hh*u}q�(h$h,hh.u�q�h2NuauuX   streamq�}q�(hhh}q�(hXx  
        Streams BindingInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param BindingInstance.BindingType binding_type: The push technology used by the Binding resources to read
        :param unicode identity: The `identity` value of the resources to read
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v2.service.binding.BindingInstance]q�h�hK%K	�q�h]q�}q�(h"(}q�(h$h%hh�u}q�(h$X   binding_typeq�hhX   default_valueq�X   values.unsetq�u}q�(h$X   identityq�hhh�X   values.unsetq�u}q�(h$X   limitq�hh.h�X   Noneq�u}q�(h$X	   page_sizeq�hh.h�h�utq�h2hX	   generatorq��q�uauuX   listq�}q�(hhh}q�(hX  
        Lists BindingInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param BindingInstance.BindingType binding_type: The push technology used by the Binding resources to read
        :param unicode identity: The `identity` value of the resources to read
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v2.service.binding.BindingInstance]q�h�hK?K	�q�h]q�}q�(h"(}q�(h$h%hh�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hh.h�h�u}q�(h$h�hh.h�h�utq�h2hX   listq�]q�Na�q�uauuX   getq�}q�(hhh}q�(hX�   
        Constructs a BindingContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.service.binding.BindingContext
        :rtype: twilio.rest.chat.v2.service.binding.BindingContextq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hh�u}q�(h$h0hNu�q�h2huauuh3}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hh�u�q�h2h=uauuX   pageq�}q�(hhh}q�(hXx  
        Retrieve a single page of BindingInstance records from the API.
        Request is executed immediately

        :param BindingInstance.BindingType binding_type: The push technology used by the Binding resources to read
        :param unicode identity: The `identity` value of the resources to read
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingPageq�h�hKYK	�q�h]q�}q�(h"(}q�(h$h%hh�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$h�hhh�X   values.unsetq�u}q�(h$X
   page_tokenq�hhh�X   values.unsetq�u}q�(h$X   page_numberq�hhh�X   values.unsetq�u}q�(h$h�h]q�(hh.eh�X   values.unsetq�utq�h2hX   BindingPageq�q�uauuX   get_pageq�}q�(hhh}q�(hX.  
        Retrieve a specific page of BindingInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingPageq�h�hKyK	�q�h]q�}q�(h"}q�(h$h%hh�u}q�(h$X
   target_urlq�hNu�q�h2h�uauuX   __call__q�}q�(hhh}q�(hX�   
        Constructs a BindingContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.service.binding.BindingContext
        :rtype: twilio.rest.chat.v2.service.binding.BindingContextq�h�hK�K	�q�h]q�}q�(h"}q�(h$h%hh�u}q�(h$h0hNu�q�h2huauuhR}q�(hhTh}q�h}q�(hhWh}q�hhZsu}q�(hhWh}q�hhZsu}q�(hhWh}q hhZsu�r  suh\}r  (hhWh}r  hh=suh_}r  (hhWh}r  hh*suuhhbh�hKK�r  uuh�}r  (hhh}r  (h	]r	  (h�h~heh]r
  h~ah}r  (h3}r  (hhh}r  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr  h�hK�K	�r  h]r  }r  (h"}r  (h$h%hh�u�r  h2h=uauuX   get_instancer  }r  (hhh}r  (hX�   
        Build an instance of BindingInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.chat.v2.service.binding.BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingInstancer  h�hK�K	�r  h]r  }r  (h"}r  (h$h%hh�u}r  (h$X   payloadr  hNu�r  h2hQuauuh}r  (hhh}r   (hXz  
        Initialize the BindingPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param service_sid: The SID of the Service that the resource is associated with

        :returns: twilio.rest.chat.v2.service.binding.BindingPage
        :rtype: twilio.rest.chat.v2.service.binding.BindingPager!  h�hK�K	�r"  h]r#  }r$  (h"(}r%  (h$h%hh�u}r&  (h$h'hh*u}r'  (h$X   responser(  hNu}r)  (h$X   solutionr*  hhZutr+  h2NuauuhR}r,  (hhTh}r-  h(}r.  (hhWh}r/  hhZsu}r0  (hhWh}r1  hhZsu}r2  (hhWh}r3  hhZsu}r4  (hhWh}r5  hhZsutr6  suh_}r7  (hhWh}r8  hh*suX   _payloadr9  }r:  (hhTh}r;  h(}r<  (hhWh}r=  hhX   floatr>  �r?  su}r@  (hhWh}rA  hhX   boolrB  �rC  su}rD  (hhWh}rE  hhX   intrF  �rG  su}rH  (hhWh}rI  hh.sutrJ  suX   _recordsrK  }rL  (hhWh}rM  hNsuuhhbh�hK�K�rN  uuX   deserializerO  }rP  (hhfhX   twilio.base.deserializerQ  hb�rR  uX   valuesrS  }rT  (hhfhX   twilio.base.valuesrU  hb�rV  uX   BindingInstancerW  }rX  (hhh}rY  (h	]rZ  (hQhuheh]r[  huah}r\  (h}r]  (hhh}r^  (hX�   
        Initialize the BindingInstance

        :returns: twilio.rest.chat.v2.service.binding.BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingInstancer_  h�hMK	�r`  h]ra  }rb  (h"(}rc  (h$h%hhQu}rd  (h$h'hh*u}re  (h$j  h]rf  (jC  h.j?  jG  eu}rg  (h$h,hh.u}rh  (h$h0hh.h�h�utri  h2NuauuX   service_sidrj  }rk  (hX   propertyrl  h}rm  (hXf   
        :returns: The SID of the Service that the resource is associated with
        :rtype: unicodern  hNhMWK	�ro  uuX   credential_sidrp  }rq  (hjl  h}rr  (hXT   
        :returns: The SID of the Credential for the binding
        :rtype: unicoders  hNhMK	�rt  uuX   sidru  }rv  (hjl  h}rw  (hXY   
        :returns: The unique string that identifies the resource
        :rtype: unicoderx  hNhMGK	�ry  uuX   urlrz  }r{  (hjl  h}r|  (hXS   
        :returns: The absolute URL of the Binding resource
        :rtype: unicoder}  hNhM�K	�r~  uuX   linksr  }r�  (hjl  h}r�  (hXY   
        :returns: The absolute URLs of the Users for the Binding
        :rtype: unicoder�  hNhM�K	�r�  uuX   identityr�  }r�  (hjl  h}r�  (hXY   
        :returns: The string that identifies the resource's User
        :rtype: unicoder�  hNhMwK	�r�  uuX   message_typesr�  }r�  (hjl  h}r�  (hXk   
        :returns: The Programmable Chat message types the binding is subscribed to
        :rtype: unicoder�  hNhM�K	�r�  uuh3}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hM�K	�r�  h]r�  }r�  (h"}r�  (h$h%hhQu�r�  h2h=uauuhG}r�  (hhh}r�  (hX�   
        Fetch a BindingInstance

        :returns: Fetched BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingInstancer�  h�hM�K	�r�  h]r�  }r�  (h"}r�  (h$h%hhQu�r�  h2hQuauuX   date_createdr�  }r�  (hjl  h}r�  (hXk   
        :returns: The RFC 2822 date and time in GMT when the resource was created
        :rtype: datetimer�  hNhM_K	�r�  uuX   BindingTyper�  }r�  (hhkh]r�  hX   BindingTyper�  �r�  auX   account_sidr�  }r�  (hjl  h}r�  (hX[   
        :returns: The SID of the Account that created the resource
        :rtype: unicoder�  hNhMOK	�r�  uuh>}r�  (hhh}r�  (hXu   
        Deletes the BindingInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h�hM�K	�r�  h]r�  }r�  (h"}r�  (h$h%hhQu�r�  h2NuauuX   endpointr�  }r�  (hjl  h}r�  (hXY   
        :returns: The unique endpoint identifier for the Binding
        :rtype: unicoder�  hNhMoK	�r�  uuX   binding_typer�  }r�  (hjl  h}r�  (hXi   
        :returns: The push technology to use for the binding
        :rtype: BindingInstance.BindingTyper�  hNhM�K	�r�  uuX   _proxyr�  }r�  (hjl  h}r�  (hX$  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: BindingContext for this BindingInstance
        :rtype: twilio.rest.chat.v2.service.binding.BindingContextr�  h]r�  (hh.ehM6K	�r�  uuX   date_updatedr�  }r�  (hjl  h}r�  (hXp   
        :returns: The RFC 2822 date and time in GMT when the resource was last updated
        :rtype: datetimer�  hNhMgK	�r�  uuX   _propertiesr�  }r�  (hhTh}r�  h(}r�  (hhWh}r�  hhZsu}r�  (hhWh}r�  hhZsu}r�  (hhWh}r�  hhZsu}r�  (hhWh}r�  hhZsutr�  suX   _contextr�  }r�  (hhTh}r�  h}r�  (hhWh}r�  hh.su}r�  (hhWh}r�  hhsu�r�  suhR}r�  (hhTh}r�  h(}r�  (hhWh}r�  hhZsu}r�  (hhWh}r�  hhZsu}r�  (hhWh}r�  hhZsu}r�  (hhWh}r�  hhZsutr�  suh_}r�  (hhWh}r�  hh*suuhhbh�hMK�r�  uuuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  X   filenamer�  X|   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\ip_messaging\v2\service\binding.pyr�  u.