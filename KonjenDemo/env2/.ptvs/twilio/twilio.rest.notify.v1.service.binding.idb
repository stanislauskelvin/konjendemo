�}q (X   membersq}q(X   BindingContextq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X%   twilio.rest.notify.v1.service.bindingqX   BindingContextq�qX   twilio.base.instance_contextqX   InstanceContextq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docqX?  
        Initialize the BindingContext

        :param Version version: Version that contains the resource
        :param service_sid: The service_sid
        :param sid: The sid

        :returns: twilio.rest.notify.v1.service.binding.BindingContext
        :rtype: twilio.rest.notify.v1.service.binding.BindingContextqX   builtinq�X   locationqMK	�qX	   overloadsq ]q!}q"(X   argsq#(}q$(X   nameq%X   selfq&hhu}q'(h%X   versionq(hX   twilio.rest.notify.v1q)X   V1q*�q+u}q,(h%X   service_sidq-hhX   NoneTypeq.�q/u}q0(h%X   sidq1hh/utq2X   ret_typeq3NuauuX   __repr__q4}q5(hhh}q6(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq7h�hM;K	�q8h ]q9}q:(h#}q;(h%h&hhu�q<h3hX   strq=�q>uauuX   deleteq?}q@(hhh}qA(hXu   
        Deletes the BindingInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolqBh�hM2K	�qCh ]qD}qE(h#}qF(h%h&hhu�qGh3NuauuX   fetchqH}qI(hhh}qJ(hX�   
        Fetch a BindingInstance

        :returns: Fetched BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingInstanceqKh�hMK	�qLh ]qM}qN(h#}qO(h%h&hhu�qPh3hX   BindingInstanceqQ�qRuauuX	   _solutionqS}qT(hX   multipleqUh}qVh}qW(hX   dataqXh}qYhhX   dictqZ�q[su�q\suX   _uriq]}q^(hhXh}q_hh>suX   _versionq`}qa(hhXh}qbhh+suuhXj    PLEASE NOTE that this class contains beta products that are subject to
    change. Use them with caution.qch�hMK�qduuX	   serializeqe}qf(hX	   modulerefqghX   twilio.base.serializeqhX    qi�qjuX   ListResourceqk}ql(hX   typerefqmh]qnX   twilio.base.list_resourceqoX   ListResourceqp�qqauX   InstanceResourceqr}qs(hhmh]qtX   twilio.base.instance_resourcequX   InstanceResourceqv�qwauX   InstanceContextqx}qy(hhmh]qzhauX   Pageq{}q|(hhmh]q}X   twilio.base.pageq~X   Pageq�q�auX   BindingListq�}q�(hhh}q�(h	]q�(hh��q�hqheh]q�hqah}q�(h}q�(hhh}q�(hX  
        Initialize the BindingList

        :param Version version: Version that contains the resource
        :param service_sid: The service_sid

        :returns: twilio.rest.notify.v1.service.binding.BindingList
        :rtype: twilio.rest.notify.v1.service.binding.BindingListq�h�hKK	�q�h ]q�}q�(h#}q�(h%h&hh�u}q�(h%h(hh+u}q�(h%h-hh/u�q�h3NuauuX   streamq�}q�(hhh}q�(hX  
        Streams BindingInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param date start_date: Only list Bindings created on or after the given date.
        :param date end_date: Only list Bindings created on or before the given date.
        :param unicode identity: Only list Bindings that have any of the specified Identities.
        :param unicode tag: Only list Bindings that have all of the specified Tags.
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.notify.v1.service.binding.BindingInstance]q�h�hKIK	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%X
   start_dateq�hhX   default_valueq�X   values.unsetq�u}q�(h%X   end_dateq�hhh�X   values.unsetq�u}q�(h%X   identityq�hhh�X   values.unsetq�u}q�(h%X   tagq�hhh�X   values.unsetq�u}q�(h%X   limitq�hh/h�X   Noneq�u}q�(h%X	   page_sizeq�hh/h�h�utq�h3hX	   generatorq��q�uauuX   createq�}q�(hhh}q�(hX�  
        Create a new BindingInstance

        :param unicode identity: The Identity to which this Binding belongs to.
        :param BindingInstance.BindingType binding_type: The type of the Binding.
        :param unicode address: The address specific to the channel.
        :param unicode tag: The list of tags associated with this Binding.
        :param unicode notification_protocol_version: The version of the protocol used to send the notification.
        :param unicode credential_sid: The unique identifier of the Credential resource to be used to send notifications to this Binding.
        :param unicode endpoint: DEPRECATED*

        :returns: Newly created BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingInstanceq�h�hK&K	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%h�hNu}q�(h%X   binding_typeq�hNu}q�(h%X   addressq�hNu}q�(h%h�hhh�X   values.unsetq�u}q�(h%X   notification_protocol_versionq�hhh�X   values.unsetq�u}q�(h%X   credential_sidq�hhh�X   values.unsetq�u}q�(h%X   endpointq�hhh�X   values.unsetq�utq�h3hRuauuX   getq�}q�(hhh}q�(hX�   
        Constructs a BindingContext

        :param sid: The sid

        :returns: twilio.rest.notify.v1.service.binding.BindingContext
        :rtype: twilio.rest.notify.v1.service.binding.BindingContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hh�u}q�(h%h1hNu�q�h3huauuh4}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hh�u�q�h3h>uauuX   pageq�}q�(hhh}q�(hX  
        Retrieve a single page of BindingInstance records from the API.
        Request is executed immediately

        :param date start_date: Only list Bindings created on or after the given date.
        :param date end_date: Only list Bindings created on or before the given date.
        :param unicode identity: Only list Bindings that have any of the specified Identities.
        :param unicode tag: Only list Bindings that have all of the specified Tags.
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingPageq�h�hK�K	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%h�hhh�X   values.unsetq�u}q�(h%X
   page_tokenq�hhh�X   values.unsetq�u}q�(h%X   page_numberq�hhh�X   values.unsetq�u}q�(h%h�h]q�(hh/eh�X   values.unsetq�utq�h3hX   BindingPageq�q�uauuX   listq�}q�(hhh}q�(hX�  
        Lists BindingInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param date start_date: Only list Bindings created on or after the given date.
        :param date end_date: Only list Bindings created on or before the given date.
        :param unicode identity: Only list Bindings that have any of the specified Identities.
        :param unicode tag: Only list Bindings that have all of the specified Tags.
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.notify.v1.service.binding.BindingInstance]q�h�hKkK	�q�h ]q�}q�(h#(}q�(h%h&hh�u}q�(h%h�hhh�X   values.unsetq�u}q (h%h�hhh�X   values.unsetr  u}r  (h%h�hhh�X   values.unsetr  u}r  (h%h�hhh�X   values.unsetr  u}r  (h%h�hh/h�h�u}r  (h%h�hh/h�h�utr  h3hX   listr	  ]r
  Na�r  uauuX   get_pager  }r  (hhh}r  (hX0  
        Retrieve a specific page of BindingInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingPager  h�hK�K	�r  h ]r  }r  (h#}r  (h%h&hh�u}r  (h%X
   target_urlr  hNu�r  h3h�uauuX   __call__r  }r  (hhh}r  (hX�   
        Constructs a BindingContext

        :param sid: The sid

        :returns: twilio.rest.notify.v1.service.binding.BindingContext
        :rtype: twilio.rest.notify.v1.service.binding.BindingContextr  h�hK�K	�r  h ]r  }r  (h#}r  (h%h&hh�u}r  (h%h1hNu�r   h3huauuhS}r!  (hhUh}r"  h}r#  (hhXh}r$  hh[su}r%  (hhXh}r&  hh[su}r'  (hhXh}r(  hh[su�r)  suh]}r*  (hhXh}r+  hh>suh`}r,  (hhXh}r-  hh+suuhhch�hKK�r.  uuX   BindingPager/  }r0  (hhh}r1  (h	]r2  (h�h�heh]r3  h�ah}r4  (h4}r5  (hhh}r6  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr7  h�hK�K	�r8  h ]r9  }r:  (h#}r;  (h%h&hh�u�r<  h3h>uauuX   get_instancer=  }r>  (hhh}r?  (hX�   
        Build an instance of BindingInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.notify.v1.service.binding.BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingInstancer@  h�hK�K	�rA  h ]rB  }rC  (h#}rD  (h%h&hh�u}rE  (h%X   payloadrF  hNu�rG  h3hRuauuh}rH  (hhh}rI  (hXR  
        Initialize the BindingPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param service_sid: The service_sid

        :returns: twilio.rest.notify.v1.service.binding.BindingPage
        :rtype: twilio.rest.notify.v1.service.binding.BindingPagerJ  h�hK�K	�rK  h ]rL  }rM  (h#(}rN  (h%h&hh�u}rO  (h%h(hh+u}rP  (h%X   responserQ  hNu}rR  (h%X   solutionrS  hh[utrT  h3NuauuhS}rU  (hhUh}rV  h(}rW  (hhXh}rX  hh[su}rY  (hhXh}rZ  hh[su}r[  (hhXh}r\  hh[su}r]  (hhXh}r^  hh[sutr_  suh`}r`  (hhXh}ra  hh+suX   _payloadrb  }rc  (hhUh}rd  h(}re  (hhXh}rf  hhX   floatrg  �rh  su}ri  (hhXh}rj  hhX   boolrk  �rl  su}rm  (hhXh}rn  hhX   intro  �rp  su}rq  (hhXh}rr  hh/sutrs  suX   _recordsrt  }ru  (hhXh}rv  hNsuuhhch�hK�K�rw  uuX   deserializerx  }ry  (hhghX   twilio.base.deserializerz  hi�r{  uX   valuesr|  }r}  (hhghX   twilio.base.valuesr~  hi�r  uhQ}r�  (hhh}r�  (h	]r�  (hRhwheh]r�  hwah}r�  (h}r�  (hhh}r�  (hX�   
        Initialize the BindingInstance

        :returns: twilio.rest.notify.v1.service.binding.BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingInstancer�  h�hMRK	�r�  h ]r�  }r�  (h#(}r�  (h%h&hhRu}r�  (h%h(hh+u}r�  (h%jF  h]r�  (jl  h/jh  jp  eu}r�  (h%h-hh/u}r�  (h%h1hh/h�h�utr�  h3NuauuX   service_sidr�  }r�  (hX   propertyr�  h}r�  (hX:   
        :returns: The service_sid
        :rtype: unicoder�  hNhM�K	�r�  uuh�}r�  (hj�  h}r�  (hXO   
        :returns: The address specific to the channel.
        :rtype: unicoder�  hNhM�K	�r�  uuX   credential_sidr�  }r�  (hj�  h}r�  (hX�   
        :returns: The unique identifier of the Credential resource to be used to send notifications to this Binding.
        :rtype: unicoder�  hNhM�K	�r�  uuX   sidr�  }r�  (hj�  h}r�  (hX2   
        :returns: The sid
        :rtype: unicoder�  hNhM�K	�r�  uuX   urlr�  }r�  (hj�  h}r�  (hX2   
        :returns: The url
        :rtype: unicoder�  hNhM�K	�r�  uuX   linksr�  }r�  (hj�  h}r�  (hX4   
        :returns: The links
        :rtype: unicoder�  hNhM�K	�r�  uuX   identityr�  }r�  (hj�  h}r�  (hXY   
        :returns: The Identity to which this Binding belongs to.
        :rtype: unicoder�  hNhM�K	�r�  uuX   tagsr�  }r�  (hj�  h}r�  (hXY   
        :returns: The list of tags associated with this Binding.
        :rtype: unicoder�  hNhM�K	�r�  uuX   date_updatedr�  }r�  (hj�  h}r�  (hX<   
        :returns: The date_updated
        :rtype: datetimer�  hNhM�K	�r�  uuh4}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hMK	�r�  h ]r�  }r�  (h#}r�  (h%h&hhRu�r�  h3h>uauuhH}r�  (hhh}r�  (hX�   
        Fetch a BindingInstance

        :returns: Fetched BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hhRu�r�  h3hRuauuX   date_createdr�  }r�  (hj�  h}r�  (hX<   
        :returns: The date_created
        :rtype: datetimer�  hNhM�K	�r�  uuX   BindingTyper�  }r�  (hhmh]r�  hX   BindingTyper�  �r�  auX   account_sidr�  }r�  (hj�  h}r�  (hX:   
        :returns: The account_sid
        :rtype: unicoder�  hNhM�K	�r�  uuh?}r�  (hhh}r�  (hXu   
        Deletes the BindingInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hhRu�r�  h3Nuauuh�}r�  (hj�  h}r�  (hX6   
        :returns: DEPRECATED*
        :rtype: unicoder�  hNhM�K	�r�  uuX   binding_typer�  }r�  (hj�  h}r�  (hXC   
        :returns: The type of the Binding.
        :rtype: unicoder�  hNhM�K	�r�  uuX   _proxyr�  }r�  (hj�  h}r�  (hX&  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: BindingContext for this BindingInstance
        :rtype: twilio.rest.notify.v1.service.binding.BindingContextr�  h]r�  (h/hehMrK	�r�  uuh�}r�  (hj�  h}r�  (hXe   
        :returns: The version of the protocol used to send the notification.
        :rtype: unicoder�  hNhM�K	�r�  uuX   _propertiesr�  }r�  (hhUh}r�  h}r�  (hhXh}r�  hh[su�r�  suX   _contextr�  }r   (hhUh}r  h}r  (hhXh}r  hh/su}r  (hhXh}r  hhsu�r  suhS}r  (hhUh}r  h}r	  (hhXh}r
  hh[su�r  suh`}r  (hhXh}r  hh+suuhhch�hMFK�r  uuuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r  X   childrenr  ]r  X   filenamer  Xv   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\notify\v1\service\binding.pyr  u.