�}q (X   membersq}q(X   verificationq}q(X   kindqX	   modulerefqX   valueqX*   twilio.rest.verify.v2.service.verificationqX    q	�q
uX   ServiceContextq}q(hX   typeqh}q(X   mroq]q(X   twilio.rest.verify.v2.serviceqX   ServiceContextq�qX   twilio.base.instance_contextqX   InstanceContextq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q (X   docq!X*  
        Initialize the ServiceContext

        :param Version version: Version that contains the resource
        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.verify.v2.service.ServiceContext
        :rtype: twilio.rest.verify.v2.service.ServiceContextq"X   builtinq#�X   locationq$K�K	�q%X	   overloadsq&]q'}q((X   argsq)}q*(X   nameq+X   selfq,hhu}q-(h+X   versionq.hX   twilio.rest.verify.v2q/X   V2q0�q1u}q2(h+X   sidq3hhX   NoneTypeq4�q5u�q6X   ret_typeq7NuauuX   __repr__q8}q9(hhh}q:(h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq;h#�h$MQK	�q<h&]q=}q>(h)}q?(h+h,hhu�q@h7hX   strqA�qBuauuX   verification_checksqC}qD(hX   propertyqEh}qF(h!X�   
        Access the verification_checks

        :returns: twilio.rest.verify.v2.service.verification_check.VerificationCheckList
        :rtype: twilio.rest.verify.v2.service.verification_check.VerificationCheckListqGh]qH(X0   twilio.rest.verify.v2.service.verification_checkqIX   VerificationCheckListqJ�qKh5eh$MFK	�qLuuX   updateqM}qN(hhh}qO(h!XG  
        Update the ServiceInstance

        :param unicode friendly_name: A string to describe the verification service
        :param unicode code_length: The length of the verification code to generate
        :param bool lookup_enabled: Whether to perform a lookup with each verification
        :param bool skip_sms_to_landlines: Whether to skip sending SMS verifications to landlines
        :param bool dtmf_input_required: Whether to ask the user to press a number before delivering the verify code in a phone call
        :param unicode tts_name: The name of an alternative text-to-speech service to use in phone calls
        :param bool psd2_enabled: Whether to pass PSD2 transaction parameters when starting a verification

        :returns: Updated ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstanceqPh#�h$MK	�qQh&]qR}qS(h)(}qT(h+h,hhu}qU(h+X   friendly_nameqVhhX   default_valueqWX   values.unsetqXu}qY(h+X   code_lengthqZhhhWX   values.unsetq[u}q\(h+X   lookup_enabledq]hhhWX   values.unsetq^u}q_(h+X   skip_sms_to_landlinesq`hhhWX   values.unsetqau}qb(h+X   dtmf_input_requiredqchhhWX   values.unsetqdu}qe(h+X   tts_nameqfhhhWX   values.unsetqgu}qh(h+X   psd2_enabledqihhhWX   values.unsetqjutqkh7hX   ServiceInstanceql�qmuauuX   verificationsqn}qo(hhEh}qp(h!X�   
        Access the verifications

        :returns: twilio.rest.verify.v2.service.verification.VerificationList
        :rtype: twilio.rest.verify.v2.service.verification.VerificationListqqh]qr(h5hX   VerificationListqs�qteh$M:K	�quuuX   deleteqv}qw(hhh}qx(h!Xu   
        Deletes the ServiceInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolqyh#�h$MK	�qzh&]q{}q|(h)}q}(h+h,hhu�q~h7NuauuX   fetchq}q�(hhh}q�(h!X�   
        Fetch a ServiceInstance

        :returns: Fetched ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstanceq�h#�h$K�K	�q�h&]q�}q�(h)}q�(h+h,hhu�q�h7hmuauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su�q�suX   _uriq�}q�(hh�h}q�hhBsuX   _verificationsq�}q�(hh�h}q�h}q�(hh�h}q�hh5su}q�(hh�h}q�hhtsu�q�suX   _verification_checksq�}q�(hh�h}q�h}q�(hh�h}q�hh5su}q�(hh�h}q�hhKsu�q�suX   _versionq�}q�(hh�h}q�hh1suuh!Xj    PLEASE NOTE that this class contains beta products that are subject to
    change. Use them with caution.q�h#�h$K�K�q�uuX   ListResourceq�}q�(hX   typerefq�h]q�X   twilio.base.list_resourceq�X   ListResourceq��q�auX   valuesq�}q�(hhhX   twilio.base.valuesq�h	�q�uX   InstanceContextq�}q�(hh�h]q�hauX   verification_checkq�}q�(hhhhIh	�q�uX   ServiceListq�}q�(hhh}q�(h]q�(hX   ServiceListq��q�h�heh]q�h�ah}q�(h}q�(hhh}q�(h!X�   
        Initialize the ServiceList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.verify.v2.service.ServiceList
        :rtype: twilio.rest.verify.v2.service.ServiceListq�h#�h$KK	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+h.hh1u�q�h7NuauuX   streamq�}q�(hhh}q�(h!X�  
        Streams ServiceInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.verify.v2.service.ServiceInstance]q�h#�h$KJK	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+X   limitq�hh5hWX   Noneq�u}q�(h+X	   page_sizeq�hh5hWh�u�q�h7hX	   generatorqچq�uauuX   createq�}q�(hhh}q�(h!XO  
        Create a new ServiceInstance

        :param unicode friendly_name: A string to describe the verification service
        :param unicode code_length: The length of the verification code to generate
        :param bool lookup_enabled: Whether to perform a lookup with each verification
        :param bool skip_sms_to_landlines: Whether to skip sending SMS verifications to landlines
        :param bool dtmf_input_required: Whether to ask the user to press a number before delivering the verify code in a phone call
        :param unicode tts_name: The name of an alternative text-to-speech service to use in phone calls
        :param bool psd2_enabled: Whether to pass PSD2 transaction parameters when starting a verification

        :returns: Newly created ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstanceq�h#�h$K&K	�q�h&]q�}q�(h)(}q�(h+h,hh�u}q�(h+hVhNu}q�(h+hZhhhWX   values.unsetq�u}q�(h+h]hhhWX   values.unsetq�u}q�(h+h`hhhWX   values.unsetq�u}q�(h+hchhhWX   values.unsetq�u}q�(h+hfhhhWX   values.unsetq�u}q�(h+hihhhWX   values.unsetq�utq�h7hmuauuX   getq�}q�(hhh}q�(h!X�   
        Constructs a ServiceContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.verify.v2.service.ServiceContext
        :rtype: twilio.rest.verify.v2.service.ServiceContextq�h#�h$K�K	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+h3hNu�q�h7huauuh8}q�(hhh}q�(h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h#�h$K�K	�q�h&]q }r  (h)}r  (h+h,hh�u�r  h7hBuauuX   pager  }r  (hhh}r  (h!X�  
        Retrieve a single page of ServiceInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServicePager  h#�h$KsK	�r  h&]r	  }r
  (h)(}r  (h+h,hh�u}r  (h+X
   page_tokenr  hhhWX   values.unsetr  u}r  (h+X   page_numberr  hhhWX   values.unsetr  u}r  (h+h�h]r  (hh5ehWX   values.unsetr  utr  h7hX   ServicePager  �r  uauuX   listr  }r  (hhh}r  (h!XP  
        Lists ServiceInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.verify.v2.service.ServiceInstance]r  h#�h$KaK	�r  h&]r  }r  (h)}r  (h+h,hh�u}r   (h+h�hh5hWh�u}r!  (h+h�hh5hWh�u�r"  h7hX   listr#  ]r$  Na�r%  uauuX   get_pager&  }r'  (hhh}r(  (h!X(  
        Retrieve a specific page of ServiceInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServicePager)  h#�h$K�K	�r*  h&]r+  }r,  (h)}r-  (h+h,hh�u}r.  (h+X
   target_urlr/  hNu�r0  h7j  uauuX   __call__r1  }r2  (hhh}r3  (h!X�   
        Constructs a ServiceContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.verify.v2.service.ServiceContext
        :rtype: twilio.rest.verify.v2.service.ServiceContextr4  h#�h$K�K	�r5  h&]r6  }r7  (h)}r8  (h+h,hh�u}r9  (h+h3hNu�r:  h7huauuh�}r;  (hh�h}r<  h}r=  (hh�h}r>  hh�su}r?  (hh�h}r@  hh�su}rA  (hh�h}rB  hh�su�rC  suh�}rD  (hh�h}rE  hhBsuh�}rF  (hh�h}rG  hh1suuh!h�h#�h$KK�rH  uuX   PagerI  }rJ  (hh�h]rK  X   twilio.base.pagerL  X   PagerM  �rN  auX   ServiceInstancerO  }rP  (hhh}rQ  (h]rR  (hmX   twilio.base.instance_resourcerS  X   InstanceResourcerT  �rU  heh]rV  jU  ah}rW  (X   friendly_namerX  }rY  (hhEh}rZ  (h!Xl   
        :returns: The string that you assigned to describe the verification service
        :rtype: unicoder[  hNh$M�K	�r\  uuhn}r]  (hhEh}r^  (h!hqh]r_  (hth5eh$M"K	�r`  uuh}ra  (hhh}rb  (h!X�   
        Initialize the ServiceInstance

        :returns: twilio.rest.verify.v2.service.ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstancerc  h#�h$M`K	�rd  h&]re  }rf  (h)(}rg  (h+h,hhmu}rh  (h+h.hh1u}ri  (h+X   payloadrj  h]rk  (hX   boolrl  �rm  h5hX   floatrn  �ro  hX   intrp  �rq  eu}rr  (h+h3hh5hWh�utrs  h7Nuauuhi}rt  (hhEh}ru  (h!Xp   
        :returns: Whether to pass PSD2 transaction parameters when starting a verification
        :rtype: boolrv  hNh$M�K	�rw  uuX   urlrx  }ry  (hhEh}rz  (h!XK   
        :returns: The absolute URL of the resource
        :rtype: unicoder{  hNh$M�K	�r|  uuX   linksr}  }r~  (hhEh}r  (h!XH   
        :returns: The URLs of related resources
        :rtype: unicoder�  hNh$M�K	�r�  uuhf}r�  (hhEh}r�  (h!Xr   
        :returns: The name of an alternative text-to-speech service to use in phone calls
        :rtype: unicoder�  hNh$M�K	�r�  uuhv}r�  (hhh}r�  (h!Xu   
        Deletes the ServiceInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h#�h$M�K	�r�  h&]r�  }r�  (h)}r�  (h+h,hhmu�r�  h7Nuauuhc}r�  (hhEh}r�  (h!X�   
        :returns: Whether to ask the user to press a number before delivering the verify code in a phone call
        :rtype: boolr�  hNh$M�K	�r�  uuh8}r�  (hhh}r�  (h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h#�h$M5K	�r�  h&]r�  }r�  (h)}r�  (h+h,hhmu�r�  h7hBuauuhC}r�  (hhEh}r�  (h!hGh]r�  (hKh5eh$M,K	�r�  uuh`}r�  (hhEh}r�  (h!X^   
        :returns: Whether to skip sending SMS verifications to landlines
        :rtype: boolr�  hNh$M�K	�r�  uuh}r�  (hhh}r�  (h!X�   
        Fetch a ServiceInstance

        :returns: Fetched ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstancer�  h#�h$M�K	�r�  h&]r�  }r�  (h)}r�  (h+h,hhmu�r�  h7hmuauuX   date_createdr�  }r�  (hhEh}r�  (h!Xk   
        :returns: The RFC 2822 date and time in GMT when the resource was created
        :rtype: datetimer�  hNh$M�K	�r�  uuX   sidr�  }r�  (hhEh}r�  (h!XY   
        :returns: The unique string that identifies the resource
        :rtype: unicoder�  hNh$M�K	�r�  uuX   account_sidr�  }r�  (hhEh}r�  (h!X[   
        :returns: The SID of the Account that created the resource
        :rtype: unicoder�  hNh$M�K	�r�  uuhM}r�  (hhh}r�  (h!XG  
        Update the ServiceInstance

        :param unicode friendly_name: A string to describe the verification service
        :param unicode code_length: The length of the verification code to generate
        :param bool lookup_enabled: Whether to perform a lookup with each verification
        :param bool skip_sms_to_landlines: Whether to skip sending SMS verifications to landlines
        :param bool dtmf_input_required: Whether to ask the user to press a number before delivering the verify code in a phone call
        :param unicode tts_name: The name of an alternative text-to-speech service to use in phone calls
        :param bool psd2_enabled: Whether to pass PSD2 transaction parameters when starting a verification

        :returns: Updated ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstancer�  h#�h$MK	�r�  h&]r�  }r�  (h)(}r�  (h+h,hhmu}r�  (h+hVhhhWX   values.unsetr�  u}r�  (h+hZhhhWX   values.unsetr�  u}r�  (h+h]hhhWX   values.unsetr�  u}r�  (h+h`hhhWX   values.unsetr�  u}r�  (h+hchhhWX   values.unsetr�  u}r�  (h+hfhhhWX   values.unsetr�  u}r�  (h+hihhhWX   values.unsetr�  utr�  h7hmuauuhZ}r�  (hhEh}r�  (h!XN   
        :returns: The length of the verification code
        :rtype: unicoder�  hNh$M�K	�r�  uuX   _proxyr�  }r�  (hhEh}r�  (h!X  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: ServiceContext for this ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceContextr�  h]r�  (hh5eh$MK	�r�  uuh]}r�  (hhEh}r�  (h!XZ   
        :returns: Whether to perform a lookup with each verification
        :rtype: boolr�  hNh$M�K	�r�  uuX   date_updatedr�  }r�  (hhEh}r�  (h!Xp   
        :returns: The RFC 2822 date and time in GMT when the resource was last updated
        :rtype: datetimer�  hNh$M�K	�r�  uuX   _propertiesr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh5su}r�  (hh�h}r�  hhsu�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hh1suuh!h�h#�h$M\K�r�  uuX   ServicePager�  }r�  (hhh}r�  (h]r�  (j  jN  heh]r�  jN  ah}r�  (h8}r�  (hhh}r�  (h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr   h#�h$K�K	�r  h&]r  }r  (h)}r  (h+h,hj  u�r  h7hBuauuX   get_instancer  }r  (hhh}r  (h!X�   
        Build an instance of ServiceInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.verify.v2.service.ServiceInstance
        :rtype: twilio.rest.verify.v2.service.ServiceInstancer	  h#�h$K�K	�r
  h&]r  }r  (h)}r  (h+h,hj  u}r  (h+jj  hNu�r  h7hmuauuh}r  (hhh}r  (h!X  
        Initialize the ServicePage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.verify.v2.service.ServicePage
        :rtype: twilio.rest.verify.v2.service.ServicePager  h#�h$K�K	�r  h&]r  }r  (h)(}r  (h+h,hj  u}r  (h+h.hh1u}r  (h+X   responser  hNu}r  (h+X   solutionr  hh�utr  h7Nuauuh�}r  (hh�h}r  h(}r  (hh�h}r   hh�su}r!  (hh�h}r"  hh�su}r#  (hh�h}r$  hh�su}r%  (hh�h}r&  hh�sutr'  suh�}r(  (hh�h}r)  hh1suX   _payloadr*  }r+  (hh�h}r,  h(}r-  (hh�h}r.  hjo  su}r/  (hh�h}r0  hjm  su}r1  (hh�h}r2  hjq  su}r3  (hh�h}r4  hh5sutr5  suX   _recordsr6  }r7  (hh�h}r8  hNsuuh!h�h#�h$K�K�r9  uuX   deserializer:  }r;  (hhhX   twilio.base.deserializer<  h	�r=  uX   InstanceResourcer>  }r?  (hh�h]r@  jU  auX   VerificationListrA  }rB  (hh�h]rC  htauX   VerificationCheckListrD  }rE  (hh�h]rF  hKauuh!X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /rG  X   childrenrH  ]rI  (hh�eX   filenamerJ  Xw   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\verify\v2\service\__init__.pyrK  u.