�}q (X   membersq}q(X	   serializeq}q(X   kindqX	   modulerefqX   valueqX   twilio.base.serializeqX    q	�q
uX   MonthlyListq}q(hX   typeqh}q(X   mroq]q(X2   twilio.rest.api.v2010.account.usage.record.monthlyqh�qX   twilio.base.list_resourceqX   ListResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docq Xb  
        Initialize the MonthlyList

        :param Version version: Version that contains the resource
        :param account_sid: A 34 character string that uniquely identifies this resource.

        :returns: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyList
        :rtype: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyListq!X   builtinq"�X   locationq#KK	�q$X	   overloadsq%]q&}q'(X   argsq(}q)(X   nameq*X   selfq+hhu}q,(h*X   versionq-hX   twilio.rest.api.v2010q.X   V2010q/�q0u}q1(h*X   account_sidq2hNu�q3X   ret_typeq4NuauuX   __repr__q5}q6(hhh}q7(h Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq8h"�h#K�K	�q9h%]q:}q;(h(}q<(h*h+hhu�q=h4hX   strq>�q?uauuX   listq@}qA(hhh}qB(h X�  
        Lists MonthlyInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param MonthlyInstance.Category category: The usage category of the UsageRecord resources to read
        :param date start_date: Only include usage that has occurred on or after this date
        :param date end_date: Only include usage that occurred on or before this date
        :param bool include_subaccounts: Whether to include usage from the master account and all its subaccounts
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.api.v2010.account.usage.record.monthly.MonthlyInstance]qCh"�h#KGK	�qDh%]qE}qF(h((}qG(h*h+hhu}qH(h*X   categoryqIhhX   default_valueqJX   values.unsetqKu}qL(h*X
   start_dateqMhhhJX   values.unsetqNu}qO(h*X   end_dateqPhhhJX   values.unsetqQu}qR(h*X   include_subaccountsqShhhJX   values.unsetqTu}qU(h*X   limitqVhhX   NoneTypeqW�qXhJX   NoneqYu}qZ(h*X	   page_sizeq[hhXhJhYutq\h4hX   listq]]q^Na�q_uauuX   streamq`}qa(hhh}qb(h XR  
        Streams MonthlyInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param MonthlyInstance.Category category: The usage category of the UsageRecord resources to read
        :param date start_date: Only include usage that has occurred on or after this date
        :param date end_date: Only include usage that occurred on or before this date
        :param bool include_subaccounts: Whether to include usage from the master account and all its subaccounts
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.api.v2010.account.usage.record.monthly.MonthlyInstance]qch"�h#K$K	�qdh%]qe}qf(h((}qg(h*h+hhu}qh(h*hIhhhJX   values.unsetqiu}qj(h*hMhhhJX   values.unsetqku}ql(h*hPhhhJX   values.unsetqmu}qn(h*hShhhJX   values.unsetqou}qp(h*hVhhXhJhYu}qq(h*h[hhXhJhYutqrh4hX	   generatorqs�qtuauuX   pagequ}qv(hhh}qw(h XR  
        Retrieve a single page of MonthlyInstance records from the API.
        Request is executed immediately

        :param MonthlyInstance.Category category: The usage category of the UsageRecord resources to read
        :param date start_date: Only include usage that has occurred on or after this date
        :param date end_date: Only include usage that occurred on or before this date
        :param bool include_subaccounts: Whether to include usage from the master account and all its subaccounts
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of MonthlyInstance
        :rtype: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyPageqxh"�h#KfK	�qyh%]qz}q{(h((}q|(h*h+hhu}q}(h*hIhhhJX   values.unsetq~u}q(h*hMhhhJX   values.unsetq�u}q�(h*hPhhhJX   values.unsetq�u}q�(h*hShhhJX   values.unsetq�u}q�(h*X
   page_tokenq�hhhJX   values.unsetq�u}q�(h*X   page_numberq�hhhJX   values.unsetq�u}q�(h*h[h]q�(hhXehJX   values.unsetq�utq�h4hX   MonthlyPageq��q�uauuX   get_pageq�}q�(hhh}q�(h X=  
        Retrieve a specific page of MonthlyInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of MonthlyInstance
        :rtype: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyPageq�h"�h#K�K	�q�h%]q�}q�(h(}q�(h*h+hhu}q�(h*X
   target_urlq�hNu�q�h4h�uauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hh?suX   _versionq�}q�(hh�h}q�hh0suuh h	h"�h#KK�q�uuX   InstanceResourceq�}q�(hX   typerefq�h]q�X   twilio.base.instance_resourceq�X   InstanceResourceq��q�auX   Pageq�}q�(hh�h]q�X   twilio.base.pageq�X   Pageq��q�auh�}q�(hhh}q�(h]q�(h�h�heh]q�h�ah}q�(h5}q�(hhh}q�(h Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h"�h#K�K	�q�h%]q�}q�(h(}q�(h*h+hh�u�q�h4h?uauuX   get_instanceq�}q�(hhh}q�(h X  
        Build an instance of MonthlyInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyInstance
        :rtype: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyInstanceq�h"�h#K�K	�q�h%]q�}q�(h(}q�(h*h+hh�u}q�(h*X   payloadq�hNu�q�h4hX   MonthlyInstanceqԆq�uauuh}q�(hhh}q�(h X�  
        Initialize the MonthlyPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param account_sid: A 34 character string that uniquely identifies this resource.

        :returns: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyPage
        :rtype: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyPageq�h"�h#K�K	�q�h%]q�}q�(h((}q�(h*h+hh�u}q�(h*h-hh0u}q�(h*X   responseq�h]q�(hXX   twilio.http.responseq�X   Responseq�q�eu}q�(h*X   solutionq�hh�utq�h4Nuauuh�}q�(hh�h}q�h}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su�q�suh�}q�(hh�h}q�hh0suX   _payloadq�}q�(hh�h}q�h(}q�(hh�h}q�hhX   floatq��q�su}q�(hh�h}q�hhX   boolq��q�su}q�(hh�h}q�hhX   intq��q su}r  (hh�h}r  hhXsutr  suX   _recordsr  }r  (hh�h}r  hNsuuh h	h"�h#K�K�r  uuX   ListResourcer  }r	  (hh�h]r
  hauX   deserializer  }r  (hhhX   twilio.base.deserializer  h	�r  uX   valuesr  }r  (hhhX   twilio.base.valuesr  h	�r  uh�}r  (hhh}r  (h]r  (h�h�heh]r  h�ah}r  (X
   start_dater  }r  (hX   propertyr  h}r  (h Xf   
        :returns: The first date for which usage is included in this UsageRecord
        :rtype: dater  hNh#M*K	�r  uuX   api_versionr  }r  (hj  h}r   (h XV   
        :returns: The API version used to create the resource
        :rtype: unicoder!  hNh#M�K	�r"  uuh}r#  (hhh}r$  (h X�   
        Initialize the MonthlyInstance

        :returns: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyInstance
        :rtype: twilio.rest.api.v2010.account.usage.record.monthly.MonthlyInstancer%  h"�h#M�K	�r&  h%]r'  }r(  (h((}r)  (h*h+hh�u}r*  (h*h-hh0u}r+  (h*h�hNu}r,  (h*h2hNutr-  h4NuauuX   descriptionr.  }r/  (hj  h}r0  (h X]   
        :returns: A plain-language description of the usage category
        :rtype: unicoder1  hNh#M
K	�r2  uuX   end_dater3  }r4  (hj  h}r5  (h Xd   
        :returns: The last date for which usage is included in the UsageRecord
        :rtype: dater6  hNh#MK	�r7  uuX   subresource_urisr8  }r9  (hj  h}r:  (h Xh   
        :returns: A list of related resources identified by their relative URIs
        :rtype: unicoder;  hNh#M2K	�r<  uuX   account_sidr=  }r>  (hj  h}r?  (h XS   
        :returns: The SID of the Account accrued the usage
        :rtype: unicoder@  hNh#M�K	�rA  uuX
   usage_unitrB  }rC  (hj  h}rD  (h XO   
        :returns: The units in which usage is measured
        :rtype: unicoderE  hNh#MJK	�rF  uuX   usagerG  }rH  (hj  h}rI  (h X>   
        :returns: The amount of usage
        :rtype: unicoderJ  hNh#MBK	�rK  uuX   categoryrL  }rM  (hj  h}rN  (h XQ   
        :returns: The category of usage
        :rtype: MonthlyInstance.CategoryrO  hNh#M�K	�rP  uuX   pricerQ  }rR  (hj  h}rS  (h XG   
        :returns: The total price of the usage
        :rtype: unicoderT  hNh#MK	�rU  uuh5}rV  (hhh}rW  (h Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strrX  h"�h#MQK	�rY  h%]rZ  }r[  (h(}r\  (h*h+hh�u�r]  h4h?uauuX   urir^  }r_  (hj  h}r`  (h Xh   
        :returns: The URI of the resource, relative to `https://api.twilio.com`
        :rtype: unicodera  hNh#M:K	�rb  uuX
   price_unitrc  }rd  (hj  h}re  (h XT   
        :returns: The currency in which `price` is measured
        :rtype: unicoderf  hNh#M"K	�rg  uuX   Categoryrh  }ri  (hh�h]rj  hX   Categoryrk  �rl  auX
   count_unitrm  }rn  (hj  h}ro  (h XO   
        :returns: The units in which count is measured
        :rtype: unicoderp  hNh#MK	�rq  uuX   countrr  }rs  (hj  h}rt  (h XE   
        :returns: The number of usage events
        :rtype: unicoderu  hNh#M�K	�rv  uuX   _propertiesrw  }rx  (hh�h}ry  h}rz  (hh�h}r{  hh�su}r|  (hh�h}r}  hh�su�r~  suX   _contextr  }r�  (hh�h}r�  hhXsuh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su�r�  suX   _versionr�  }r�  (hh�h}r�  hh0suuh h	h"�h#K�K�r�  uuuh X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  X   filenamer�  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env2\lib\site-packages\twilio\rest\api\v2010\account\usage\record\monthly.pyr�  u.