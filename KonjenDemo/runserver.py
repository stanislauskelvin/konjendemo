"""
This script runs the KonjenDemo application using a development server.
"""

from os import environ
from KonjenDemo import app, server
from threading import Thread
import os

#if __name__ == '__main__':
#    HOST = environ.get('SERVER_HOST', 'localhost')
#    try:
#        PORT = int(environ.get('SERVER_PORT', '5555'))
#    except ValueError:
#        PORT = 5555
#    print("PORT: " + str(PORT))
#    app.run(HOST, PORT)

def start_app():
    print('starting app')
    app.run(port=5001)

def start_server():
    print('starting server')
    server.run(port=5002, debug = True)


if __name__ == '__main__':
    if os.environ.get("WERKZEUG_RUN_MAIN") != 'true':
        Thread(target=start_app).start()
    start_server()
    

