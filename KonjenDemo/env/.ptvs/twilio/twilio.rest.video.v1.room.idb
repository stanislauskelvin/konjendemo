�}q (X   membersq}q(X   RoomInstanceq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X   twilio.rest.video.v1.roomqh�qX   twilio.base.instance_resourceqX   InstanceResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   unique_nameq}q(hX   propertyqh}q(X   docqXQ   
        :returns: A developer-supplied Name of the Room.
        :rtype: unicodeqhNX   locationqM�K	�quuX   __repr__q}q(hX   functionq h}q!(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq"X   builtinq#�hMeK	�q$X	   overloadsq%]q&}q'(X   argsq(}q)(X   nameq*X   selfq+hhu�q,X   ret_typeq-hX   strq.�q/uauuX   max_participantsq0}q1(hhh}q2(hXi   
        :returns: Maximum number of concurrent Participants allowed in the Room.
        :rtype: unicodeq3hNhMK	�q4uuX   status_callback_methodq5}q6(hhh}q7(hXg   
        :returns: HTTP method Twilio should use when requesting the above URL.
        :rtype: unicodeq8hNhM�K	�q9uuX   __init__q:}q;(hh h}q<(hX�   
        Initialize the RoomInstance

        :returns: twilio.rest.video.v1.room.RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstanceq=h#�hM}K	�q>h%]q?}q@(h((}qA(h*h+hhu}qB(h*X   versionqChX   twilio.rest.video.v1qDX   V1qE�qFu}qG(h*X   payloadqHh]qI(hX   intqJ�qKhX   boolqL�qMhX   NoneTypeqN�qOhX   floatqP�qQeu}qR(h*X   sidqShhOX   default_valueqTX   NoneqUutqVh-NuauuX   media_regionqW}qX(hhh}qY(hXV   
        :returns: Region for the media server in Group Rooms.
        :rtype: unicodeqZhNhM&K	�q[uuX   sidq\}q](hhh}q^(hXy   
        :returns: A system-generated 34-character string that uniquely identifies this resource.
        :rtype: unicodeq_hNhM�K	�q`uuX   enable_turnqa}qb(hhh}qc(hXW   
        :returns: Enable Twilio's Network Traversal TURN service.
        :rtype: boolqdhNhM�K	�qeuuX   account_sidqf}qg(hhh}qh(hXb   
        :returns: The unique ID of the Account associated with this Room.
        :rtype: unicodeqihNhM�K	�qjuuX   urlqk}ql(hhh}qm(hXN   
        :returns: The absolute URL for this resource.
        :rtype: unicodeqnhNhM.K	�qouuX   linksqp}qq(hhh}qr(hX4   
        :returns: The links
        :rtype: unicodeqshNhM6K	�qtuuX   status_callbackqu}qv(hhh}qw(hXx   
        :returns: A URL that Twilio sends asynchronous webhook requests to on every Room event.
        :rtype: unicodeqxhNhM�K	�qyuuX   video_codecsqz}q{(hhh}q|(hXK   
        :returns: The video_codecs
        :rtype: RoomInstance.VideoCodecq}hNhMK	�q~uuX
   RoomStatusq}q�(hX   typerefq�h]q�hh�q�auX
   recordingsq�}q�(hhh}q�(hX�   
        Access the recordings

        :returns: twilio.rest.video.v1.room.recording.RoomRecordingList
        :rtype: twilio.rest.video.v1.room.recording.RoomRecordingListq�h]q�(X#   twilio.rest.video.v1.room.recordingq�X   RoomRecordingListq��q�hOehMRK	�q�uuX   typeq�}q�(hhh}q�(hXq   
        :returns: Type of Room, either peer-to-peer, group-small or group.
        :rtype: RoomInstance.RoomTypeq�hNhMK	�q�uuX   statusq�}q�(hhh}q�(hXh   
        :returns: A string representing the status of the Room.
        :rtype: RoomInstance.RoomStatusq�hNhM�K	�q�uuX   fetchq�}q�(hh h}q�(hX|   
        Fetch a RoomInstance

        :returns: Fetched RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstanceq�h#�hM=K	�q�h%]q�}q�(h(}q�(h*h+hhu�q�h-huauuX   date_createdq�}q�(hhh}q�(hXw   
        :returns: The date that this resource was created, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimeq�hNhM�K	�q�uuX   record_participants_on_connectq�}q�(hhh}q�(hXR   
        :returns: Start recording when Participants connect.
        :rtype: boolq�hNhMK	�q�uuX   participantsq�}q�(hhh}q�(hX�   
        Access the participants

        :returns: twilio.rest.video.v1.room.room_participant.ParticipantList
        :rtype: twilio.rest.video.v1.room.room_participant.ParticipantListq�h]q�(X*   twilio.rest.video.v1.room.room_participantq�X   ParticipantListq��q�hOehM\K	�q�uuX   end_timeq�}q�(hhh}q�(hXh   
        :returns: The end time of the Room, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimeq�hNhM�K	�q�uuX   RoomTypeq�}q�(hh�h]q�hh��q�auX   updateq�}q�(hh h}q�(hX�   
        Update the RoomInstance

        :param RoomInstance.RoomStatus status: Set to completed to end the Room.

        :returns: Updated RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstanceq�h#�hMFK	�q�h%]q�}q�(h(}q�(h*h+hhu}q�(h*X   statusq�hNu�q�h-huauuX
   VideoCodecq�}q�(hh�h]q�hhǆq�auX   _proxyq�}q�(hhh}q�(hX  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: RoomContext for this RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomContextq�h]q�(hX   RoomContextqІq�hOehM�K	�q�uuX   durationq�}q�(hhh}q�(hXO   
        :returns: The duration of the Room in seconds.
        :rtype: unicodeq�hNhM�K	�q�uuX   date_updatedq�}q�(hhh}q�(hX|   
        :returns: The date that this resource was last updated, given as a UTC ISO 8601 Timestamp.
        :rtype: datetimeq�hNhM�K	�q�uuX   _propertiesq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq�q�su�q�suX   _contextq�}q�(hh�h}q�h}q�(hh�h}q�hhOsu}q�(hh�h}q�hh�su�q�suX	   _solutionq�}q�(hh�h}q�h}q�(hh�h}q�hh�su�q�suX   _versionq�}q�(hh�h}q�hhFsuuhX    q�h#�hMlK�q�uuX	   serializeq�}q�(hX	   modulerefq�hX   twilio.base.serializeq�h��q�uX   ListResourceq�}q (hh�h]r  X   twilio.base.list_resourcer  X   ListResourcer  �r  auX   InstanceResourcer  }r  (hh�h]r  hauX   InstanceContextr  }r	  (hh�h]r
  X   twilio.base.instance_contextr  X   InstanceContextr  �r  auX   RoomListr  }r  (hhh}r  (h	]r  (hj  �r  j  heh]r  j  ah}r  (h:}r  (hh h}r  (hX�   
        Initialize the RoomList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.video.v1.room.RoomList
        :rtype: twilio.rest.video.v1.room.RoomListr  h#�hKK	�r  h%]r  }r  (h(}r  (h*h+hj  u}r  (h*hChhFu�r  h-NuauuX   streamr  }r  (hh h}r   (hX.  
        Streams RoomInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param RoomInstance.RoomStatus status: Only show Rooms with the given status.
        :param unicode unique_name: Only show Rooms with the provided Name.
        :param datetime date_created_after: Only show Rooms that started on or after this date, given as YYYY-MM-DD.
        :param datetime date_created_before: Only show Rooms that started before this date, given as YYYY-MM-DD.
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.video.v1.room.RoomInstance]r!  h#�hKOK	�r"  h%]r#  }r$  (h((}r%  (h*h+hj  u}r&  (h*h�hhhTX   values.unsetr'  u}r(  (h*X   unique_namer)  hhhTX   values.unsetr*  u}r+  (h*X   date_created_afterr,  hhhTX   values.unsetr-  u}r.  (h*X   date_created_beforer/  hhhTX   values.unsetr0  u}r1  (h*X   limitr2  hhOhThUu}r3  (h*X	   page_sizer4  hhOhThUutr5  h-hX	   generatorr6  �r7  uauuX   creater8  }r9  (hh h}r:  (hX�  
        Create a new RoomInstance

        :param bool enable_turn: Use Twilio Network Traversal for TURN service.
        :param RoomInstance.RoomType type: Type of room, either peer-to-peer, group-small or group.
        :param unicode unique_name: Name of the Room.
        :param unicode status_callback: A URL that Twilio sends asynchronous webhook requests to on every room event.
        :param unicode status_callback_method: HTTP method Twilio should use when requesting the above URL.
        :param unicode max_participants: Maximum number of Participants in the Room.
        :param bool record_participants_on_connect: Start Participant recording when connected.
        :param RoomInstance.VideoCodec video_codecs: An array of video codecs supported when publishing a Track in the Room.
        :param unicode media_region: Region for the media server in Group Rooms.

        :returns: Newly created RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstancer;  h#�hK&K	�r<  h%]r=  }r>  (h((}r?  (h*h+hj  u}r@  (h*hahhhTX   values.unsetrA  u}rB  (h*X   typerC  hhhTX   values.unsetrD  u}rE  (h*j)  hhhTX   values.unsetrF  u}rG  (h*X   status_callbackrH  hhhTX   values.unsetrI  u}rJ  (h*X   status_callback_methodrK  hhhTX   values.unsetrL  u}rM  (h*X   max_participantsrN  hhhTX   values.unsetrO  u}rP  (h*h�hhhTX   values.unsetrQ  u}rR  (h*hzhhhTX   values.unsetrS  u}rT  (h*hWhhhTX   values.unsetrU  utrV  h-huauuX   getrW  }rX  (hh h}rY  (hX�   
        Constructs a RoomContext

        :param sid: The Room Sid or name that uniquely identifies this resource.

        :returns: twilio.rest.video.v1.room.RoomContext
        :rtype: twilio.rest.video.v1.room.RoomContextrZ  h#�hK�K	�r[  h%]r\  }r]  (h(}r^  (h*h+hj  u}r_  (h*hShNu�r`  h-h�uauuh}ra  (hh h}rb  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strrc  h#�hK�K	�rd  h%]re  }rf  (h(}rg  (h*h+hj  u�rh  h-h/uauuX   pageri  }rj  (hh h}rk  (hX+  
        Retrieve a single page of RoomInstance records from the API.
        Request is executed immediately

        :param RoomInstance.RoomStatus status: Only show Rooms with the given status.
        :param unicode unique_name: Only show Rooms with the provided Name.
        :param datetime date_created_after: Only show Rooms that started on or after this date, given as YYYY-MM-DD.
        :param datetime date_created_before: Only show Rooms that started before this date, given as YYYY-MM-DD.
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomPagerl  h#�hK�K	�rm  h%]rn  }ro  (h((}rp  (h*h+hj  u}rq  (h*h�hhhTX   values.unsetrr  u}rs  (h*j)  hhhTX   values.unsetrt  u}ru  (h*j,  hhhTX   values.unsetrv  u}rw  (h*j/  hhhTX   values.unsetrx  u}ry  (h*X
   page_tokenrz  hhhTX   values.unsetr{  u}r|  (h*X   page_numberr}  hhhTX   values.unsetr~  u}r  (h*j4  h]r�  (hhOehTX   values.unsetr�  utr�  h-hX   RoomPager�  �r�  uauuX   listr�  }r�  (hh h}r�  (hX�  
        Lists RoomInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param RoomInstance.RoomStatus status: Only show Rooms with the given status.
        :param unicode unique_name: Only show Rooms with the provided Name.
        :param datetime date_created_after: Only show Rooms that started on or after this date, given as YYYY-MM-DD.
        :param datetime date_created_before: Only show Rooms that started before this date, given as YYYY-MM-DD.
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.video.v1.room.RoomInstance]r�  h#�hKrK	�r�  h%]r�  }r�  (h((}r�  (h*h+hj  u}r�  (h*h�hhhTX   values.unsetr�  u}r�  (h*j)  hhhTX   values.unsetr�  u}r�  (h*j,  hhhTX   values.unsetr�  u}r�  (h*j/  hhhTX   values.unsetr�  u}r�  (h*j2  hhOhThUu}r�  (h*j4  hhOhThUutr�  h-hX   listr�  ]r�  Na�r�  uauuX   get_pager�  }r�  (hh h}r�  (hX  
        Retrieve a specific page of RoomInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomPager�  h#�hK�K	�r�  h%]r�  }r�  (h(}r�  (h*h+hj  u}r�  (h*X
   target_urlr�  hNu�r�  h-j�  uauuX   __call__r�  }r�  (hh h}r�  (hX�   
        Constructs a RoomContext

        :param sid: The Room Sid or name that uniquely identifies this resource.

        :returns: twilio.rest.video.v1.room.RoomContext
        :rtype: twilio.rest.video.v1.room.RoomContextr�  h#�hK�K	�r�  h%]r�  }r�  (h(}r�  (h*h+hj  u}r�  (h*hShNu�r�  h-h�uauuh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su�r�  suX   _urir�  }r�  (hh�h}r�  hh/suh�}r�  (hh�h}r�  hhFsuuhh�h#�hKK�r�  uuX   RoomRecordingListr�  }r�  (hh�h]r�  h�auX   Pager�  }r�  (hh�h]r�  X   twilio.base.pager�  X   Pager�  �r�  auh�}r�  (hhh}r�  (h	]r�  (h�j  heh]r�  j  ah}r�  (h:}r�  (hh h}r�  (hX'  
        Initialize the RoomContext

        :param Version version: Version that contains the resource
        :param sid: The Room Sid or name that uniquely identifies this resource.

        :returns: twilio.rest.video.v1.room.RoomContext
        :rtype: twilio.rest.video.v1.room.RoomContextr�  h#�hMK	�r�  h%]r�  }r�  (h(}r�  (h*h+hh�u}r�  (h*hChhFu}r�  (h*hShhOu�r�  h-Nuauuh}r�  (hh h}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h#�hMaK	�r�  h%]r�  }r�  (h(}r�  (h*h+hh�u�r�  h-h/uauuh�}r�  (hhh}r�  (hh�h]r�  (h�hOehMJK	�r�  uuh�}r�  (hh h}r�  (hX�   
        Update the RoomInstance

        :param RoomInstance.RoomStatus status: Set to completed to end the Room.

        :returns: Updated RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstancer�  h#�hM6K	�r�  h%]r�  }r�  (h(}r�  (h*h+hh�u}r�  (h*h�hNu�r�  h-huauuh�}r�  (hhh}r�  (hh�h]r�  (h�hOehMVK	�r�  uuh�}r�  (hh h}r�  (hX|   
        Fetch a RoomInstance

        :returns: Fetched RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstancer�  h#�hM%K	�r�  h%]r�  }r�  (h(}r�  (h*h+hh�u�r�  h-huauuh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suj�  }r�  (hh�h}r�  hh/suX   _recordingsr�  }r   (hh�h}r  h}r  (hh�h}r  hhOsu}r  (hh�h}r  hh�su�r  suX   _participantsr  }r  (hh�h}r	  h}r
  (hh�h}r  hhOsu}r  (hh�h}r  hh�su�r  suh�}r  (hh�h}r  hhFsuuhh�h#�hMK�r  uuX   ParticipantListr  }r  (hh�h]r  h�auX   deserializer  }r  (hh�hX   twilio.base.deserializer  h��r  uX   valuesr  }r  (hh�hX   twilio.base.valuesr  h��r  uX	   recordingr  }r  (hh�hh�h��r  uX   room_participantr   }r!  (hh�hh�h��r"  uj�  }r#  (hhh}r$  (h	]r%  (j�  j�  heh]r&  j�  ah}r'  (h}r(  (hh h}r)  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr*  h#�hMK	�r+  h%]r,  }r-  (h(}r.  (h*h+hj�  u�r/  h-h/uauuX   get_instancer0  }r1  (hh h}r2  (hX�   
        Build an instance of RoomInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.video.v1.room.RoomInstance
        :rtype: twilio.rest.video.v1.room.RoomInstancer3  h#�hK�K	�r4  h%]r5  }r6  (h(}r7  (h*h+hj�  u}r8  (h*hHhNu�r9  h-huauuh:}r:  (hh h}r;  (hX  
        Initialize the RoomPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.video.v1.room.RoomPage
        :rtype: twilio.rest.video.v1.room.RoomPager<  h#�hK�K	�r=  h%]r>  }r?  (h((}r@  (h*h+hj�  u}rA  (h*hChhFu}rB  (h*X   responserC  hNu}rD  (h*X   solutionrE  hh�utrF  h-Nuauuh�}rG  (hh�h}rH  h(}rI  (hh�h}rJ  hh�su}rK  (hh�h}rL  hh�su}rM  (hh�h}rN  hh�su}rO  (hh�h}rP  hh�sutrQ  suh�}rR  (hh�h}rS  hhFsuX   _payloadrT  }rU  (hh�h}rV  h(}rW  (hh�h}rX  hhKsu}rY  (hh�h}rZ  hhQsu}r[  (hh�h}r\  hhOsu}r]  (hh�h}r^  hhMsutr_  suX   _recordsr`  }ra  (hh�h}rb  hNsuuhh�h#�hK�K�rc  uuuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /rd  X   childrenre  ]rf  (j  j   eX   filenamerg  Xr   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\video\v1\room\__init__.pyrh  u.