�}q (X   membersq}q(X
   InviteListq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X*   twilio.rest.chat.v2.service.channel.inviteqX
   InviteListq�qX   twilio.base.list_resourceqX   ListResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docqX�  
        Initialize the InviteList

        :param Version version: Version that contains the resource
        :param service_sid: The SID of the Service that the resource is associated with
        :param channel_sid: The SID of the Channel the new resource belongs to

        :returns: twilio.rest.chat.v2.service.channel.invite.InviteList
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteListqX   builtinq�X   locationqKK	�qX	   overloadsq ]q!}q"(X   argsq#(}q$(X   nameq%X   selfq&hhu}q'(h%X   versionq(hX   twilio.rest.chat.v2q)X   V2q*�q+u}q,(h%X   service_sidq-hhX   NoneTypeq.�q/u}q0(h%X   channel_sidq1hh/utq2X   ret_typeq3NuauuX   streamq4}q5(hhh}q6(hX
  
        Streams InviteInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param unicode identity: The `identity` value of the resources to read
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v2.service.channel.invite.InviteInstance]q7h�hK?K	�q8h ]q9}q:(h#(}q;(h%h&hhu}q<(h%X   identityq=hhX   default_valueq>X   values.unsetq?u}q@(h%X   limitqAhh/h>X   NoneqBu}qC(h%X	   page_sizeqDhh/h>hButqEh3hX	   generatorqF�qGuauuX   createqH}qI(hhh}qJ(hXB  
        Create a new InviteInstance

        :param unicode identity: The `identity` value that identifies the new resource's User
        :param unicode role_sid: The Role assigned to the new member

        :returns: Newly created InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteInstanceqKh�hK&K	�qLh ]qM}qN(h#}qO(h%h&hhu}qP(h%h=hNu}qQ(h%X   role_sidqRhhh>X   values.unsetqSu�qTh3hX   InviteInstanceqU�qVuauuX   getqW}qX(hhh}qY(hX�   
        Constructs a InviteContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.service.channel.invite.InviteContext
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteContextqZh�hK�K	�q[h ]q\}q](h#}q^(h%h&hhu}q_(h%X   sidq`hNu�qah3hX   InviteContextqb�qcuauuX   __repr__qd}qe(hhh}qf(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strqgh�hK�K	�qhh ]qi}qj(h#}qk(h%h&hhu�qlh3hX   strqm�qnuauuX   pageqo}qp(hhh}qq(hX	  
        Retrieve a single page of InviteInstance records from the API.
        Request is executed immediately

        :param unicode identity: The `identity` value of the resources to read
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InvitePageqrh�hKjK	�qsh ]qt}qu(h#(}qv(h%h&hhu}qw(h%h=hhh>X   values.unsetqxu}qy(h%X
   page_tokenqzhhh>X   values.unsetq{u}q|(h%X   page_numberq}hhh>X   values.unsetq~u}q(h%hDh]q�(hh/eh>X   values.unsetq�utq�h3hX
   InvitePageq��q�uauuX   listq�}q�(hhh}q�(hX�  
        Lists InviteInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param unicode identity: The `identity` value of the resources to read
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v2.service.channel.invite.InviteInstance]q�h�hKWK	�q�h ]q�}q�(h#(}q�(h%h&hhu}q�(h%h=hhh>X   values.unsetq�u}q�(h%hAhh/h>hBu}q�(h%hDhh/h>hButq�h3hX   listq�]q�Na�q�uauuX   get_pageq�}q�(hhh}q�(hX2  
        Retrieve a specific page of InviteInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InvitePageq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hhu}q�(h%X
   target_urlq�hNu�q�h3h�uauuX   __call__q�}q�(hhh}q�(hX�   
        Constructs a InviteContext

        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.service.channel.invite.InviteContext
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hhu}q�(h%h`hNu�q�h3hcuauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hhnsuX   _versionq�}q�(hh�h}q�hh+suuhX    q�h�hKK�q�uuX	   serializeq�}q�(hX	   modulerefq�hX   twilio.base.serializeq�h��q�uX   ListResourceq�}q�(hX   typerefq�h]q�hauX   InstanceResourceq�}q�(hh�h]q�X   twilio.base.instance_resourceq�X   InstanceResourceq͆q�auX   InstanceContextq�}q�(hh�h]q�X   twilio.base.instance_contextq�X   InstanceContextqӆq�auX   InviteContextq�}q�(hhh}q�(h	]q�(hch�heh]q�h�ah}q�(h}q�(hhh}q�(hX�  
        Initialize the InviteContext

        :param Version version: Version that contains the resource
        :param service_sid: The SID of the Service to fetch the resource from
        :param channel_sid: The SID of the Channel the resource to fetch belongs to
        :param sid: The unique string that identifies the resource

        :returns: twilio.rest.chat.v2.service.channel.invite.InviteContext
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteContextq�h�hK�K	�q�h ]q�}q�(h#(}q�(h%h&hhcu}q�(h%h(hh+u}q�(h%h-hh/u}q�(h%h1hh/u}q�(h%h`hh/utq�h3Nuauuhd}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hM%K	�q�h ]q�}q�(h#}q�(h%h&hhcu�q�h3hnuauuX   deleteq�}q�(hhh}q�(hXt   
        Deletes the InviteInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolq�h�hMK	�q�h ]q�}q�(h#}q�(h%h&hhcu�q�h3NuauuX   fetchq�}q�(hhh}q�(hX�   
        Fetch a InviteInstance

        :returns: Fetched InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteInstanceq�h�hMK	�q�h ]q�}q�(h#}q�(h%h&hhcu�q h3hVuauuh�}r  (hh�h}r  h}r  (hh�h}r  hh�su�r  suh�}r  (hh�h}r  hhnsuh�}r  (hh�h}r	  hh+suuhh�h�hK�K�r
  uuX   Pager  }r  (hh�h]r  X   twilio.base.pager  X   Pager  �r  auX   InviteInstancer  }r  (hhh}r  (h	]r  (hVh�heh]r  h�ah}r  (h}r  (hhh}r  (hX�   
        Initialize the InviteInstance

        :returns: twilio.rest.chat.v2.service.channel.invite.InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteInstancer  h�hM3K	�r  h ]r  }r  (h#(}r  (h%h&hhVu}r  (h%h(hh+u}r  (h%X   payloadr   h]r!  (hX   intr"  �r#  hX   boolr$  �r%  h/hX   floatr&  �r'  eu}r(  (h%h-hh/u}r)  (h%h1hh/u}r*  (h%h`hh/h>hButr+  h3NuauuX   service_sidr,  }r-  (hX   propertyr.  h}r/  (hXf   
        :returns: The SID of the Service that the resource is associated with
        :rtype: unicoder0  hNhM}K	�r1  uuX   sidr2  }r3  (hj.  h}r4  (hXY   
        :returns: The unique string that identifies the resource
        :rtype: unicoder5  hNhMeK	�r6  uuX   urlr7  }r8  (hj.  h}r9  (hXR   
        :returns: The absolute URL of the Invite resource
        :rtype: unicoder:  hNhM�K	�r;  uuX   identityr<  }r=  (hj.  h}r>  (hXY   
        :returns: The string that identifies the resource's User
        :rtype: unicoder?  hNhM�K	�r@  uuh�}rA  (hhh}rB  (hXt   
        Deletes the InviteInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolrC  h�hM�K	�rD  h ]rE  }rF  (h#}rG  (h%h&hhVu�rH  h3Nuauuhd}rI  (hhh}rJ  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strrK  h�hM�K	�rL  h ]rM  }rN  (h#}rO  (h%h&hhVu�rP  h3hnuauuX   role_sidrQ  }rR  (hj.  h}rS  (hXU   
        :returns: The SID of the Role assigned to the member
        :rtype: unicoderT  hNhM�K	�rU  uuh�}rV  (hhh}rW  (hX�   
        Fetch a InviteInstance

        :returns: Fetched InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteInstancerX  h�hM�K	�rY  h ]rZ  }r[  (h#}r\  (h%h&hhVu�r]  h3hVuauuX   date_createdr^  }r_  (hj.  h}r`  (hXk   
        :returns: The RFC 2822 date and time in GMT when the resource was created
        :rtype: datetimera  hNhM�K	�rb  uuX   account_sidrc  }rd  (hj.  h}re  (hX[   
        :returns: The SID of the Account that created the resource
        :rtype: unicoderf  hNhMmK	�rg  uuX   channel_sidrh  }ri  (hj.  h}rj  (hX]   
        :returns: The SID of the Channel the new resource belongs to
        :rtype: unicoderk  hNhMuK	�rl  uuX   _proxyrm  }rn  (hj.  h}ro  (hX(  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: InviteContext for this InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteContextrp  h]rq  (hch/ehMSK	�rr  uuX
   created_byrs  }rt  (hj.  h}ru  (hX[   
        :returns: The identity of the User that created the invite
        :rtype: unicoderv  hNhM�K	�rw  uuX   date_updatedrx  }ry  (hj.  h}rz  (hXp   
        :returns: The RFC 2822 date and time in GMT when the resource was last updated
        :rtype: datetimer{  hNhM�K	�r|  uuX   _propertiesr}  }r~  (hh�h}r  h}r�  (hh�h}r�  hh�su�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh/su}r�  (hh�h}r�  hhcsu�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hh+suuhh�h�hM0K�r�  uuX   deserializer�  }r�  (hh�hX   twilio.base.deserializer�  h��r�  uX   valuesr�  }r�  (hh�hX   twilio.base.valuesr�  h��r�  uX
   InvitePager�  }r�  (hhh}r�  (h	]r�  (h�j  heh]r�  j  ah}r�  (hd}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hK�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h3hnuauuX   get_instancer�  }r�  (hhh}r�  (hX�   
        Build an instance of InviteInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.chat.v2.service.channel.invite.InviteInstance
        :rtype: twilio.rest.chat.v2.service.channel.invite.InviteInstancer�  h�hK�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u}r�  (h%j   hNu�r�  h3hVuauuh}r�  (hhh}r�  (hX�  
        Initialize the InvitePage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param service_sid: The SID of the Service that the resource is associated with
        :param channel_sid: The SID of the Channel the new resource belongs to

        :returns: twilio.rest.chat.v2.service.channel.invite.InvitePage
        :rtype: twilio.rest.chat.v2.service.channel.invite.InvitePager�  h�hK�K	�r�  h ]r�  }r�  (h#(}r�  (h%h&hh�u}r�  (h%h(hh+u}r�  (h%X   responser�  hNu}r�  (h%X   solutionr�  hh�utr�  h3Nuauuh�}r�  (hh�h}r�  h(}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�sutr�  suh�}r�  (hh�h}r�  hh+suX   _payloadr�  }r�  (hh�h}r�  h(}r�  (hh�h}r�  hj#  su}r�  (hh�h}r�  hj'  su}r�  (hh�h}r�  hh/su}r�  (hh�h}r�  hj%  sutr�  suX   _recordsr�  }r�  (hh�h}r�  hNsuuhh�h�hK�K�r�  uuuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  X   filenamer�  Xz   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\chat\v2\service\channel\invite.pyr�  u.