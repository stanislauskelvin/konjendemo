�}q (X   membersq}q(X   document_permissionq}q(X   kindqX	   modulerefqX   valueqX=   twilio.rest.preview.sync.service.document.document_permissionqX    q	�q
uX	   serializeq}q(hhhX   twilio.base.serializeqh	�quX   ListResourceq}q(hX   typerefqh]qX   twilio.base.list_resourceqX   ListResourceq�qauX   InstanceResourceq}q(hhh]qX   twilio.base.instance_resourceqX   InstanceResourceq�qauX   InstanceContextq}q(hhh]qX   twilio.base.instance_contextqX   InstanceContextq �q!auX   DocumentListq"}q#(hX   typeq$h}q%(X   mroq&]q'(X)   twilio.rest.preview.sync.service.documentq(h"�q)hX   builtinsq*X   objectq+�q,eX   basesq-]q.hah}q/(X   __init__q0}q1(hX   functionq2h}q3(X   docq4X%  
        Initialize the DocumentList

        :param Version version: Version that contains the resource
        :param service_sid: The service_sid

        :returns: twilio.rest.preview.sync.service.document.DocumentList
        :rtype: twilio.rest.preview.sync.service.document.DocumentListq5X   builtinq6�X   locationq7KK	�q8X	   overloadsq9]q:}q;(X   argsq<}q=(X   nameq>X   selfq?h$h)u}q@(h>X   versionqAh$X   twilio.rest.preview.syncqBX   SyncqC�qDu}qE(h>X   service_sidqFh$h*X   NoneTypeqG�qHu�qIX   ret_typeqJNuauuX   streamqK}qL(hh2h}qM(h4X�  
        Streams DocumentInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.preview.sync.service.document.DocumentInstance]qNh6�h7K<K	�qOh9]qP}qQ(h<}qR(h>h?h$h)u}qS(h>X   limitqTh$hHX   default_valueqUX   NoneqVu}qW(h>X	   page_sizeqXh$hHhUhVu�qYhJh*X	   generatorqZ�q[uauuX   createq\}q](hh2h}q^(h4X�   
        Create a new DocumentInstance

        :param unicode unique_name: The unique_name
        :param dict data: The data

        :returns: Newly created DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstanceq_h6�h7K(K	�q`h9]qa}qb(h<}qc(h>h?h$h)u}qd(h>X   unique_nameqeh$h,hUX   values.unsetqfu}qg(h>X   dataqhh$]qi(h,h*X   dictqj�qkehUX   values.unsetqlu�qmhJh(X   DocumentInstanceqn�qouauuX   getqp}qq(hh2h}qr(h4X�   
        Constructs a DocumentContext

        :param sid: The sid

        :returns: twilio.rest.preview.sync.service.document.DocumentContext
        :rtype: twilio.rest.preview.sync.service.document.DocumentContextqsh6�h7K�K	�qth9]qu}qv(h<}qw(h>h?h$h)u}qx(h>X   sidqyh$Nu�qzhJh(X   DocumentContextq{�q|uauuX   __repr__q}}q~(hh2h}q(h4Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h6�h7K�K	�q�h9]q�}q�(h<}q�(h>h?h$h)u�q�hJh*X   strq��q�uauuX   pageq�}q�(hh2h}q�(h4X�  
        Retrieve a single page of DocumentInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentPageq�h6�h7KeK	�q�h9]q�}q�(h<(}q�(h>h?h$h)u}q�(h>X
   page_tokenq�h$h,hUX   values.unsetq�u}q�(h>X   page_numberq�h$h,hUX   values.unsetq�u}q�(h>hXh$]q�(h,hHehUX   values.unsetq�utq�hJh(X   DocumentPageq��q�uauuX   listq�}q�(hh2h}q�(h4X^  
        Lists DocumentInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.preview.sync.service.document.DocumentInstance]q�h6�h7KSK	�q�h9]q�}q�(h<}q�(h>h?h$h)u}q�(h>hTh$hHhUhVu}q�(h>hXh$hHhUhVu�q�hJh*X   listq�]q�Na�q�uauuX   get_pageq�}q�(hh2h}q�(h4X7  
        Retrieve a specific page of DocumentInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentPageq�h6�h7K|K	�q�h9]q�}q�(h<}q�(h>h?h$h)u}q�(h>X
   target_urlq�h$Nu�q�hJh�uauuX   __call__q�}q�(hh2h}q�(h4X�   
        Constructs a DocumentContext

        :param sid: The sid

        :returns: twilio.rest.preview.sync.service.document.DocumentContext
        :rtype: twilio.rest.preview.sync.service.document.DocumentContextq�h6�h7K�K	�q�h9]q�}q�(h<}q�(h>h?h$h)u}q�(h>hyh$Nu�q�hJh|uauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�h$hksu}q�(hh�h}q�h$hksu}q�(hh�h}q�h$hksu�q�suX   _uriq�}q�(hh�h}q�h$h�suX   _versionq�}q�(hh�h}q�h$hDsuuh4X�    PLEASE NOTE that this class contains preview products that are subject
    to change. Use them with caution. If you currently do not have developer
    preview access, please contact help@twilio.com.q�h6�h7KK�q�uuX   DocumentPermissionListq�}q�(hhh]q�hX   DocumentPermissionListqֆq�auX   Pageq�}q�(hhh]q�X   twilio.base.pageq�X   Pageq܆q�auX   DocumentContextq�}q�(hh$h}q�(h&]q�(h|h!h,eh-]q�h!ah}q�(h0}q�(hh2h}q�(h4XJ  
        Initialize the DocumentContext

        :param Version version: Version that contains the resource
        :param service_sid: The service_sid
        :param sid: The sid

        :returns: twilio.rest.preview.sync.service.document.DocumentContext
        :rtype: twilio.rest.preview.sync.service.document.DocumentContextq�h6�h7K�K	�q�h9]q�}q�(h<(}q�(h>h?h$h|u}q�(h>hAh$hDu}q�(h>hFh$hHu}q�(h>hyh$hHutq�hJNuauuh}}q�(hh2h}q�(h4Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h6�h7M7K	�q�h9]q�}q�(h<}q�(h>h?h$h|u�q�hJh�uauuX   document_permissionsq�}q�(hX   propertyq�h}q�(h4X�   
        Access the document_permissions

        :returns: twilio.rest.preview.sync.service.document.document_permission.DocumentPermissionList
        :rtype: twilio.rest.preview.sync.service.document.document_permission.DocumentPermissionListq�h$]q�(hHh�eh7M(K	�q�uuX   updateq�}q�(hh2h}q (h4X�   
        Update the DocumentInstance

        :param dict data: The data

        :returns: Updated DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstancer  h6�h7MK	�r  h9]r  }r  (h<}r  (h>h?h$h|u}r  (h>hhh$hku�r  hJhouauuX   deleter  }r	  (hh2h}r
  (h4Xv   
        Deletes the DocumentInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr  h6�h7MK	�r  h9]r  }r  (h<}r  (h>h?h$h|u�r  hJNuauuX   fetchr  }r  (hh2h}r  (h4X�   
        Fetch a DocumentInstance

        :returns: Fetched DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstancer  h6�h7K�K	�r  h9]r  }r  (h<}r  (h>h?h$h|u�r  hJhouauuh�}r  (hh�h}r  h}r  (hh�h}r  h$hksu�r  suh�}r  (hh�h}r   h$h�suX   _document_permissionsr!  }r"  (hh�h}r#  h}r$  (hh�h}r%  h$hHsu}r&  (hh�h}r'  h$h�su�r(  suh�}r)  (hh�h}r*  h$hDsuuh4h�h6�h7K�K�r+  uuX   DocumentPager,  }r-  (hh$h}r.  (h&]r/  (h�h�h,eh-]r0  h�ah}r1  (h}}r2  (hh2h}r3  (h4Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr4  h6�h7K�K	�r5  h9]r6  }r7  (h<}r8  (h>h?h$h�u�r9  hJh�uauuX   get_instancer:  }r;  (hh2h}r<  (h4X  
        Build an instance of DocumentInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.preview.sync.service.document.DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstancer=  h6�h7K�K	�r>  h9]r?  }r@  (h<}rA  (h>h?h$h�u}rB  (h>X   payloadrC  h$Nu�rD  hJhouauuh0}rE  (hh2h}rF  (h4X]  
        Initialize the DocumentPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param service_sid: The service_sid

        :returns: twilio.rest.preview.sync.service.document.DocumentPage
        :rtype: twilio.rest.preview.sync.service.document.DocumentPagerG  h6�h7K�K	�rH  h9]rI  }rJ  (h<(}rK  (h>h?h$h�u}rL  (h>hAh$hDu}rM  (h>X   responserN  h$Nu}rO  (h>X   solutionrP  h$hkutrQ  hJNuauuh�}rR  (hh�h}rS  h(}rT  (hh�h}rU  h$hksu}rV  (hh�h}rW  h$hksu}rX  (hh�h}rY  h$hksu}rZ  (hh�h}r[  h$hksutr\  suh�}r]  (hh�h}r^  h$hDsuX   _payloadr_  }r`  (hh�h}ra  h(}rb  (hh�h}rc  h$h*X   intrd  �re  su}rf  (hh�h}rg  h$h*X   floatrh  �ri  su}rj  (hh�h}rk  h$hHsu}rl  (hh�h}rm  h$h*X   boolrn  �ro  sutrp  suX   _recordsrq  }rr  (hh�h}rs  h$Nsuuh4h�h6�h7K�K�rt  uuX   deserializeru  }rv  (hhhX   twilio.base.deserializerw  h	�rx  uX   valuesry  }rz  (hhhX   twilio.base.valuesr{  h	�r|  uhn}r}  (hh$h}r~  (h&]r  (hohh,eh-]r�  hah}r�  (h0}r�  (hh2h}r�  (h4X�   
        Initialize the DocumentInstance

        :returns: twilio.rest.preview.sync.service.document.DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstancer�  h6�h7MGK	�r�  h9]r�  }r�  (h<(}r�  (h>h?h$hou}r�  (h>hAh$hDu}r�  (h>jC  h$]r�  (je  jo  hHji  eu}r�  (h>hFh$hHu}r�  (h>hyh$hHhUhVutr�  hJNuauuX   service_sidr�  }r�  (hh�h}r�  (h4X:   
        :returns: The service_sid
        :rtype: unicoder�  h$Nh7M�K	�r�  uuX   sidr�  }r�  (hh�h}r�  (h4X2   
        :returns: The sid
        :rtype: unicoder�  h$Nh7MuK	�r�  uuX   datar�  }r�  (hh�h}r�  (h4X0   
        :returns: The data
        :rtype: dictr�  h$Nh7M�K	�r�  uuX   urlr�  }r�  (hh�h}r�  (h4X2   
        :returns: The url
        :rtype: unicoder�  h$Nh7M�K	�r�  uuX   linksr�  }r�  (hh�h}r�  (h4X4   
        :returns: The links
        :rtype: unicoder�  h$Nh7M�K	�r�  uuj  }r�  (hh2h}r�  (h4Xv   
        Deletes the DocumentInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h6�h7M�K	�r�  h9]r�  }r�  (h<}r�  (h>h?h$hou�r�  hJNuauuX   unique_namer�  }r�  (hh�h}r�  (h4X:   
        :returns: The unique_name
        :rtype: unicoder�  h$Nh7M}K	�r�  uuh}}r�  (hh2h}r�  (h4Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h6�h7M�K	�r�  h9]r�  }r�  (h<}r�  (h>h?h$hou�r�  hJh�uauuX   revisionr�  }r�  (hh�h}r�  (h4X7   
        :returns: The revision
        :rtype: unicoder�  h$Nh7M�K	�r�  uuj  }r�  (hh2h}r�  (h4X�   
        Fetch a DocumentInstance

        :returns: Fetched DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstancer�  h6�h7M�K	�r�  h9]r�  }r�  (h<}r�  (h>h?h$hou�r�  hJhouauuX   date_createdr�  }r�  (hh�h}r�  (h4X<   
        :returns: The date_created
        :rtype: datetimer�  h$Nh7M�K	�r�  uuh�}r�  (hh�h}r�  (h4h�h$]r�  (h�hHeh7M�K	�r�  uuX   account_sidr�  }r�  (hh�h}r�  (h4X:   
        :returns: The account_sid
        :rtype: unicoder�  h$Nh7M�K	�r�  uuh�}r�  (hh2h}r�  (h4X�   
        Update the DocumentInstance

        :param dict data: The data

        :returns: Updated DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentInstancer�  h6�h7M�K	�r�  h9]r�  }r�  (h<}r�  (h>h?h$hou}r�  (h>hhh$Nu�r�  hJhouauuX   _proxyr�  }r�  (hh�h}r�  (h4X-  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: DocumentContext for this DocumentInstance
        :rtype: twilio.rest.preview.sync.service.document.DocumentContextr�  h$]r�  (hHh|eh7MdK	�r�  uuX
   created_byr�  }r�  (hh�h}r�  (h4X9   
        :returns: The created_by
        :rtype: unicoder�  h$Nh7M�K	�r�  uuX   date_updatedr�  }r�  (hh�h}r�  (h4X<   
        :returns: The date_updated
        :rtype: datetimer�  h$Nh7M�K	�r�  uuX   _propertiesr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  h$hksu�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  h$hHsu}r�  (hh�h}r�  h$h|su�r�  suh�}r�  (hh�h}r   h}r  (hh�h}r  h$hksu�r  suh�}r  (hh�h}r  h$hDsuuh4h�h6�h7MBK�r  uuuh4X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r  X   childrenr  ]r	  haX   filenamer
  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\preview\sync\service\document\__init__.pyr  u.