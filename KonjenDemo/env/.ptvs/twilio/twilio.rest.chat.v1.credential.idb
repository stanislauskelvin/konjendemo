�}q (X   membersq}q(X   CredentialListq}q(X   kindqX   typeqX   valueq}q(X   mroq	]q
(X   twilio.rest.chat.v1.credentialqX   CredentialListq�qX   twilio.base.list_resourceqX   ListResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q(X   docqX�   
        Initialize the CredentialList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.chat.v1.credential.CredentialList
        :rtype: twilio.rest.chat.v1.credential.CredentialListqX   builtinq�X   locationqKK	�qX	   overloadsq ]q!}q"(X   argsq#}q$(X   nameq%X   selfq&hhu}q'(h%X   versionq(hX   twilio.rest.chat.v1q)X   V1q*�q+u�q,X   ret_typeq-NuauuX   streamq.}q/(hhh}q0(hX�  
        Streams CredentialInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v1.credential.CredentialInstance]q1h�hK#K	�q2h ]q3}q4(h#}q5(h%h&hhu}q6(h%X   limitq7hhX   NoneTypeq8�q9X   default_valueq:X   Noneq;u}q<(h%X	   page_sizeq=hh9h:h;u�q>h-hX	   generatorq?�q@uauuX   listqA}qB(hhh}qC(hXW  
        Lists CredentialInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.chat.v1.credential.CredentialInstance]qDh�hK:K	�qEh ]qF}qG(h#}qH(h%h&hhu}qI(h%h7hh9h:h;u}qJ(h%h=hh9h:h;u�qKh-hX   listqL]qMNa�qNuauuX   getqO}qP(hhh}qQ(hX�   
        Constructs a CredentialContext

        :param sid: The sid

        :returns: twilio.rest.chat.v1.credential.CredentialContext
        :rtype: twilio.rest.chat.v1.credential.CredentialContextqRh�hK�K	�qSh ]qT}qU(h#}qV(h%h&hhu}qW(h%X   sidqXhNu�qYh-hX   CredentialContextqZ�q[uauuX   __repr__q\}q](hhh}q^(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq_h�hK�K	�q`h ]qa}qb(h#}qc(h%h&hhu�qdh-hX   strqe�qfuauuX   pageqg}qh(hhh}qi(hX�  
        Retrieve a single page of CredentialInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialPageqjh�hKLK	�qkh ]ql}qm(h#(}qn(h%h&hhu}qo(h%X
   page_tokenqphhh:X   values.unsetqqu}qr(h%X   page_numberqshhh:X   values.unsetqtu}qu(h%h=h]qv(hh9eh:X   values.unsetqwutqxh-hX   CredentialPageqy�qzuauuX   createq{}q|(hhh}q}(hX-  
        Create a new CredentialInstance

        :param CredentialInstance.PushService type: Credential type, one of "gcm" or "apn"
        :param unicode friendly_name: Friendly name for stored credential
        :param unicode certificate: [APN only] URL encoded representation of the certificate, e.
        :param unicode private_key: [APN only] URL encoded representation of the private key, e.
        :param bool sandbox: [APN only] use this credential for sending to production or sandbox APNs
        :param unicode api_key: [GCM only] This is the "API key" for project from Google Developer console for your GCM Service application credential
        :param unicode secret: The secret

        :returns: Newly created CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstanceq~h�hKtK	�qh ]q�}q�(h#(}q�(h%h&hhu}q�(h%X   typeq�hNu}q�(h%X   friendly_nameq�hhh:X   values.unsetq�u}q�(h%X   certificateq�hhh:X   values.unsetq�u}q�(h%X   private_keyq�hhh:X   values.unsetq�u}q�(h%X   sandboxq�hhh:X   values.unsetq�u}q�(h%X   api_keyq�hhh:X   values.unsetq�u}q�(h%X   secretq�hhh:X   values.unsetq�utq�h-hX   CredentialInstanceq��q�uauuX   get_pageq�}q�(hhh}q�(hX2  
        Retrieve a specific page of CredentialInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialPageq�h�hKcK	�q�h ]q�}q�(h#}q�(h%h&hhu}q�(h%X
   target_urlq�hNu�q�h-hzuauuX   __call__q�}q�(hhh}q�(hX�   
        Constructs a CredentialContext

        :param sid: The sid

        :returns: twilio.rest.chat.v1.credential.CredentialContext
        :rtype: twilio.rest.chat.v1.credential.CredentialContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hhu}q�(h%hXhNu�q�h-h[uauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hhfsuX   _versionq�}q�(hh�h}q�hh+suuhX    q�h�hKK�q�uuX   ListResourceq�}q�(hX   typerefq�h]q�hauX   InstanceResourceq�}q�(hh�h]q�X   twilio.base.instance_resourceq�X   InstanceResourceqˆq�auX   InstanceContextq�}q�(hh�h]q�X   twilio.base.instance_contextq�X   InstanceContextqцq�auX   Pageq�}q�(hh�h]q�X   twilio.base.pageq�X   Pageq׆q�auX   CredentialContextq�}q�(hhh}q�(h	]q�(h[h�heh]q�h�ah}q�(h}q�(hhh}q�(hX  
        Initialize the CredentialContext

        :param Version version: Version that contains the resource
        :param sid: The sid

        :returns: twilio.rest.chat.v1.credential.CredentialContext
        :rtype: twilio.rest.chat.v1.credential.CredentialContextq�h�hK�K	�q�h ]q�}q�(h#}q�(h%h&hh[u}q�(h%h(hh+u}q�(h%hXhh9u�q�h-Nuauuh\}q�(hhh}q�(hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h�hM,K	�q�h ]q�}q�(h#}q�(h%h&hh[u�q�h-hfuauuX   updateq�}q�(hhh}q�(hX�  
        Update the CredentialInstance

        :param unicode friendly_name: Friendly name for stored credential
        :param unicode certificate: [APN only] URL encoded representation of the certificate, e.
        :param unicode private_key: [APN only] URL encoded representation of the private key, e.
        :param bool sandbox: [APN only] use this credential for sending to production or sandbox APNs
        :param unicode api_key: [GCM only] This is the "API key" for project from Google Developer console for your GCM Service application credential
        :param unicode secret: The secret

        :returns: Updated CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstanceq�h�hMK	�q�h ]q�}q�(h#(}q�(h%h&hh[u}q�(h%h�hhh:X   values.unsetq�u}q�(h%h�hhh:X   values.unsetq�u}q�(h%h�hhh:X   values.unsetq�u}q�(h%h�hhh:X   values.unsetq u}r  (h%h�hhh:X   values.unsetr  u}r  (h%h�hhh:X   values.unsetr  utr  h-h�uauuX   deleter  }r  (hhh}r  (hXx   
        Deletes the CredentialInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr	  h�hM#K	�r
  h ]r  }r  (h#}r  (h%h&hh[u�r  h-NuauuX   fetchr  }r  (hhh}r  (hX�   
        Fetch a CredentialInstance

        :returns: Fetched CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstancer  h�hK�K	�r  h ]r  }r  (h#}r  (h%h&hh[u�r  h-h�uauuh�}r  (hh�h}r  h}r  (hh�h}r  hh�su�r  suh�}r  (hh�h}r  hhfsuh�}r  (hh�h}r   hh+suuhh�h�hK�K�r!  uuX   CredentialPager"  }r#  (hhh}r$  (h	]r%  (hzh�heh]r&  h�ah}r'  (h\}r(  (hhh}r)  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr*  h�hK�K	�r+  h ]r,  }r-  (h#}r.  (h%h&hhzu�r/  h-hfuauuX   get_instancer0  }r1  (hhh}r2  (hX�   
        Build an instance of CredentialInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.chat.v1.credential.CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstancer3  h�hK�K	�r4  h ]r5  }r6  (h#}r7  (h%h&hhzu}r8  (h%X   payloadr9  hNu�r:  h-h�uauuh}r;  (hhh}r<  (hX!  
        Initialize the CredentialPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.chat.v1.credential.CredentialPage
        :rtype: twilio.rest.chat.v1.credential.CredentialPager=  h�hK�K	�r>  h ]r?  }r@  (h#(}rA  (h%h&hhzu}rB  (h%h(hh+u}rC  (h%X   responserD  hNu}rE  (h%X   solutionrF  hh�utrG  h-Nuauuh�}rH  (hh�h}rI  h}rJ  (hh�h}rK  hh�su}rL  (hh�h}rM  hh�su}rN  (hh�h}rO  hh�su�rP  suh�}rQ  (hh�h}rR  hh+suX   _payloadrS  }rT  (hh�h}rU  h(}rV  (hh�h}rW  hhX   intrX  �rY  su}rZ  (hh�h}r[  hhX   floatr\  �r]  su}r^  (hh�h}r_  hh9su}r`  (hh�h}ra  hhX   boolrb  �rc  sutrd  suX   _recordsre  }rf  (hh�h}rg  hNsuuhh�h�hK�K�rh  uuX   deserializeri  }rj  (hX	   modulerefrk  hX   twilio.base.deserializerl  h��rm  uX   valuesrn  }ro  (hjk  hX   twilio.base.valuesrp  h��rq  uX   CredentialInstancerr  }rs  (hhh}rt  (h	]ru  (h�h�heh]rv  h�ah}rw  (X   friendly_namerx  }ry  (hX   propertyrz  h}r{  (hXT   
        :returns: The human-readable name of this resource.
        :rtype: unicoder|  hNhMvK	�r}  uuh}r~  (hhh}r  (hX�   
        Initialize the CredentialInstance

        :returns: twilio.rest.chat.v1.credential.CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstancer�  h�hM?K	�r�  h ]r�  }r�  (h#(}r�  (h%h&hh�u}r�  (h%h(hh+u}r�  (h%j9  h]r�  (jY  jc  h9j]  eu}r�  (h%hXhh9h:h;utr�  h-NuauuX   PushServicer�  }r�  (hh�h]r�  hX   PushServicer�  �r�  auX   sidr�  }r�  (hjz  h}r�  (hXh   
        :returns: A 34 character string that uniquely identifies this resource.
        :rtype: unicoder�  hNhMfK	�r�  uuX   sandboxr�  }r�  (hjz  h}r�  (hXq   
        :returns: [APN only] true when this resource should use the sandbox APN service.
        :rtype: unicoder�  hNhM�K	�r�  uuX   urlr�  }r�  (hjz  h}r�  (hXX   
        :returns: An absolute URL for this credential resource.
        :rtype: unicoder�  hNhM�K	�r�  uuj  }r�  (hhh}r�  (hXx   
        Deletes the CredentialInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h-Nuauuh\}r�  (hhh}r�  (hXq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h-hfuauuX   typer�  }r�  (hjz  h}r�  (hX�   
        :returns: Indicates which push notifications service this credential is for - either gcm or apn
        :rtype: CredentialInstance.PushServicer�  hNhM~K	�r�  uuj  }r�  (hhh}r�  (hX�   
        Fetch a CredentialInstance

        :returns: Fetched CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#}r�  (h%h&hh�u�r�  h-h�uauuX   date_createdr�  }r�  (hjz  h}r�  (hXT   
        :returns: The date that this resource was created.
        :rtype: datetimer�  hNhM�K	�r�  uuX   account_sidr�  }r�  (hjz  h}r�  (hXp   
        :returns: The unique id of the Account[/console] responsible for this resource.
        :rtype: unicoder�  hNhMnK	�r�  uuh�}r�  (hhh}r�  (hX�  
        Update the CredentialInstance

        :param unicode friendly_name: Friendly name for stored credential
        :param unicode certificate: [APN only] URL encoded representation of the certificate, e.
        :param unicode private_key: [APN only] URL encoded representation of the private key, e.
        :param bool sandbox: [APN only] use this credential for sending to production or sandbox APNs
        :param unicode api_key: [GCM only] This is the "API key" for project from Google Developer console for your GCM Service application credential
        :param unicode secret: The secret

        :returns: Updated CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialInstancer�  h�hM�K	�r�  h ]r�  }r�  (h#(}r�  (h%h&hh�u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  u}r�  (h%h�hhh:X   values.unsetr�  utr�  h-h�uauuX   _proxyr�  }r�  (hjz  h}r�  (hX(  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: CredentialContext for this CredentialInstance
        :rtype: twilio.rest.chat.v1.credential.CredentialContextr�  h]r�  (h[h9ehMYK	�r�  uuX   date_updatedr�  }r�  (hjz  h}r�  (hXY   
        :returns: The date that this resource was last updated.
        :rtype: datetimer�  hNhM�K	�r�  uuX   _propertiesr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  hh9su}r�  (hh�h}r�  hh[su�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hh+suuhh�h�hM7K�r�  uuuhX`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  X   filenamer�  Xn   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\chat\v1\credential.pyr�  u.