�}q (X   membersq}q(X	   serializeq}q(X   kindqX	   modulerefqX   valueqX   twilio.base.serializeqX    q	�q
uX   ListResourceq}q(hX   typerefqh]qX   twilio.base.list_resourceqX   ListResourceq�qauX   InstanceResourceq}q(hhh]qX   twilio.base.instance_resourceqX   InstanceResourceq�qauX   Pageq}q(hhh]qX   twilio.base.pageqX   Pageq�qauX   DataSessionInstanceq}q(hX   typeq h}q!(X   mroq"]q#(X(   twilio.rest.wireless.v1.sim.data_sessionq$h�q%hX   builtinsq&X   objectq'�q(eX   basesq)]q*hah}q+(X
   radio_linkq,}q-(hX   propertyq.h}q/(X   docq0X�   
        :returns: The generation of wireless technology that the device was attached to the cellular tower using.
        :rtype: unicodeq1h NX   locationq2K�K	�q3uuX   cell_idq4}q5(hh.h}q6(h0X�   
        :returns: The unique id of the cellular tower that the device was attached to at the moment when the Data Session was last updated.
        :rtype: unicodeq7h Nh2MK	�q8uuX   operator_countryq9}q:(hh.h}q;(h0X�   
        :returns: The three letter country code representing where the device's Data Session took place.
        :rtype: unicodeq<h Nh2M	K	�q=uuX   __init__q>}q?(hX   functionq@h}qA(h0X�   
        Initialize the DataSessionInstance

        :returns: twilio.rest.wireless.v1.sim.data_session.DataSessionInstance
        :rtype: twilio.rest.wireless.v1.sim.data_session.DataSessionInstanceqBX   builtinqC�h2K�K	�qDX	   overloadsqE]qF}qG(X   argsqH(}qI(X   nameqJX   selfqKh h%u}qL(hJX   versionqMh X   twilio.rest.wireless.v1qNX   V1qO�qPu}qQ(hJX   payloadqRh Nu}qS(hJX   sim_sidqTh NutqUX   ret_typeqVNuauuX   cell_location_estimateqW}qX(hh.h}qY(h0X�   
        :returns: An object representing the estimated location where the device's Data Session took place.
        :rtype: dictqZh Nh2M!K	�q[uuX   sidq\}q](hh.h}q^(h0Xs   
        :returns: The unique id of the Data Session resource that this Data Record is for.
        :rtype: unicodeq_h Nh2K�K	�q`uuX   startqa}qb(hh.h}qc(h0Xu   
        :returns: The date that this Data Session started, given as GMT in ISO 8601 format.
        :rtype: datetimeqdh Nh2MAK	�qeuuX   operator_nameqf}qg(hh.h}qh(h0X�   
        :returns: The friendly name of the mobile operator network that the SIM-connected device is attached to.
        :rtype: unicodeqih Nh2MK	�qjuuX   operator_mccqk}ql(hh.h}qm(h0X�   
        :returns: The 'mobile country code' is the unique id of the home country where the Data Session took place.
        :rtype: unicodeqnh Nh2K�K	�qouuX   packets_downloadedqp}qq(hh.h}qr(h0X�   
        :returns: The number of packets downloaded by the device between the start time and when the Data Session was last updated.
        :rtype: unicodeqsh Nh2M1K	�qtuuX   endqu}qv(hh.h}qw(h0Xm   
        :returns: The date that this record ended, given as GMT in ISO 8601 format.
        :rtype: datetimeqxh Nh2MIK	�qyuuX   __repr__qz}q{(hh@h}q|(h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq}hC�h2MPK	�q~hE]q}q�(hH}q�(hJhKh h%u�q�hVh&X   strq��q�uauuX   operator_mncq�}q�(hh.h}q�(h0X�   
        :returns: The 'mobile network code' is the unique id specific to the mobile operator network where the Data Session took place.
        :rtype: unicodeq�h Nh2MK	�q�uuX   account_sidq�}q�(hh.h}q�(h0X`   
        :returns: The unique id of the Account that the SIM belongs to.
        :rtype: unicodeq�h Nh2K�K	�q�uuX   last_updatedq�}q�(hh.h}q�(h0Xz   
        :returns: The date that this resource was last updated, given as GMT in ISO 8601 format.
        :rtype: datetimeq�h Nh2M9K	�q�uuX   packets_uploadedq�}q�(hh.h}q�(h0X�   
        :returns: The number of packets uploaded by the device between the start time and when the Data Session was last updated.
        :rtype: unicodeq�h Nh2M)K	�q�uuX   sim_sidq�}q�(hh.h}q�(h0Xk   
        :returns: The unique id of the SIM resource that this Data Session is for.
        :rtype: unicodeq�h Nh2K�K	�q�uuX   _propertiesq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�h h&X   dictq��q�su}q�(hh�h}q�h h�su�q�suX   _contextq�}q�(hh�h}q�h h&X   NoneTypeq��q�suX	   _solutionq�}q�(hh�h}q�h}q�(hh�h}q�h h�su}q�(hh�h}q�h h�su�q�suX   _versionq�}q�(hh�h}q�h hPsuuh0h	hC�h2K�K�q�uuX   DataSessionListq�}q�(hh h}q�(h"]q�(h$h��q�hh(eh)]q�hah}q�(h>}q�(hh@h}q�(h0XY  
        Initialize the DataSessionList

        :param Version version: Version that contains the resource
        :param sim_sid: The unique id of the SIM resource that this Data Session is for.

        :returns: twilio.rest.wireless.v1.sim.data_session.DataSessionList
        :rtype: twilio.rest.wireless.v1.sim.data_session.DataSessionListq�hC�h2KK	�q�hE]q�}q�(hH}q�(hJhKh h�u}q�(hJhMh hPu}q�(hJhTh h�u�q�hVNuauuhz}q�(hh@h}q�(h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�hC�h2K�K	�q�hE]q�}q�(hH}q�(hJhKh h�u�q�hVh�uauuX   listq�}q�(hh@h}q�(h0X�  
        Lists DataSessionInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param datetime end: The end
        :param datetime start: The start
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.wireless.v1.sim.data_session.DataSessionInstance]q�hC�h2K>K	�q�hE]q�}q�(hH(}q�(hJhKh h�u}q�(hJX   endq�h h(X   default_valueq�X   values.unsetq�u}q�(hJX   startq�h h(h�X   values.unsetq�u}q�(hJX   limitq�h h�h�X   Noneq�u}q�(hJX	   page_sizeq�h h�h�h�utq�hVh&X   listq�]q�Na�q�uauuX   streamq�}q�(hh@h}q�(h0X  
        Streams DataSessionInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param datetime end: The end
        :param datetime start: The start
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.wireless.v1.sim.data_session.DataSessionInstance]q�hC�h2K$K	�q�hE]q�}q�(hH(}q�(hJhKh h�u}q�(hJh�h h(h�X   values.unsetq�u}q�(hJh�h h(h�X   values.unsetq�u}q�(hJh�h h�h�h�u}q�(hJh�h h�h�h�utq�hVh&X	   generatorq��q�uauuX   pageq�}q�(hh@h}q�(h0X  
        Retrieve a single page of DataSessionInstance records from the API.
        Request is executed immediately

        :param datetime end: The end
        :param datetime start: The start
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of DataSessionInstance
        :rtype: twilio.rest.wireless.v1.sim.data_session.DataSessionPageq hC�h2KSK	�r  hE]r  }r  (hH(}r  (hJhKh h�u}r  (hJh�h h(h�X   values.unsetr  u}r  (hJh�h h(h�X   values.unsetr  u}r	  (hJX
   page_tokenr
  h h(h�X   values.unsetr  u}r  (hJX   page_numberr  h h(h�X   values.unsetr  u}r  (hJh�h ]r  (h(h�eh�X   values.unsetr  utr  hVh$X   DataSessionPager  �r  uauuX   get_pager  }r  (hh@h}r  (h0X?  
        Retrieve a specific page of DataSessionInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of DataSessionInstance
        :rtype: twilio.rest.wireless.v1.sim.data_session.DataSessionPager  hC�h2KrK	�r  hE]r  }r  (hH}r  (hJhKh h�u}r  (hJX
   target_urlr  h Nu�r  hVj  uauuh�}r   (hh�h}r!  h}r"  (hh�h}r#  h h�su}r$  (hh�h}r%  h h�su}r&  (hh�h}r'  h h�su�r(  suX   _urir)  }r*  (hh�h}r+  h h�suX   _versionr,  }r-  (hh�h}r.  h hPsuuh0h	hC�h2KK�r/  uuX   deserializer0  }r1  (hhhX   twilio.base.deserializer2  h	�r3  uX   valuesr4  }r5  (hhhX   twilio.base.valuesr6  h	�r7  uj  }r8  (hh h}r9  (h"]r:  (j  hh(eh)]r;  hah}r<  (hz}r=  (hh@h}r>  (h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr?  hC�h2K�K	�r@  hE]rA  }rB  (hH}rC  (hJhKh j  u�rD  hVh�uauuX   get_instancerE  }rF  (hh@h}rG  (h0X
  
        Build an instance of DataSessionInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.wireless.v1.sim.data_session.DataSessionInstance
        :rtype: twilio.rest.wireless.v1.sim.data_session.DataSessionInstancerH  hC�h2K�K	�rI  hE]rJ  }rK  (hH}rL  (hJhKh j  u}rM  (hJhRh Nu�rN  hVh%uauuh>}rO  (hh@h}rP  (h0X�  
        Initialize the DataSessionPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param sim_sid: The unique id of the SIM resource that this Data Session is for.

        :returns: twilio.rest.wireless.v1.sim.data_session.DataSessionPage
        :rtype: twilio.rest.wireless.v1.sim.data_session.DataSessionPagerQ  hC�h2K�K	�rR  hE]rS  }rT  (hH(}rU  (hJhKh j  u}rV  (hJhMh hPu}rW  (hJX   responserX  h Nu}rY  (hJX   solutionrZ  h h�utr[  hVNuauuh�}r\  (hh�h}r]  h(}r^  (hh�h}r_  h h�su}r`  (hh�h}ra  h h�su}rb  (hh�h}rc  h h�su}rd  (hh�h}re  h h�sutrf  suj,  }rg  (hh�h}rh  h hPsuX   _payloadri  }rj  (hh�h}rk  h(}rl  (hh�h}rm  h h&X   intrn  �ro  su}rp  (hh�h}rq  h h&X   floatrr  �rs  su}rt  (hh�h}ru  h h�su}rv  (hh�h}rw  h h&X   boolrx  �ry  sutrz  suX   _recordsr{  }r|  (hh�h}r}  h Nsuuh0h	hC�h2K�K�r~  uuuh0X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r  X   childrenr�  ]r�  X   filenamer�  Xx   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\wireless\v1\sim\data_session.pyr�  u.