�}q (X   membersq}q(X   ListResourceq}q(X   kindqX   typerefqX   valueq]qX   twilio.base.list_resourceq	X   ListResourceq
�qauX   InstanceResourceq}q(hhh]qX   twilio.base.instance_resourceqX   InstanceResourceq�qauX   InstanceContextq}q(hhh]qX   twilio.base.instance_contextqX   InstanceContextq�qauX   CredentialListListq}q(hX   typeqh}q(X   mroq]q(X-   twilio.rest.trunking.v1.trunk.credential_listqh�qhX   builtinsq X   objectq!�q"eX   basesq#]q$hah}q%(X   __init__q&}q'(hX   functionq(h}q)(X   docq*X;  
        Initialize the CredentialListList

        :param Version version: Version that contains the resource
        :param trunk_sid: The trunk_sid

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListList
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListListq+X   builtinq,�X   locationq-KK	�q.X	   overloadsq/]q0}q1(X   argsq2}q3(X   nameq4X   selfq5hhu}q6(h4X   versionq7hX   twilio.rest.trunking.v1q8X   V1q9�q:u}q;(h4X	   trunk_sidq<hh X   NoneTypeq=�q>u�q?X   ret_typeq@NuauuX   streamqA}qB(hh(h}qC(h*X�  
        Streams CredentialListInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstance]qDh,�h-K7K	�qEh/]qF}qG(h2}qH(h4h5hhu}qI(h4X   limitqJhh>X   default_valueqKX   NoneqLu}qM(h4X	   page_sizeqNhh>hKhLu�qOh@h X	   generatorqP�qQuauuX   createqR}qS(hh(h}qT(h*X�  
        Create a new CredentialListInstance

        :param unicode credential_list_sid: The SID of the Credential List that you want to associate with this trunk. Once associated, Twilio will start authenticating access to the trunk against this list.

        :returns: Newly created CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstanceqUh,�h-K$K	�qVh/]qW}qX(h2}qY(h4h5hhu}qZ(h4X   credential_list_sidq[hNu�q\h@hX   CredentialListInstanceq]�q^uauuX   getq_}q`(hh(h}qa(h*X�   
        Constructs a CredentialListContext

        :param sid: The sid

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContext
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContextqbh,�h-K�K	�qch/]qd}qe(h2}qf(h4h5hhu}qg(h4X   sidqhhNu�qih@hX   CredentialListContextqj�qkuauuX   __repr__ql}qm(hh(h}qn(h*Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strqoh,�h-K�K	�qph/]qq}qr(h2}qs(h4h5hhu�qth@h X   strqu�qvuauuX   pageqw}qx(hh(h}qy(h*X�  
        Retrieve a single page of CredentialListInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListPageqzh,�h-K`K	�q{h/]q|}q}(h2(}q~(h4h5hhu}q(h4X
   page_tokenq�hh"hKX   values.unsetq�u}q�(h4X   page_numberq�hh"hKX   values.unsetq�u}q�(h4hNh]q�(h"h>ehKX   values.unsetq�utq�h@hX   CredentialListPageq��q�uauuX   listq�}q�(hh(h}q�(h*Xn  
        Lists CredentialListInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstance]q�h,�h-KNK	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4hJhh>hKhLu}q�(h4hNhh>hKhLu�q�h@h X   listq�]q�Na�q�uauuX   get_pageq�}q�(hh(h}q�(h*XM  
        Retrieve a specific page of CredentialListInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListPageq�h,�h-KwK	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4X
   target_urlq�hNu�q�h@h�uauuX   __call__q�}q�(hh(h}q�(h*X�   
        Constructs a CredentialListContext

        :param sid: The sid

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContext
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContextq�h,�h-K�K	�q�h/]q�}q�(h2}q�(h4h5hhu}q�(h4hhhNu�q�h@hkuauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hh X   dictq��q�su}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hhvsuX   _versionq�}q�(hh�h}q�hh:suuh*X    q�h,�h-KK�q�uuh]}q�(hhh}q�(h]q�(h^hh"eh#]q�hah}q�(hl}q�(hh(h}q�(h*Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h,�h-M�K	�q�h/]q�}q�(h2}q�(h4h5hh^u�q�h@hvuauuX   friendly_nameq�}q�(hX   propertyq�h}q�(h*X<   
        :returns: The friendly_name
        :rtype: unicodeq�hNh-MSK	�q�uuh&}q�(hh(h}q�(h*X�   
        Initialize the CredentialListInstance

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstanceq�h,�h-MK	�q�h/]q�}q�(h2(}q�(h4h5hh^u}q�(h4h7hh:u}q�(h4X   payloadq�h]q�(h X   intq�q�h X   boolq�q�h>h X   floatq�q�eu}q�(h4h<hh>u}q�(h4hhhh>hKhLutq�h@Nuauuhh}q�(hh�h}q�(h*X2   
        :returns: The sid
        :rtype: unicodeq�hNh-MCK	�q�uuX   urlq�}q�(hh�h}q�(h*X2   
        :returns: The url
        :rtype: unicodeq�hNh-MkK	�q�uuX   deleteq�}q�(hh(h}q�(h*X|   
        Deletes the CredentialListInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolq�h,�h-M{K	�q�h/]q�}q�(h2}q�(h4h5hh^u�q�h@NuauuX   fetchq�}q�(hh(h}q�(h*X�   
        Fetch a CredentialListInstance

        :returns: Fetched CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstanceq h,�h-MrK	�r  h/]r  }r  (h2}r  (h4h5hh^u�r  h@h^uauuX   date_createdr  }r  (hh�h}r  (h*X<   
        :returns: The date_created
        :rtype: datetimer	  hNh-M[K	�r
  uuX   account_sidr  }r  (hh�h}r  (h*X:   
        :returns: The account_sid
        :rtype: unicoder  hNh-M;K	�r  uuh<}r  (hh�h}r  (h*X8   
        :returns: The trunk_sid
        :rtype: unicoder  hNh-MKK	�r  uuX   _proxyr  }r  (hh�h}r  (h*XC  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: CredentialListContext for this CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContextr  h]r  (h>hkeh-M*K	�r  uuX   date_updatedr  }r  (hh�h}r  (h*X<   
        :returns: The date_updated
        :rtype: datetimer  hNh-McK	�r  uuX   _propertiesr  }r   (hh�h}r!  h}r"  (hh�h}r#  hh�su�r$  suX   _contextr%  }r&  (hh�h}r'  h}r(  (hh�h}r)  hh>su}r*  (hh�h}r+  hhksu�r,  suh�}r-  (hh�h}r.  h}r/  (hh�h}r0  hh�su�r1  suh�}r2  (hh�h}r3  hh:suuh*h�h,�h-MK�r4  uuX   Pager5  }r6  (hhh]r7  X   twilio.base.pager8  X   Pager9  �r:  auX   deserializer;  }r<  (hX	   modulerefr=  hX   twilio.base.deserializer>  hr?  uX   valuesr@  }rA  (hj=  hX   twilio.base.valuesrB  hrC  uh�}rD  (hhh}rE  (h]rF  (h�j:  h"eh#]rG  j:  ah}rH  (hl}rI  (hh(h}rJ  (h*Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strrK  h,�h-K�K	�rL  h/]rM  }rN  (h2}rO  (h4h5hh�u�rP  h@hvuauuX   get_instancerQ  }rR  (hh(h}rS  (h*X  
        Build an instance of CredentialListInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstancerT  h,�h-K�K	�rU  h/]rV  }rW  (h2}rX  (h4h5hh�u}rY  (h4h�hNu�rZ  h@h^uauuh&}r[  (hh(h}r\  (h*Xs  
        Initialize the CredentialListPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param trunk_sid: The trunk_sid

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListPage
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListPager]  h,�h-K�K	�r^  h/]r_  }r`  (h2(}ra  (h4h5hh�u}rb  (h4h7hh:u}rc  (h4X   responserd  hNu}re  (h4X   solutionrf  hh�utrg  h@Nuauuh�}rh  (hh�h}ri  h(}rj  (hh�h}rk  hh�su}rl  (hh�h}rm  hh�su}rn  (hh�h}ro  hh�su}rp  (hh�h}rq  hh�sutrr  suh�}rs  (hh�h}rt  hh:suX   _payloadru  }rv  (hh�h}rw  h(}rx  (hh�h}ry  hh�su}rz  (hh�h}r{  hh�su}r|  (hh�h}r}  hh>su}r~  (hh�h}r  hh�sutr�  suX   _recordsr�  }r�  (hh�h}r�  hNsuuh*h�h,�h-K�K�r�  uuhj}r�  (hhh}r�  (h]r�  (hkhh"eh#]r�  hah}r�  (h&}r�  (hh(h}r�  (h*X`  
        Initialize the CredentialListContext

        :param Version version: Version that contains the resource
        :param trunk_sid: The trunk_sid
        :param sid: The sid

        :returns: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContext
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListContextr�  h,�h-K�K	�r�  h/]r�  }r�  (h2(}r�  (h4h5hhku}r�  (h4h7hh:u}r�  (h4h<hh>u}r�  (h4hhhh>utr�  h@Nuauuhl}r�  (hh(h}r�  (h*Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h,�h-MK	�r�  h/]r�  }r�  (h2}r�  (h4h5hhku�r�  h@hvuauuh�}r�  (hh(h}r�  (h*X|   
        Deletes the CredentialListInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr�  h,�h-K�K	�r�  h/]r�  }r�  (h2}r�  (h4h5hhku�r�  h@Nuauuh�}r�  (hh(h}r�  (h*X�   
        Fetch a CredentialListInstance

        :returns: Fetched CredentialListInstance
        :rtype: twilio.rest.trunking.v1.trunk.credential_list.CredentialListInstancer�  h,�h-K�K	�r�  h/]r�  }r�  (h2}r�  (h4h5hhku�r�  h@h^uauuh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hhvsuh�}r�  (hh�h}r�  hh:suuh*h�h,�h-K�K�r�  uuuh*X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  X   filenamer�  X}   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\trunking\v1\trunk\credential_list.pyr�  u.