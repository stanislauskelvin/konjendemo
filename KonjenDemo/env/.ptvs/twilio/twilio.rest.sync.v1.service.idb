�}q (X   membersq}q(X   DocumentListq}q(X   kindqX   typerefqX   valueq]qX$   twilio.rest.sync.v1.service.documentq	X   DocumentListq
�qauX   ServiceListq}q(hX   typeqh}q(X   mroq]q(X   twilio.rest.sync.v1.serviceqX   ServiceListq�qX   twilio.base.list_resourceqX   ListResourceq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionq h}q!(X   docq"X�   
        Initialize the ServiceList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.sync.v1.service.ServiceList
        :rtype: twilio.rest.sync.v1.service.ServiceListq#X   builtinq$�X   locationq%KK	�q&X	   overloadsq']q(}q)(X   argsq*}q+(X   nameq,X   selfq-hhu}q.(h,X   versionq/hX   twilio.rest.sync.v1q0X   V1q1�q2u�q3X   ret_typeq4NuauuX   streamq5}q6(hh h}q7(h"X�  
        Streams ServiceInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.sync.v1.service.ServiceInstance]q8h$�h%KEK	�q9h']q:}q;(h*}q<(h,h-hhu}q=(h,X   limitq>hhX   NoneTypeq?�q@X   default_valueqAX   NoneqBu}qC(h,X	   page_sizeqDhh@hAhBu�qEh4hX	   generatorqF�qGuauuX   createqH}qI(hh h}qJ(h"X�  
        Create a new ServiceInstance

        :param unicode friendly_name: Human-readable name for this service instance
        :param unicode webhook_url: A URL that will receive event updates when objects are manipulated.
        :param bool reachability_webhooks_enabled: true or false - controls whether this instance fires webhooks when client endpoints connect to Sync
        :param bool acl_enabled: true or false - determines whether token identities must be granted access to Sync objects via the Permissions API in this Service.

        :returns: Newly created ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstanceqKh$�h%K(K	�qLh']qM}qN(h*(}qO(h,h-hhu}qP(h,X   friendly_nameqQhhhAX   values.unsetqRu}qS(h,X   webhook_urlqThhhAX   values.unsetqUu}qV(h,X   reachability_webhooks_enabledqWhhhAX   values.unsetqXu}qY(h,X   acl_enabledqZhhhAX   values.unsetq[utq\h4hX   ServiceInstanceq]�q^uauuX   getq_}q`(hh h}qa(h"X�   
        Constructs a ServiceContext

        :param sid: The sid

        :returns: twilio.rest.sync.v1.service.ServiceContext
        :rtype: twilio.rest.sync.v1.service.ServiceContextqbh$�h%K�K	�qch']qd}qe(h*}qf(h,h-hhu}qg(h,X   sidqhhNu�qih4hX   ServiceContextqj�qkuauuX   __repr__ql}qm(hh h}qn(h"Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strqoh$�h%K�K	�qph']qq}qr(h*}qs(h,h-hhu�qth4hX   strqu�qvuauuX   pageqw}qx(hh h}qy(h"X�  
        Retrieve a single page of ServiceInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServicePageqzh$�h%KnK	�q{h']q|}q}(h*(}q~(h,h-hhu}q(h,X
   page_tokenq�hhhAX   values.unsetq�u}q�(h,X   page_numberq�hhhAX   values.unsetq�u}q�(h,hDh]q�(hh@ehAX   values.unsetq�utq�h4hX   ServicePageq��q�uauuX   listq�}q�(hh h}q�(h"XN  
        Lists ServiceInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.sync.v1.service.ServiceInstance]q�h$�h%K\K	�q�h']q�}q�(h*}q�(h,h-hhu}q�(h,h>hh@hAhBu}q�(h,hDhh@hAhBu�q�h4hX   listq�]q�Na�q�uauuX   get_pageq�}q�(hh h}q�(h"X&  
        Retrieve a specific page of ServiceInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServicePageq�h$�h%K�K	�q�h']q�}q�(h*}q�(h,h-hhu}q�(h,X
   target_urlq�hNu�q�h4h�uauuX   __call__q�}q�(hh h}q�(h"X�   
        Constructs a ServiceContext

        :param sid: The sid

        :returns: twilio.rest.sync.v1.service.ServiceContext
        :rtype: twilio.rest.sync.v1.service.ServiceContextq�h$�h%K�K	�q�h']q�}q�(h*}q�(h,h-hhu}q�(h,hhhNu�q�h4hkuauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�hhX   dictq��q�su}q�(hh�h}q�hh�su}q�(hh�h}q�hh�su�q�suX   _uriq�}q�(hh�h}q�hhvsuX   _versionq�}q�(hh�h}q�hh2suuh"Xj    PLEASE NOTE that this class contains beta products that are subject to
    change. Use them with caution.q�h$�h%KK�q�uuX   deserializeq�}q�(hX	   modulerefq�hX   twilio.base.deserializeq�X    qȆq�uX   documentq�}q�(hh�hh	hȆq�uX   ListResourceq�}q�(hhh]q�hauX   ServiceInstanceq�}q�(hhh}q�(h]q�(h^X   twilio.base.instance_resourceq�X   InstanceResourceqՆq�heh]q�h�ah}q�(X	   documentsq�}q�(hX   propertyq�h}q�(h"X�   
        Access the documents

        :returns: twilio.rest.sync.v1.service.document.DocumentList
        :rtype: twilio.rest.sync.v1.service.document.DocumentListq�h]q�(hh@eh%MK	�q�uuX   friendly_nameq�}q�(hh�h}q�(h"XX   
        :returns: Human-readable name for this service instance
        :rtype: unicodeq�hNh%M�K	�q�uuX   fetchq�}q�(hh h}q�(h"X�   
        Fetch a ServiceInstance

        :returns: Fetched ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstanceq�h$�h%M�K	�q�h']q�}q�(h*}q�(h,h-hh^u�q�h4h^uauuh}q�(hh h}q�(h"X�   
        Initialize the ServiceInstance

        :returns: twilio.rest.sync.v1.service.ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstanceq�h$�h%MnK	�q�h']q�}q�(h*(}q�(h,h-hh^u}q�(h,h/hh2u}q�(h,X   payloadq�h]q�(hX   intq��q�hX   boolq��q�h@hX   floatq��q�eu}q�(h,hhhh@hAhButq h4NuauuX
   sync_listsr  }r  (hh�h}r  (h"X�   
        Access the sync_lists

        :returns: twilio.rest.sync.v1.service.sync_list.SyncListList
        :rtype: twilio.rest.sync.v1.service.sync_list.SyncListListr  h]r  (X%   twilio.rest.sync.v1.service.sync_listr  X   SyncListListr  �r  h@eh%M!K	�r	  uuX   sidr
  }r  (hh�h}r  (h"X2   
        :returns: The sid
        :rtype: unicoder  hNh%M�K	�r  uuX   urlr  }r  (hh�h}r  (h"X2   
        :returns: The url
        :rtype: unicoder  hNh%M�K	�r  uuX   linksr  }r  (hh�h}r  (h"X4   
        :returns: The links
        :rtype: unicoder  hNh%M�K	�r  uuX   deleter  }r  (hh h}r  (h"Xu   
        Deletes the ServiceInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr  h$�h%M�K	�r  h']r  }r  (h*}r   (h,h-hh^u�r!  h4NuauuX	   sync_mapsr"  }r#  (hh�h}r$  (h"X�   
        Access the sync_maps

        :returns: twilio.rest.sync.v1.service.sync_map.SyncMapList
        :rtype: twilio.rest.sync.v1.service.sync_map.SyncMapListr%  h]r&  (X$   twilio.rest.sync.v1.service.sync_mapr'  X   SyncMapListr(  �r)  h@eh%M+K	�r*  uuX   unique_namer+  }r,  (hh�h}r-  (h"X:   
        :returns: The unique_name
        :rtype: unicoder.  hNh%M�K	�r/  uuhl}r0  (hh h}r1  (h"Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr2  h$�h%M>K	�r3  h']r4  }r5  (h*}r6  (h,h-hh^u�r7  h4hvuauuX   acl_enabledr8  }r9  (hh�h}r:  (h"X�   
        :returns: true or false - determines whether token identities must be granted access to Sync objects via the Permissions API in this Service.
        :rtype: boolr;  hNh%M�K	�r<  uuX   reachability_webhooks_enabledr=  }r>  (hh�h}r?  (h"X�   
        :returns: true or false - controls whether this instance fires webhooks when client endpoints connect to Sync
        :rtype: boolr@  hNh%M�K	�rA  uuX   date_createdrB  }rC  (hh�h}rD  (h"X<   
        :returns: The date_created
        :rtype: datetimerE  hNh%M�K	�rF  uuX   sync_streamsrG  }rH  (hh�h}rI  (h"X�   
        Access the sync_streams

        :returns: twilio.rest.sync.v1.service.sync_stream.SyncStreamList
        :rtype: twilio.rest.sync.v1.service.sync_stream.SyncStreamListrJ  h]rK  (X'   twilio.rest.sync.v1.service.sync_streamrL  X   SyncStreamListrM  �rN  h@eh%M5K	�rO  uuX   webhook_urlrP  }rQ  (hh�h}rR  (h"Xn   
        :returns: A URL that will receive event updates when objects are manipulated.
        :rtype: unicoderS  hNh%M�K	�rT  uuX   account_sidrU  }rV  (hh�h}rW  (h"X:   
        :returns: The account_sid
        :rtype: unicoderX  hNh%M�K	�rY  uuX   updaterZ  }r[  (hh h}r\  (h"X�  
        Update the ServiceInstance

        :param unicode webhook_url: A URL that will receive event updates when objects are manipulated.
        :param unicode friendly_name: Human-readable name for this service instance
        :param bool reachability_webhooks_enabled: True or false - controls whether this instance fires webhooks when client endpoints connect to Sync
        :param bool acl_enabled: true or false - determines whether token identities must be granted access to Sync objects via the Permissions API in this Service.

        :returns: Updated ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstancer]  h$�h%MK	�r^  h']r_  }r`  (h*(}ra  (h,h-hh^u}rb  (h,hThhhAX   values.unsetrc  u}rd  (h,hQhhhAX   values.unsetre  u}rf  (h,hWhhhAX   values.unsetrg  u}rh  (h,hZhhhAX   values.unsetri  utrj  h4h^uauuX   _proxyrk  }rl  (hh�h}rm  (h"X  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: ServiceContext for this ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceContextrn  h]ro  (hkh@eh%M�K	�rp  uuX   date_updatedrq  }rr  (hh�h}rs  (h"X<   
        :returns: The date_updated
        :rtype: datetimert  hNh%M�K	�ru  uuX   _propertiesrv  }rw  (hh�h}rx  h}ry  (hh�h}rz  hh�su�r{  suX   _contextr|  }r}  (hh�h}r~  h}r  (hh�h}r�  hh@su}r�  (hh�h}r�  hhksu�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  hh�su�r�  suh�}r�  (hh�h}r�  hh2suuh"h�h$�h%MjK�r�  uuX   SyncStreamListr�  }r�  (hhh]r�  jN  auX   SyncMapListr�  }r�  (hhh]r�  j)  auX   ServicePager�  }r�  (hhh}r�  (h]r�  (h�X   twilio.base.pager�  X   Pager�  �r�  heh]r�  j�  ah}r�  (hl}r�  (hh h}r�  (h"Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h$�h%K�K	�r�  h']r�  }r�  (h*}r�  (h,h-hh�u�r�  h4hvuauuX   get_instancer�  }r�  (hh h}r�  (h"X�   
        Build an instance of ServiceInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.sync.v1.service.ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstancer�  h$�h%K�K	�r�  h']r�  }r�  (h*}r�  (h,h-hh�u}r�  (h,h�hNu�r�  h4h^uauuh}r�  (hh h}r�  (h"X  
        Initialize the ServicePage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.sync.v1.service.ServicePage
        :rtype: twilio.rest.sync.v1.service.ServicePager�  h$�h%K�K	�r�  h']r�  }r�  (h*(}r�  (h,h-hh�u}r�  (h,h/hh2u}r�  (h,X   responser�  hNu}r�  (h,X   solutionr�  hh�utr�  h4Nuauuh�}r�  (hh�h}r�  h(}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�sutr�  suh�}r�  (hh�h}r�  hh2suX   _payloadr�  }r�  (hh�h}r�  h(}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh�su}r�  (hh�h}r�  hh@su}r�  (hh�h}r�  hh�sutr�  suX   _recordsr�  }r�  (hh�h}r�  hNsuuh"h�h$�h%K�K�r�  uuX   sync_mapr�  }r�  (hh�hj'  hȆr�  uX   valuesr�  }r�  (hh�hX   twilio.base.valuesr�  hȆr�  uX	   sync_listr�  }r�  (hh�hj  hȆr�  uX   SyncListListr�  }r�  (hhh]r�  j  auX   InstanceResourcer�  }r�  (hhh]r�  h�auX   Pager�  }r�  (hhh]r�  j�  auX   InstanceContextr�  }r�  (hhh]r�  X   twilio.base.instance_contextr�  X   InstanceContextr�  �r�  auX   sync_streamr�  }r�  (hh�hjL  hȆr�  uX   ServiceContextr�  }r�  (hhh}r�  (h]r�  (hkj�  heh]r�  j�  ah}r�  (h�}r�  (hh�h}r�  (h"h�h]r�  (hh@eh%M0K	�r�  uuh}r�  (hh h}r�  (h"X�   
        Initialize the ServiceContext

        :param Version version: Version that contains the resource
        :param sid: The sid

        :returns: twilio.rest.sync.v1.service.ServiceContext
        :rtype: twilio.rest.sync.v1.service.ServiceContextr�  h$�h%K�K	�r   h']r  }r  (h*}r  (h,h-hhku}r  (h,h/hh2u}r  (h,hhhh@u�r  h4Nuauuj  }r  (hh�h}r  (h"j  h]r	  (j  h@eh%M<K	�r
  uuj"  }r  (hh�h}r  (h"j%  h]r  (j)  h@eh%MHK	�r  uuhl}r  (hh h}r  (h"Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr  h$�h%M_K	�r  h']r  }r  (h*}r  (h,h-hhku�r  h4hvuauuh�}r  (hh h}r  (h"X�   
        Fetch a ServiceInstance

        :returns: Fetched ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstancer  h$�h%K�K	�r  h']r  }r  (h*}r  (h,h-hhku�r  h4h^uauujG  }r  (hh�h}r   (h"jJ  h]r!  (h@jN  eh%MTK	�r"  uujZ  }r#  (hh h}r$  (h"X�  
        Update the ServiceInstance

        :param unicode webhook_url: A URL that will receive event updates when objects are manipulated.
        :param unicode friendly_name: Human-readable name for this service instance
        :param bool reachability_webhooks_enabled: True or false - controls whether this instance fires webhooks when client endpoints connect to Sync
        :param bool acl_enabled: true or false - determines whether token identities must be granted access to Sync objects via the Permissions API in this Service.

        :returns: Updated ServiceInstance
        :rtype: twilio.rest.sync.v1.service.ServiceInstancer%  h$�h%MK	�r&  h']r'  }r(  (h*(}r)  (h,h-hhku}r*  (h,hThhhAX   values.unsetr+  u}r,  (h,hQhhhAX   values.unsetr-  u}r.  (h,hWhhhAX   values.unsetr/  u}r0  (h,hZhhhAX   values.unsetr1  utr2  h4h^uauuj  }r3  (hh h}r4  (h"Xu   
        Deletes the ServiceInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr5  h$�h%M	K	�r6  h']r7  }r8  (h*}r9  (h,h-hhku�r:  h4Nuauuh�}r;  (hh�h}r<  h}r=  (hh�h}r>  hh�su�r?  suh�}r@  (hh�h}rA  hhvsuX
   _documentsrB  }rC  (hh�h}rD  h}rE  (hh�h}rF  hh@su}rG  (hh�h}rH  hhsu�rI  suX   _sync_listsrJ  }rK  (hh�h}rL  h}rM  (hh�h}rN  hh@su}rO  (hh�h}rP  hj  su�rQ  suX
   _sync_mapsrR  }rS  (hh�h}rT  h}rU  (hh�h}rV  hh@su}rW  (hh�h}rX  hj)  su�rY  suX   _sync_streamsrZ  }r[  (hh�h}r\  h}r]  (hh�h}r^  hh@su}r_  (hh�h}r`  hjN  su�ra  suh�}rb  (hh�h}rc  hh2suuh"h�h$�h%K�K�rd  uuuh"X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /re  X   childrenrf  ]rg  (j�  j�  j�  h�eX   filenamerh  Xt   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\sync\v1\service\__init__.pyri  u.