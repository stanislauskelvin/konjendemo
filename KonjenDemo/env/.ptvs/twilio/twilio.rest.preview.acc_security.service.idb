�}q (X   membersq}q(X   verificationq}q(X   kindqX	   modulerefqX   valueqX5   twilio.rest.preview.acc_security.service.verificationqX    q	�q
uX   ServiceContextq}q(hX   typeqh}q(X   mroq]q(X(   twilio.rest.preview.acc_security.serviceqX   ServiceContextq�qX   twilio.base.instance_contextqX   InstanceContextq�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __init__q}q(hX   functionqh}q (X   docq!X4  
        Initialize the ServiceContext

        :param Version version: Version that contains the resource
        :param sid: Verification Service Instance SID.

        :returns: twilio.rest.preview.acc_security.service.ServiceContext
        :rtype: twilio.rest.preview.acc_security.service.ServiceContextq"X   builtinq#�X   locationq$K�K	�q%X	   overloadsq&]q'}q((X   argsq)}q*(X   nameq+X   selfq,hhu}q-(h+X   versionq.hX    twilio.rest.preview.acc_securityq/X   AccSecurityq0�q1u}q2(h+X   sidq3hhX   NoneTypeq4�q5u�q6X   ret_typeq7NuauuX   __repr__q8}q9(hhh}q:(h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq;h#�h$M+K	�q<h&]q=}q>(h)}q?(h+h,hhu�q@h7hX   strqA�qBuauuX   verification_checksqC}qD(hX   propertyqEh}qF(h!X�   
        Access the verification_checks

        :returns: twilio.rest.preview.acc_security.service.verification_check.VerificationCheckList
        :rtype: twilio.rest.preview.acc_security.service.verification_check.VerificationCheckListqGh]qH(X;   twilio.rest.preview.acc_security.service.verification_checkqIX   VerificationCheckListqJ�qKh5eh$M K	�qLuuX   updateqM}qN(hhh}qO(h!X)  
        Update the ServiceInstance

        :param unicode name: Friendly name of the service
        :param unicode code_length: Length of verification code. Valid values are 4-10

        :returns: Updated ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstanceqPh#�h$K�K	�qQh&]qR}qS(h)}qT(h+h,hhu}qU(h+X   nameqVhhX   default_valueqWX   values.unsetqXu}qY(h+X   code_lengthqZhhhWX   values.unsetq[u�q\h7hX   ServiceInstanceq]�q^uauuX   verificationsq_}q`(hhEh}qa(h!X�   
        Access the verifications

        :returns: twilio.rest.preview.acc_security.service.verification.VerificationList
        :rtype: twilio.rest.preview.acc_security.service.verification.VerificationListqbh]qc(h5hX   VerificationListqd�qeeh$MK	�qfuuX   fetchqg}qh(hhh}qi(h!X�   
        Fetch a ServiceInstance

        :returns: Fetched ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstanceqjh#�h$K�K	�qkh&]ql}qm(h)}qn(h+h,hhu�qoh7h^uauuX	   _solutionqp}qq(hX   multipleqrh}qsh}qt(hX   dataquh}qvhhX   dictqw�qxsu�qysuX   _uriqz}q{(hhuh}q|hhBsuX   _verificationsq}}q~(hhrh}qh}q�(hhuh}q�hh5su}q�(hhuh}q�hhesu�q�suX   _verification_checksq�}q�(hhrh}q�h}q�(hhuh}q�hh5su}q�(hhuh}q�hhKsu�q�suX   _versionq�}q�(hhuh}q�hh1suuh!X�    PLEASE NOTE that this class contains preview products that are subject
    to change. Use them with caution. If you currently do not have developer
    preview access, please contact help@twilio.com.q�h#�h$K�K�q�uuX   ListResourceq�}q�(hX   typerefq�h]q�X   twilio.base.list_resourceq�X   ListResourceq��q�auX   valuesq�}q�(hhhX   twilio.base.valuesq�h	�q�uX   InstanceContextq�}q�(hh�h]q�hauX   verification_checkq�}q�(hhhhIh	�q�uX   ServiceListq�}q�(hhh}q�(h]q�(hX   ServiceListq��q�h�heh]q�h�ah}q�(h}q�(hhh}q�(h!X�   
        Initialize the ServiceList

        :param Version version: Version that contains the resource

        :returns: twilio.rest.preview.acc_security.service.ServiceList
        :rtype: twilio.rest.preview.acc_security.service.ServiceListq�h#�h$KK	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+h.hh1u�q�h7NuauuX   streamq�}q�(hhh}q�(h!X�  
        Streams ServiceInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.preview.acc_security.service.ServiceInstance]q�h#�h$K;K	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+X   limitq�hh5hWX   Noneq�u}q�(h+X	   page_sizeq�hh5hWh�u�q�h7hX	   generatorqq�uauuX   createq�}q�(hhh}q�(h!X1  
        Create a new ServiceInstance

        :param unicode name: Friendly name of the service
        :param unicode code_length: Length of verification code. Valid values are 4-10

        :returns: Newly created ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstanceq�h#�h$K'K	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+hVhNu}q�(h+hZhhhWX   values.unsetq�u�q�h7h^uauuX   getq�}q�(hhh}q�(h!X�   
        Constructs a ServiceContext

        :param sid: Verification Service Instance SID.

        :returns: twilio.rest.preview.acc_security.service.ServiceContext
        :rtype: twilio.rest.preview.acc_security.service.ServiceContextq�h#�h$K�K	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+h3hNu�q�h7huauuh8}q�(hhh}q�(h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h#�h$K�K	�q�h&]q�}q�(h)}q�(h+h,hh�u�q�h7hBuauuX   pageq�}q�(hhh}q�(h!X�  
        Retrieve a single page of ServiceInstance records from the API.
        Request is executed immediately

        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServicePageq�h#�h$KdK	�q�h&]q�}q�(h)(}q�(h+h,hh�u}q�(h+X
   page_tokenq�hhhWX   values.unsetq�u}q�(h+X   page_numberq�hhhWX   values.unsetq�u}q�(h+h�h]q�(hh5ehWX   values.unsetq�utq�h7hX   ServicePageq�q�uauuX   listq�}q�(hhh}q�(h!X[  
        Lists ServiceInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.preview.acc_security.service.ServiceInstance]q�h#�h$KRK	�q�h&]q�}q�(h)}q�(h+h,hh�u}q�(h+h�hh5hWh�u}q�(h+h�hh5hWh�u�q h7hX   listr  ]r  Na�r  uauuX   get_pager  }r  (hhh}r  (h!X3  
        Retrieve a specific page of ServiceInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServicePager  h#�h$K{K	�r  h&]r	  }r
  (h)}r  (h+h,hh�u}r  (h+X
   target_urlr  hNu�r  h7h�uauuX   __call__r  }r  (hhh}r  (h!X�   
        Constructs a ServiceContext

        :param sid: Verification Service Instance SID.

        :returns: twilio.rest.preview.acc_security.service.ServiceContext
        :rtype: twilio.rest.preview.acc_security.service.ServiceContextr  h#�h$K�K	�r  h&]r  }r  (h)}r  (h+h,hh�u}r  (h+h3hNu�r  h7huauuhp}r  (hhrh}r  h}r  (hhuh}r  hhxsu}r  (hhuh}r  hhxsu�r  suhz}r   (hhuh}r!  hhBsuh�}r"  (hhuh}r#  hh1suuh!h�h#�h$KK�r$  uuX   Pager%  }r&  (hh�h]r'  X   twilio.base.pager(  X   Pager)  �r*  auX   ServiceInstancer+  }r,  (hhh}r-  (h]r.  (h^X   twilio.base.instance_resourcer/  X   InstanceResourcer0  �r1  heh]r2  j1  ah}r3  (hC}r4  (hhEh}r5  (h!hGh]r6  (hKh5eh$M�K	�r7  uuh}r8  (hhh}r9  (h!X�   
        Initialize the ServiceInstance

        :returns: twilio.rest.preview.acc_security.service.ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstancer:  h#�h$M;K	�r;  h&]r<  }r=  (h)(}r>  (h+h,hh^u}r?  (h+h.hh1u}r@  (h+X   payloadrA  h]rB  (hX   intrC  �rD  hX   boolrE  �rF  h5hX   floatrG  �rH  eu}rI  (h+h3hh5hWh�utrJ  h7NuauuX   sidrK  }rL  (hhEh}rM  (h!XZ   
        :returns: A string that uniquely identifies this Service.
        :rtype: unicoderN  hNh$MbK	�rO  uuX   urlrP  }rQ  (hhEh}rR  (h!X2   
        :returns: The url
        :rtype: unicoderS  hNh$M�K	�rT  uuX   linksrU  }rV  (hhEh}rW  (h!X4   
        :returns: The links
        :rtype: unicoderX  hNh$M�K	�rY  uuh8}rZ  (hhh}r[  (h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr\  h#�h$M�K	�r]  h&]r^  }r_  (h)}r`  (h+h,hh^u�ra  h7hBuauuh_}rb  (hhEh}rc  (h!hbh]rd  (heh5eh$M�K	�re  uuhg}rf  (hhh}rg  (h!X�   
        Fetch a ServiceInstance

        :returns: Fetched ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstancerh  h#�h$M�K	�ri  h&]rj  }rk  (h)}rl  (h+h,hh^u�rm  h7h^uauuX   date_createdrn  }ro  (hhEh}rp  (h!XM   
        :returns: The date this Service was created
        :rtype: datetimerq  hNh$M�K	�rr  uuX   account_sidrs  }rt  (hhEh}ru  (h!X7   
        :returns: Account Sid.
        :rtype: unicoderv  hNh$MjK	�rw  uuX   namerx  }ry  (hhEh}rz  (h!XG   
        :returns: Friendly name of the service
        :rtype: unicoder{  hNh$MrK	�r|  uuhM}r}  (hhh}r~  (h!X)  
        Update the ServiceInstance

        :param unicode name: Friendly name of the service
        :param unicode code_length: Length of verification code. Valid values are 4-10

        :returns: Updated ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstancer  h#�h$M�K	�r�  h&]r�  }r�  (h)}r�  (h+h,hh^u}r�  (h+hVhhhWX   values.unsetr�  u}r�  (h+hZhhhWX   values.unsetr�  u�r�  h7h^uauuX   code_lengthr�  }r�  (hhEh}r�  (h!X]   
        :returns: Length of verification code. Valid values are 4-10
        :rtype: unicoder�  hNh$MzK	�r�  uuX   _proxyr�  }r�  (hhEh}r�  (h!X)  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: ServiceContext for this ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceContextr�  h]r�  (h5heh$MUK	�r�  uuX   date_updatedr�  }r�  (hhEh}r�  (h!XM   
        :returns: The date this Service was updated
        :rtype: datetimer�  hNh$M�K	�r�  uuX   _propertiesr�  }r�  (hhrh}r�  h}r�  (hhuh}r�  hhxsu�r�  suX   _contextr�  }r�  (hhrh}r�  h}r�  (hhuh}r�  hh5su}r�  (hhuh}r�  hhsu�r�  suhp}r�  (hhrh}r�  h}r�  (hhuh}r�  hhxsu�r�  suh�}r�  (hhuh}r�  hh1suuh!h�h#�h$M6K�r�  uuX   ServicePager�  }r�  (hhh}r�  (h]r�  (h�j*  heh]r�  j*  ah}r�  (h8}r�  (hhh}r�  (h!Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h#�h$K�K	�r�  h&]r�  }r�  (h)}r�  (h+h,hh�u�r�  h7hBuauuX   get_instancer�  }r�  (hhh}r�  (h!X�   
        Build an instance of ServiceInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.preview.acc_security.service.ServiceInstance
        :rtype: twilio.rest.preview.acc_security.service.ServiceInstancer�  h#�h$K�K	�r�  h&]r�  }r�  (h)}r�  (h+h,hh�u}r�  (h+jA  hNu�r�  h7h^uauuh}r�  (hhh}r�  (h!X,  
        Initialize the ServicePage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API

        :returns: twilio.rest.preview.acc_security.service.ServicePage
        :rtype: twilio.rest.preview.acc_security.service.ServicePager�  h#�h$K�K	�r�  h&]r�  }r�  (h)(}r�  (h+h,hh�u}r�  (h+h.hh1u}r�  (h+X   responser�  hNu}r�  (h+X   solutionr�  hhxutr�  h7Nuauuhp}r�  (hhrh}r�  h}r�  (hhuh}r�  hhxsu}r�  (hhuh}r�  hhxsu}r�  (hhuh}r�  hhxsu�r�  suh�}r�  (hhuh}r�  hh1suX   _payloadr�  }r�  (hhrh}r�  h(}r�  (hhuh}r�  hjD  su}r�  (hhuh}r�  hjH  su}r�  (hhuh}r�  hh5su}r�  (hhuh}r�  hjF  sutr�  suX   _recordsr�  }r�  (hhuh}r�  hNsuuh!h�h#�h$K�K�r�  uuX   deserializer�  }r�  (hhhX   twilio.base.deserializer�  h	�r�  uX   InstanceResourcer�  }r�  (hh�h]r�  j1  auX   VerificationListr�  }r�  (hh�h]r�  heauX   VerificationCheckListr�  }r�  (hh�h]r�  hKauuh!X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  (hh�eX   filenamer�  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\preview\acc_security\service\__init__.pyr   u.