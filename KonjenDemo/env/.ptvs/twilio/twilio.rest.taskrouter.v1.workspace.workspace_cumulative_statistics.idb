�}q (X   membersq}q(X	   serializeq}q(X   kindqX	   modulerefqX   valueqX   twilio.base.serializeqX    q	�q
uX   ListResourceq}q(hX   typerefqh]qX   twilio.base.list_resourceqX   ListResourceq�qauX   InstanceResourceq}q(hhh]qX   twilio.base.instance_resourceqX   InstanceResourceq�qauX   InstanceContextq}q(hhh]qX   twilio.base.instance_contextqX   InstanceContextq�qauX%   WorkspaceCumulativeStatisticsInstanceq}q(hX   typeq h}q!(X   mroq"]q#(XC   twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statisticsq$h�q%hX   builtinsq&X   objectq'�q(eX   basesq)]q*hah}q+(X   tasks_movedq,}q-(hX   propertyq.h}q/(X   docq0Xn   
        :returns: The total number of Tasks that were moved from one queue to another
        :rtype: unicodeq1h h&X   intq2�q3X   locationq4MuK	�q5uuX   split_by_wait_timeq6}q7(hh.h}q8(h0X�   
        :returns: The splits of the tasks canceled and accepted based on the provided SplitByWaitTime parameter.
        :rtype: dictq9h h3h4M=K	�q:uuX   avg_task_acceptance_timeq;}q<(hh.h}q=(h0X\   
        :returns: The average time from Task creation to acceptance
        :rtype: unicodeq>h h3h4K�K	�q?uuX   reservations_acceptedq@}qA(hh.h}qB(h0X_   
        :returns: The total number of Reservations accepted by Workers
        :rtype: unicodeqCh h3h4MK	�qDuuX   wait_duration_until_acceptedqE}qF(hh.h}qG(h0X]   
        :returns: The wait duration stats for tasks that were accepted.
        :rtype: dictqHh h3h4MEK	�qIuuX   __init__qJ}qK(hX   functionqLh}qM(h0X4  
        Initialize the WorkspaceCumulativeStatisticsInstance

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsInstanceqNX   builtinqO�h4K�K	�qPX	   overloadsqQ]qR}qS(X   argsqT(}qU(X   nameqVX   selfqWh h%u}qX(hVX   versionqYh X   twilio.rest.taskrouter.v1qZX   V1q[�q\u}q](hVX   payloadq^h ]q_(h3h&X   boolq`�qah&X   NoneTypeqb�qch&X   floatqd�qeeu}qf(hVX   workspace_sidqgh NutqhX   ret_typeqiNuauuX
   start_timeqj}qk(hh.h}ql(h0X:   
        :returns: The start_time
        :rtype: datetimeqmh h3h4K�K	�qnuuX   workspace_sidqo}qp(hh.h}qq(h0X<   
        :returns: The workspace_sid
        :rtype: unicodeqrh h3h4M�K	�qsuuX   tasks_createdqt}qu(hh.h}qv(h0XL   
        :returns: The total number of Tasks created
        :rtype: unicodeqwh h3h4MeK	�qxuuX   reservations_rescindedqy}qz(hh.h}q{(h0X_   
        :returns: The total number of Reservations that were rescinded
        :rtype: unicodeq|h h3h4M5K	�q}uuX   reservations_createdq~}q(hh.h}q�(h0Xi   
        :returns: The total number of Reservations that were created for Workers
        :rtype: unicodeq�h h3h4MK	�q�uuX   urlq�}q�(hh.h}q�(h0X2   
        :returns: The url
        :rtype: unicodeq�h h3h4M�K	�q�uuX   __repr__q�}q�(hhLh}q�(h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�hO�h4M�K	�q�hQ]q�}q�(hT}q�(hVhWh h%u�q�hih&X   strq��q�uauuX   tasks_deletedq�}q�(hh.h}q�(h0XV   
        :returns: The total number of Tasks that were deleted
        :rtype: unicodeq�h h3h4MmK	�q�uuX   reservations_timed_outq�}q�(hh.h}q�(h0X_   
        :returns: The total number of Reservations that were timed out
        :rtype: unicodeq�h h3h4M%K	�q�uuX   reservations_rejectedq�}q�(hh.h}q�(h0X^   
        :returns: The total number of Reservations that were rejected
        :rtype: unicodeq�h h3h4MK	�q�uuX   wait_duration_until_canceledq�}q�(hh.h}q�(h0X]   
        :returns: The wait duration stats for tasks that were canceled.
        :rtype: dictq�h h3h4MMK	�q�uuX   reservations_canceledq�}q�(hh.h}q�(h0X^   
        :returns: The total number of Reservations that were canceled
        :rtype: unicodeq�h h3h4M-K	�q�uuX   fetchq�}q�(hhLh}q�(h0X�  
        Fetch a WorkspaceCumulativeStatisticsInstance

        :param datetime end_date: Filter cumulative statistics by an end date.
        :param unicode minutes: Filter cumulative statistics by up to 'x' minutes in the past.
        :param datetime start_date: Filter cumulative statistics by a start date.
        :param unicode task_channel: Filter real-time and cumulative statistics by TaskChannel.
        :param unicode split_by_wait_time: A comma separated values for viewing splits of tasks canceled and accepted above the given threshold in seconds.

        :returns: Fetched WorkspaceCumulativeStatisticsInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsInstanceq�hO�h4M�K	�q�hQ]q�}q�(hT(}q�(hVhWh h%u}q�(hVX   end_dateq�h h(X   default_valueq�X   values.unsetq�u}q�(hVX   minutesq�h h(h�X   values.unsetq�u}q�(hVX
   start_dateq�h h(h�X   values.unsetq�u}q�(hVX   task_channelq�h h(h�X   values.unsetq�u}q�(hVh6h h(h�X   values.unsetq�utq�hih%uauuX   tasks_timed_out_in_workflowq�}q�(hh.h}q�(h0Xk   
        :returns: The total number of Tasks that were timed out of their Workflows
        :rtype: unicodeq�h h3h4M}K	�q�uuX   tasks_completedq�}q�(hh.h}q�(h0XX   
        :returns: The total number of Tasks that were completed
        :rtype: unicodeq�h h3h4M]K	�q�uuX   end_timeq�}q�(hh.h}q�(h0X8   
        :returns: The end_time
        :rtype: datetimeq�h h3h4MK	�q�uuX   account_sidq�}q�(hh.h}q�(h0X:   
        :returns: The account_sid
        :rtype: unicodeq�h h3h4K�K	�q�uuX   tasks_canceledq�}q�(hh.h}q�(h0XW   
        :returns: The total number of Tasks that were canceled
        :rtype: unicodeq�h h3h4MUK	�q�uuX   _proxyq�}q�(hh.h}q�(h0X�  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: WorkspaceCumulativeStatisticsContext for this WorkspaceCumulativeStatisticsInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContextq�h ]q�(h$X$   WorkspaceCumulativeStatisticsContextq�q�hceh4K�K	�q�uuX   _propertiesq�}q�(hX   multipleq�h}q�h(}q�(hX   dataq�h}q�h h&X   dictq�q�su}q�(hh�h}q�h h�su}q�(hh�h}q�h h�su}q�(hh�h}q�h h�sutq�suX   _contextq�}q�(hh�h}q�h}q�(hh�h}q�h hcsu}q�(hh�h}q�h h�su�q�suX	   _solutionq�}q�(hh�h}q�h(}q (hh�h}r  h h�su}r  (hh�h}r  h h�su}r  (hh�h}r  h h�su}r  (hh�h}r  h h�sutr  suX   _versionr	  }r
  (hh�h}r  h h\suuh0h	hO�h4K�K�r  uuX   Pager  }r  (hhh]r  X   twilio.base.pager  X   Pager  �r  auX!   WorkspaceCumulativeStatisticsListr  }r  (hh h}r  (h"]r  (h$j  �r  hh(eh)]r  hah}r  (hJ}r  (hhLh}r  (h0X�  
        Initialize the WorkspaceCumulativeStatisticsList

        :param Version version: Version that contains the resource
        :param workspace_sid: The workspace_sid

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsList
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsListr  hO�h4KK	�r  hQ]r  }r  (hT}r   (hVhWh j  u}r!  (hVhYh h\u}r"  (hVhgh hcu�r#  hiNuauuX   __call__r$  }r%  (hhLh}r&  (h0X/  
        Constructs a WorkspaceCumulativeStatisticsContext

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContext
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContextr'  hO�h4K0K	�r(  hQ]r)  }r*  (hT}r+  (hVhWh j  u�r,  hih�uauuh�}r-  (hhLh}r.  (h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr/  hO�h4K<K	�r0  hQ]r1  }r2  (hT}r3  (hVhWh j  u�r4  hih�uauuX   getr5  }r6  (hhLh}r7  (h0X/  
        Constructs a WorkspaceCumulativeStatisticsContext

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContext
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContextr8  hO�h4K$K	�r9  hQ]r:  }r;  (hT}r<  (hVhWh j  u�r=  hih�uauuh�}r>  (hh�h}r?  h}r@  (hh�h}rA  h h�su}rB  (hh�h}rC  h h�su}rD  (hh�h}rE  h h�su�rF  suj	  }rG  (hh�h}rH  h h\suuh0h	hO�h4KK�rI  uuX   deserializerJ  }rK  (hhhX   twilio.base.deserializerL  h	�rM  uX   valuesrN  }rO  (hhhX   twilio.base.valuesrP  h	�rQ  uX!   WorkspaceCumulativeStatisticsPagerR  }rS  (hh h}rT  (h"]rU  (h$jR  �rV  j  h(eh)]rW  j  ah}rX  (h�}rY  (hhLh}rZ  (h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr[  hO�h4KhK	�r\  hQ]r]  }r^  (hT}r_  (hVhWh jV  u�r`  hih�uauuX   get_instancera  }rb  (hhLh}rc  (h0Xv  
        Build an instance of WorkspaceCumulativeStatisticsInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsInstancerd  hO�h4KYK	�re  hQ]rf  }rg  (hT}rh  (hVhWh jV  u}ri  (hVh^h Nu�rj  hih%uauuhJ}rk  (hhLh}rl  (h0X�  
        Initialize the WorkspaceCumulativeStatisticsPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param workspace_sid: The workspace_sid

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsPage
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsPagerm  hO�h4KIK	�rn  hQ]ro  }rp  (hT(}rq  (hVhWh jV  u}rr  (hVhYh Nu}rs  (hVX   responsert  h Nu}ru  (hVX   solutionrv  h Nutrw  hiNuauuh�}rx  (hh�h}ry  h Nsuuh0h	hO�h4KFK�rz  uuh�}r{  (hh h}r|  (h"]r}  (h�hh(eh)]r~  hah}r  (h�}r�  (hhLh}r�  (h0X�  
        Fetch a WorkspaceCumulativeStatisticsInstance

        :param datetime end_date: Filter cumulative statistics by an end date.
        :param unicode minutes: Filter cumulative statistics by up to 'x' minutes in the past.
        :param datetime start_date: Filter cumulative statistics by a start date.
        :param unicode task_channel: Filter real-time and cumulative statistics by TaskChannel.
        :param unicode split_by_wait_time: A comma separated values for viewing splits of tasks canceled and accepted above the given threshold in seconds.

        :returns: Fetched WorkspaceCumulativeStatisticsInstance
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsInstancer�  hO�h4K�K	�r�  hQ]r�  }r�  (hT(}r�  (hVhWh h�u}r�  (hVh�h h(h�X   values.unsetr�  u}r�  (hVh�h h(h�X   values.unsetr�  u}r�  (hVh�h h(h�X   values.unsetr�  u}r�  (hVh�h h(h�X   values.unsetr�  u}r�  (hVh6h h(h�X   values.unsetr�  utr�  hih%uauuhJ}r�  (hhLh}r�  (h0X�  
        Initialize the WorkspaceCumulativeStatisticsContext

        :param Version version: Version that contains the resource
        :param workspace_sid: The workspace_sid

        :returns: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContext
        :rtype: twilio.rest.taskrouter.v1.workspace.workspace_cumulative_statistics.WorkspaceCumulativeStatisticsContextr�  hO�h4KuK	�r�  hQ]r�  }r�  (hT}r�  (hVhWh h�u}r�  (hVhYh h\u}r�  (hVhgh Nu�r�  hiNuauuh�}r�  (hhLh}r�  (h0Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  hO�h4K�K	�r�  hQ]r�  }r�  (hT}r�  (hVhWh h�u�r�  hih�uauuh�}r�  (hh�h}r�  h}r�  (hh�h}r�  h h�su�r�  suX   _urir�  }r�  (hh�h}r�  h h�suj	  }r�  (hh�h}r�  h h\suuh0h	hO�h4KrK�r�  uuuh0X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r�  X   childrenr�  ]r�  X   filenamer�  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\taskrouter\v1\workspace\workspace_cumulative_statistics.pyr�  u.