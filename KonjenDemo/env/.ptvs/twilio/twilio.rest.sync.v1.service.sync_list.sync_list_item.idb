�}q (X   membersq}q(X	   serializeq}q(X   kindqX	   modulerefqX   valueqX   twilio.base.serializeqX    q	�q
uX   ListResourceq}q(hX   typerefqh]qX   twilio.base.list_resourceqX   ListResourceq�qauX   InstanceResourceq}q(hhh]qX   twilio.base.instance_resourceqX   InstanceResourceq�qauX   InstanceContextq}q(hhh]qX   twilio.base.instance_contextqX   InstanceContextq�qauX   SyncListItemListq}q(hX   typeq h}q!(X   mroq"]q#(X4   twilio.rest.sync.v1.service.sync_list.sync_list_itemq$X   SyncListItemListq%�q&hX   builtinsq'X   objectq(�q)eX   basesq*]q+hah}q,(X   __init__q-}q.(hX   functionq/h}q0(X   docq1X�  
        Initialize the SyncListItemList

        :param Version version: Version that contains the resource
        :param service_sid: The unique SID identifier of the Service Instance that hosts this List object.
        :param list_sid: The unique 34-character SID identifier of the List containing this Item.

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemList
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemListq2X   builtinq3�X   locationq4KK	�q5X	   overloadsq6]q7}q8(X   argsq9(}q:(X   nameq;X   selfq<h h&u}q=(h;X   versionq>h X   twilio.rest.sync.v1q?X   V1q@�qAu}qB(h;X   service_sidqCh h'X   NoneTypeqD�qEu}qF(h;X   list_sidqGh hEutqHX   ret_typeqINuauuX   streamqJ}qK(hh/h}qL(h1X�  
        Streams SyncListItemInstance records from the API as a generator stream.
        This operation lazily loads records as efficiently as possible until the limit
        is reached.
        The results are returned as a generator, so this operation is memory efficient.

        :param SyncListItemInstance.QueryResultOrder order: A string; asc or desc
        :param unicode from_: An integer representing Item index offset.
        :param SyncListItemInstance.QueryFromBoundType bounds: The bounds
        :param int limit: Upper limit for the number of records to return. stream()
                          guarantees to never return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, stream() will attempt to read the
                              limit with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstance]qMh3�h4KHK	�qNh6]qO}qP(h9(}qQ(h;h<h h&u}qR(h;X   orderqSh h)X   default_valueqTX   values.unsetqUu}qV(h;X   from_qWh h)hTX   values.unsetqXu}qY(h;X   boundsqZh h)hTX   values.unsetq[u}q\(h;X   limitq]h hEhTX   Noneq^u}q_(h;X	   page_sizeq`h hEhTh^utqahIh'X	   generatorqb�qcuauuX   createqd}qe(hh/h}qf(h1XT  
        Create a new SyncListItemInstance

        :param dict data: Contains arbitrary user-defined, schema-less data that this List Item stores, represented by a JSON object, up to 16KB.
        :param unicode ttl: Alias for item_ttl
        :param unicode item_ttl: Time-to-live of this item in seconds, defaults to no expiration.
        :param unicode collection_ttl: Time-to-live of this item's parent List in seconds, defaults to no expiration.

        :returns: Newly created SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstanceqgh3�h4K'K	�qhh6]qi}qj(h9(}qk(h;h<h h&u}ql(h;X   dataqmh h'X   dictqn�qou}qp(h;X   ttlqqh h)hTX   values.unsetqru}qs(h;X   item_ttlqth h)hTX   values.unsetquu}qv(h;X   collection_ttlqwh h)hTX   values.unsetqxutqyhIh$X   SyncListItemInstanceqz�q{uauuX   getq|}q}(hh/h}q~(h1X�   
        Constructs a SyncListItemContext

        :param index: The index

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContext
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContextqh3�h4K�K	�q�h6]q�}q�(h9}q�(h;h<h h&u}q�(h;X   indexq�h Nu�q�hIh$X   SyncListItemContextq��q�uauuX   __repr__q�}q�(hh/h}q�(h1Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strq�h3�h4K�K	�q�h6]q�}q�(h9}q�(h;h<h h&u�q�hIh'X   strq��q�uauuX   pageq�}q�(hh/h}q�(h1X�  
        Retrieve a single page of SyncListItemInstance records from the API.
        Request is executed immediately

        :param SyncListItemInstance.QueryResultOrder order: A string; asc or desc
        :param unicode from_: An integer representing Item index offset.
        :param SyncListItemInstance.QueryFromBoundType bounds: The bounds
        :param str page_token: PageToken provided by the API
        :param int page_number: Page Number, this value is simply for client state
        :param int page_size: Number of records to return, defaults to 50

        :returns: Page of SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemPageq�h3�h4KyK	�q�h6]q�}q�(h9(}q�(h;h<h h&u}q�(h;hSh h)hTX   values.unsetq�u}q�(h;hWh h)hTX   values.unsetq�u}q�(h;hZh h)hTX   values.unsetq�u}q�(h;X
   page_tokenq�h h)hTX   values.unsetq�u}q�(h;X   page_numberq�h h)hTX   values.unsetq�u}q�(h;h`h ]q�(h)hEehTX   values.unsetq�utq�hIh$X   SyncListItemPageq��q�uauuX   listq�}q�(hh/h}q�(h1XV  
        Lists SyncListItemInstance records from the API as a list.
        Unlike stream(), this operation is eager and will load `limit` records into
        memory before returning.

        :param SyncListItemInstance.QueryResultOrder order: A string; asc or desc
        :param unicode from_: An integer representing Item index offset.
        :param SyncListItemInstance.QueryFromBoundType bounds: The bounds
        :param int limit: Upper limit for the number of records to return. list() guarantees
                          never to return more than limit.  Default is no limit
        :param int page_size: Number of records to fetch per request, when not set will use
                              the default value of 50 records.  If no page_size is defined
                              but a limit is defined, list() will attempt to read the limit
                              with the most efficient page size, i.e. min(limit, 1000)

        :returns: Generator that will yield up to limit results
        :rtype: list[twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstance]q�h3�h4KcK	�q�h6]q�}q�(h9(}q�(h;h<h h&u}q�(h;hSh h)hTX   values.unsetq�u}q�(h;hWh h)hTX   values.unsetq�u}q�(h;hZh h)hTX   values.unsetq�u}q�(h;h]h hEhTh^u}q�(h;h`h hEhTh^utq�hIh'X   listq�]q�Na�q�uauuX   get_pageq�}q�(hh/h}q�(h1XN  
        Retrieve a specific page of SyncListItemInstance records from the API.
        Request is executed immediately

        :param str target_url: API-generated URL for the requested results page

        :returns: Page of SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemPageq�h3�h4K�K	�q�h6]q�}q�(h9}q�(h;h<h h&u}q�(h;X
   target_urlq�h Nu�q�hIh�uauuX   __call__q�}q�(hh/h}q�(h1X�   
        Constructs a SyncListItemContext

        :param index: The index

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContext
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContextq�h3�h4K�K	�q�h6]q�}q�(h9}q�(h;h<h h&u}q�(h;h�h Nu�q�hIh�uauuX	   _solutionq�}q�(hX   multipleq�h}q�h}q�(hX   dataq�h}q�h hosu}q�(hh�h}q�h hosu}q�(hh�h}q�h hosu�q�suX   _uriq�}q�(hh�h}q�h h�suX   _versionq�}q�(hh�h}q�h hAsuuh1Xj    PLEASE NOTE that this class contains beta products that are subject to
    change. Use them with caution.q�h3�h4KK�q�uuX   Pageq�}q�(hhh]q�X   twilio.base.pageq�X   Pageq�q�auhz}q�(hh h}q�(h"]q�(h{hh)eh*]q�hah}q�(h-}q�(hh/h}q�(h1X�   
        Initialize the SyncListItemInstance

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstanceq�h3�h4MtK	�q�h6]q�}q�(h9(}q�(h;h<h h{u}q�(h;h>h hAu}q�(h;X   payloadq�h ]q (h'X   intr  �r  h'X   boolr  �r  hEh'X   floatr  �r  eu}r  (h;hCh hEu}r  (h;hGh hEu}r	  (h;h�h hEhTh^utr
  hINuauuX   service_sidr  }r  (hX   propertyr  h}r  (h1Xy   
        :returns: The unique SID identifier of the Service Instance that hosts this List object.
        :rtype: unicoder  h Nh4M�K	�r  uuX   datar  }r  (hj  h}r  (h1X�   
        :returns: Contains arbitrary user-defined, schema-less data that this List Item stores, represented by a JSON object, up to 16KB.
        :rtype: dictr  h Nh4M�K	�r  uuX   urlr  }r  (hj  h}r  (h1XJ   
        :returns: The absolute URL for this item.
        :rtype: unicoder  h Nh4M�K	�r  uuX   indexr  }r  (hj  h}r  (h1XX   
        :returns: Contains the numeric index of this List Item.
        :rtype: unicoder  h Nh4M�K	�r  uuX   deleter   }r!  (hh/h}r"  (h1Xz   
        Deletes the SyncListItemInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr#  h3�h4MK	�r$  h6]r%  }r&  (h9}r'  (h;h<h h{u�r(  hINuauuX   list_sidr)  }r*  (hj  h}r+  (h1Xs   
        :returns: The unique 34-character SID identifier of the List containing this Item.
        :rtype: unicoder,  h Nh4M�K	�r-  uuh�}r.  (hh/h}r/  (h1Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr0  h3�h4MK	�r1  h6]r2  }r3  (h9}r4  (h;h<h h{u�r5  hIh�uauuX   revisionr6  }r7  (hj  h}r8  (h1Xz   
        :returns: Contains the current revision of this item, represented by a string identifier.
        :rtype: unicoder9  h Nh4M�K	�r:  uuX   QueryFromBoundTyper;  }r<  (hhh]r=  h$X   QueryFromBoundTyper>  �r?  auX   fetchr@  }rA  (hh/h}rB  (h1X�   
        Fetch a SyncListItemInstance

        :returns: Fetched SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstancerC  h3�h4M�K	�rD  h6]rE  }rF  (h9}rG  (h;h<h h{u�rH  hIh{uauuX   date_createdrI  }rJ  (hj  h}rK  (h1Xi   
        :returns: The date this item was created, given in UTC ISO 8601 format.
        :rtype: datetimerL  h Nh4M�K	�rM  uuX   date_expiresrN  }rO  (hj  h}rP  (h1Xo   
        :returns: Contains the date this item expires and gets deleted automatically.
        :rtype: datetimerQ  h Nh4M�K	�rR  uuX   account_sidrS  }rT  (hj  h}rU  (h1X[   
        :returns: The unique SID identifier of the Twilio Account.
        :rtype: unicoderV  h Nh4M�K	�rW  uuX   QueryResultOrderrX  }rY  (hhh]rZ  h$X   QueryResultOrderr[  �r\  auX   updater]  }r^  (hh/h}r_  (h1XL  
        Update the SyncListItemInstance

        :param dict data: Contains arbitrary user-defined, schema-less data that this List Item stores, represented by a JSON object, up to 16KB.
        :param unicode ttl: Alias for item_ttl
        :param unicode item_ttl: Time-to-live of this item in seconds, defaults to no expiration.
        :param unicode collection_ttl: Time-to-live of this item's parent List in seconds, defaults to no expiration.

        :returns: Updated SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstancer`  h3�h4MK	�ra  h6]rb  }rc  (h9(}rd  (h;h<h h{u}re  (h;hmh h)hTX   values.unsetrf  u}rg  (h;hqh h)hTX   values.unsetrh  u}ri  (h;hth h)hTX   values.unsetrj  u}rk  (h;hwh h)hTX   values.unsetrl  utrm  hIh{uauuX   _proxyrn  }ro  (hj  h}rp  (h1XD  
        Generate an instance context for the instance, the context is capable of
        performing various actions.  All instance actions are proxied to the context

        :returns: SyncListItemContext for this SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContextrq  h ]rr  (h�hEeh4M�K	�rs  uuX
   created_byrt  }ru  (hj  h}rv  (h1XO   
        :returns: The identity of this item's creator.
        :rtype: unicoderw  h Nh4M�K	�rx  uuX   date_updatedry  }rz  (hj  h}r{  (h1Xx   
        :returns: Specifies the date this item was last updated, given in UTC ISO 8601 format.
        :rtype: datetimer|  h Nh4M�K	�r}  uuX   _propertiesr~  }r  (hh�h}r�  h}r�  (hh�h}r�  h hosu�r�  suX   _contextr�  }r�  (hh�h}r�  h}r�  (hh�h}r�  h hEsu}r�  (hh�h}r�  h h�su�r�  suh�}r�  (hh�h}r�  h}r�  (hh�h}r�  h hosu�r�  suh�}r�  (hh�h}r�  h hAsuuh1h�h3�h4MhK�r�  uuh�}r�  (hh h}r�  (h"]r�  (h�h�h)eh*]r�  h�ah}r�  (h�}r�  (hh/h}r�  (h1Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h3�h4K�K	�r�  h6]r�  }r�  (h9}r�  (h;h<h h�u�r�  hIh�uauuX   get_instancer�  }r�  (hh/h}r�  (h1X%  
        Build an instance of SyncListItemInstance

        :param dict payload: Payload response from the API

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstancer�  h3�h4K�K	�r�  h6]r�  }r�  (h9}r�  (h;h<h h�u}r�  (h;h�h Nu�r�  hIh{uauuh-}r�  (hh/h}r�  (h1X   
        Initialize the SyncListItemPage

        :param Version version: Version that contains the resource
        :param Response response: Response from the API
        :param service_sid: The unique SID identifier of the Service Instance that hosts this List object.
        :param list_sid: The unique 34-character SID identifier of the List containing this Item.

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemPage
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemPager�  h3�h4K�K	�r�  h6]r�  }r�  (h9(}r�  (h;h<h h�u}r�  (h;h>h hAu}r�  (h;X   responser�  h Nu}r�  (h;X   solutionr�  h houtr�  hINuauuh�}r�  (hh�h}r�  h(}r�  (hh�h}r�  h hosu}r�  (hh�h}r�  h hosu}r�  (hh�h}r�  h hosu}r�  (hh�h}r�  h hosutr�  suh�}r�  (hh�h}r�  h hAsuX   _payloadr�  }r�  (hh�h}r�  h(}r�  (hh�h}r�  h j  su}r�  (hh�h}r�  h j  su}r�  (hh�h}r�  h hEsu}r�  (hh�h}r�  h j  sutr�  suX   _recordsr�  }r�  (hh�h}r�  h Nsuuh1h�h3�h4K�K�r�  uuX   deserializer�  }r�  (hhhX   twilio.base.deserializer�  h	�r�  uX   valuesr�  }r�  (hhhX   twilio.base.valuesr�  h	�r�  uh�}r�  (hh h}r�  (h"]r�  (h�hh)eh*]r�  hah}r�  (h-}r�  (hh/h}r�  (h1X�  
        Initialize the SyncListItemContext

        :param Version version: Version that contains the resource
        :param service_sid: The service_sid
        :param list_sid: The list_sid
        :param index: The index

        :returns: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContext
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemContextr�  h3�h4M	K	�r�  h6]r�  }r�  (h9(}r�  (h;h<h h�u}r�  (h;h>h hAu}r�  (h;hCh hEu}r�  (h;hGh hEu}r�  (h;h�h hEutr�  hINuauuh�}r�  (hh/h}r�  (h1Xq   
        Provide a friendly representation

        :returns: Machine friendly representation
        :rtype: strr�  h3�h4M]K	�r�  h6]r�  }r�  (h9}r�  (h;h<h h�u�r�  hIh�uauuj]  }r�  (hh/h}r�  (h1XL  
        Update the SyncListItemInstance

        :param dict data: Contains arbitrary user-defined, schema-less data that this List Item stores, represented by a JSON object, up to 16KB.
        :param unicode ttl: Alias for item_ttl
        :param unicode item_ttl: Time-to-live of this item in seconds, defaults to no expiration.
        :param unicode collection_ttl: Time-to-live of this item's parent List in seconds, defaults to no expiration.

        :returns: Updated SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstancer�  h3�h4M;K	�r�  h6]r�  }r�  (h9(}r�  (h;h<h h�u}r�  (h;hmh ]r�  (h)hoehTX   values.unsetr�  u}r   (h;hqh h)hTX   values.unsetr  u}r  (h;hth h)hTX   values.unsetr  u}r  (h;hwh h)hTX   values.unsetr  utr  hIh{uauuj   }r  (hh/h}r  (h1Xz   
        Deletes the SyncListItemInstance

        :returns: True if delete succeeds, False otherwise
        :rtype: boolr	  h3�h4M2K	�r
  h6]r  }r  (h9}r  (h;h<h h�u�r  hINuauuj@  }r  (hh/h}r  (h1X�   
        Fetch a SyncListItemInstance

        :returns: Fetched SyncListItemInstance
        :rtype: twilio.rest.sync.v1.service.sync_list.sync_list_item.SyncListItemInstancer  h3�h4MK	�r  h6]r  }r  (h9}r  (h;h<h h�u�r  hIh{uauuh�}r  (hh�h}r  h}r  (hh�h}r  h hosu�r  suh�}r  (hh�h}r  h h�suh�}r  (hh�h}r  h hAsuuh1h�h3�h4MK�r   uuuh1X`   
This code was generated by
\ / _    _  _|   _  _
 | (_)\/(_)(_|\/| |(/_  v1.0.0
      /       /r!  X   childrenr"  ]r#  X   filenamer$  X�   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\twilio\rest\sync\v1\service\sync_list\sync_list_item.pyr%  u.