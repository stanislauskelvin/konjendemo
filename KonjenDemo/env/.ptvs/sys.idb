�}q (X   docqXX  This module provides access to some objects used or maintained by the
interpreter and to functions that interact strongly with the interpreter.

Dynamic objects:

argv -- command line arguments; argv[0] is the script pathname if known
path -- module search path; path[0] is the script directory, else ''
modules -- dictionary of loaded modules

displayhook -- called to show results in an interactive session
excepthook -- called to handle any uncaught exception other than SystemExit
  To customize printing in an interactive session or to install a custom
  top-level exception handler, assign other functions to replace these.

stdin -- standard input file object; used by input()
stdout -- standard output file object; used by print()
stderr -- standard error object; used for error messages
  By assigning other file objects (or objects that behave like files)
  to these, it is possible to redirect all of the interpreter's I/O.

last_type -- type of last uncaught exception
last_value -- value of last uncaught exception
last_traceback -- traceback of last uncaught exception
  These three are only available in an interactive session after a
  traceback has been printed.

Static objects:

builtin_module_names -- tuple of module names built into this interpreter
copyright -- copyright notice pertaining to this interpreter
exec_prefix -- prefix used to find the machine-specific Python library
executable -- absolute path of the executable binary of the Python interpreter
float_info -- a struct sequence with information about the float implementation.
float_repr_style -- string indicating the style of repr() output for floats
hash_info -- a struct sequence with information about the hash algorithm.
hexversion -- version information encoded as a single integer
implementation -- Python implementation information.
int_info -- a struct sequence with information about the int implementation.
maxsize -- the largest supported length of containers.
maxunicode -- the value of the largest Unicode code point
platform -- platform identifier
prefix -- prefix used to find the Python library
thread_info -- a struct sequence with information about the thread implementation.
version -- the version of this interpreter as a string
version_info -- version information as a named tuple
dllhandle -- [Windows only] integer handle of the Python DLL
winver -- [Windows only] version number of the Python DLL
_enablelegacywindowsfsencoding -- [Windows only] 
__stdin__ -- the original stdin; don't touch!
__stdout__ -- the original stdout; don't touch!
__stderr__ -- the original stderr; don't touch!
__displayhook__ -- the original displayhook; don't touch!
__excepthook__ -- the original excepthook; don't touch!

Functions:

displayhook() -- print an object to the screen, and save it in builtins._
excepthook() -- print an exception and its traceback to sys.stderr
exc_info() -- return thread-safe information about the current exception
exit() -- exit the interpreter by raising SystemExit
getdlopenflags() -- returns flags to be used for dlopen() calls
getprofile() -- get the global profiling function
getrefcount() -- return the reference count for an object (plus one :-)
getrecursionlimit() -- return the max recursion depth for the interpreter
getsizeof() -- return the size of an object in bytes
gettrace() -- get the global debug tracing function
setcheckinterval() -- control how often the interpreter checks for events
setdlopenflags() -- set the flags to be used for dlopen() calls
setprofile() -- set the global profiling function
setrecursionlimit() -- set the max recursion depth for the interpreter
settrace() -- set the global debug tracing function
qX   membersq}q(X   versionq}q(X   kindqX   dataqX   valueq	}q
X   typeq]q(X   builtinsqX   strq�qX   __builtin__qX   strq�qesuX   setcheckintervalq}q(hX   functionqh	}q(hX�   setcheckinterval(n)

Tell the Python interpreter to check for asynchronous events every
n instructions.  This also affects how often thread switches occur.qX	   overloadsq]q(}q(X   argsq}qX   nameqX   nqs�qhX�   Tell the Python interpreter to check for asynchronous events every
n instructions.  This also affects how often thread switches occur.q u}q!(X   ret_typeq"]q#hX   NoneTypeq$�q%aX   argsq&}q'(X   typeq(]q)hX   intq*�q+aX   nameq,X   valueq-u�q.ueuuX
   path_hooksq/}q0(hhh	}q1h]q2(hX   listq3�q4hX   listq5�q6esuX   _enablelegacywindowsfsencodingq7}q8(hhh	}q9(hX  _enablelegacywindowsfsencoding()

Changes the default filesystem encoding to mbcs:replace for consistency
with earlier versions of Python. See PEP 529 for more information.

This is equivalent to defining the PYTHONLEGACYWINDOWSFSENCODING 
environment variable before launching Python.q:h]q;}q<(h)hX�   Changes the default filesystem encoding to mbcs:replace for consistency
with earlier versions of Python. See PEP 529 for more information.

This is equivalent to defining the PYTHONLEGACYWINDOWSFSENCODING 
environment variable before launching Python.q=uauuX   dont_write_bytecodeq>}q?(hhh	}q@h]qA(hX   boolqB�qChX   boolqD�qEesuX   setswitchintervalqF}qG(hhh	}qH(hX|  setswitchinterval(n)

Set the ideal thread switching delay inside the Python interpreter
The actual frequency of switching threads can be lower if the
interpreter executes long sequences of uninterruptible code
(this is implementation-specific and workload-dependent).

The parameter must represent the desired switching delay in seconds
A typical value is 0.005 (5 milliseconds).qIh]qJ(}qK(h}qLhhs�qMhXf  Set the ideal thread switching delay inside the Python interpreter
The actual frequency of switching threads can be lower if the
interpreter executes long sequences of uninterruptible code
(this is implementation-specific and workload-dependent).

The parameter must represent the desired switching delay in seconds
A typical value is 0.005 (5 milliseconds).qNu}qO(X   ret_typeqP]qQh%aX   argsqR}qS(X   typeqT]qUhX   floatqV�qWaX   nameqXhu�qYueuuX   getwindowsversionqZ}q[(hhh	}q\(hX�  getwindowsversion()

Return information about the running version of Windows as a named tuple.
The members are named: major, minor, build, platform, service_pack,
service_pack_major, service_pack_minor, suite_mask, and product_type. For
backward compatibility, only the first 5 items are available by indexing.
All elements are numbers, except service_pack and platform_type which are
strings, and platform_version which is a 3-tuple. Platform is always 2.
Product_type may be 1 for a workstation, 2 for a domain controller, 3 for a
server. Platform_version is a 3-tuple containing a version number that is
intended for identifying the OS rather than feature detection.q]h]q^(}q_(h)hX�  Return information about the running version of Windows as a named tuple.
The members are named: major, minor, build, platform, service_pack,
service_pack_major, service_pack_minor, suite_mask, and product_type. For
backward compatibility, only the first 5 items are available by indexing.
All elements are numbers, except service_pack and platform_type which are
strings, and platform_version which is a 3-tuple. Platform is always 2.
Product_type may be 1 for a workstation, 2 for a domain controller, 3 for a
server. Platform_version is a 3-tuple containing a version number that is
intended for identifying the OS rather than feature detection.q`u}qa(h"]qbhX   tupleqc�qdah&)ueuuX   modulesqe}qf(hhh	}qgh]qh(hX   dictqi�qjhX   dictqk�qlesuX	   _getframeqm}qn(hhh	}qo(hX�  _getframe([depth]) -> frameobject

Return a frame object from the call stack.  If optional integer depth is
given, return the frame object that many calls below the top of the stack.
If that is deeper than the call stack, ValueError is raised.  The default
for depth is zero, returning the frame at the top of the call stack.

This function should be used for internal and specialized
purposes only.qph]qq}qr(h}qs(hX   depthqtX   default_valuequX   Noneqvu�qwhXl  Return a frame object from the call stack.  If optional integer depth is
given, return the frame object that many calls below the top of the stack.
If that is deeper than the call stack, ValueError is raised.  The default
for depth is zero, returning the frame at the top of the call stack.

This function should be used for internal and specialized
purposes only.qxX   ret_typeqy]qzX    q{X   frameobjectq|�q}auauuX   _homeq~}q(hhh	}q�h]q�hasuX
   getprofileq�}q�(hhh	}q�(hXt   getprofile()

Return the profiling function set with sys.setprofile.
See the profiler chapter in the library manual.q�h]q�}q�(h)hXf   Return the profiling function set with sys.setprofile.
See the profiler chapter in the library manual.q�uauuX   version_infoq�}q�(hhh	}q�h]q�(X   sysq�X   version_infoq��q�hX   sys.version_infoq��q�esuX   argvq�}q�(hhh	}q�h]q�(h4h6esuX   warnoptionsq�}q�(hhh	}q�h]q�(h4h6esuX   thread_infoq�}q�(hhh	}q�h]q�h�X   thread_infoq��q�asuX   __excepthook__q�}q�(hhh	}q�(hXt   excepthook(exctype, value, traceback) -> None

Handle an exception by displaying it with a traceback on sys.stderr.
q�h]q�(}q�(h}q�hX   exctypeq�s}q�hX   valueq�s}q�hX	   tracebackq�s�q�hXE   Handle an exception by displaying it with a traceback on sys.stderr.
q�hy]q�hX   NoneTypeq��q�au}q�(h"]q�h%ah&}q�(h(]q�hX   objectq��q�ah,X   exctypeq�u}q�(h(]q�h�ah,X   valueq�u}q�(h(]q�h�ah,X	   tracebackq�u�q�ueuuX
   setprofileq�}q�(hhh	}q�(hX�   setprofile(function)

Set the profiling function.  It will be called on each function call
and return.  See the profiler chapter in the library manual.q�h]q�}q�(h}q�hX   functionq�s�q�hX�   Set the profiling function.  It will be called on each function call
and return.  See the profiler chapter in the library manual.q�uauuX   getrefcountq�}q�(hhh	}q�(hX�   getrefcount(object) -> integer

Return the reference count of object.  The count returned is generally
one higher than you might expect, because it includes the (temporary)
reference as an argument to getrefcount().q�h]q�(}q�(h}q�hX   objectq�s�q�hX�   Return the reference count of object.  The count returned is generally
one higher than you might expect, because it includes the (temporary)
reference as an argument to getrefcount().q�hy]q�hX   intqԆq�au}q�(h"]q�h%ah&)ueuuX   getallocatedblocksq�}q�(hhh	}q�(hXr   getallocatedblocks() -> integer

Return the number of memory blocks currently allocated, regardless of their
size.q�h]q�}q�(h)hXQ   Return the number of memory blocks currently allocated, regardless of their
size.q�hy]q�h�auauuX   is_finalizingq�}q�(hhh	}q�(hX1   is_finalizing()
Return True if Python is exiting.q�h]q�}q�(h)hX!   Return True if Python is exiting.q�uauuX   winverq�}q�(hhh	}q�h]q�(hhesuX
   maxunicodeq�}q�(hhh	}q�h]q�(hX   intq�q�h+esuX   _clear_type_cacheq�}q�(hhh	}q�(hXA   _clear_type_cache() -> None
Clear the internal type lookup cache.q�h]q�}q�(h)hX%   Clear the internal type lookup cache.q�hy]q�h�auauuX   _debugmallocstatsq�}q�(hhh	}q�(hX�   _debugmallocstats()

Print summary info to stderr about the state of
pymalloc's structures.

In Py_DEBUG mode, also perform some expensive internal consistency
checks.
q�h]q�}q�(h)hX�   Print summary info to stderr about the state of
pymalloc's structures.

In Py_DEBUG mode, also perform some expensive internal consistency
checks.
q�uauuX   set_asyncgen_hooksr   }r  (hhh	}r  (hXd   set_asyncgen_hooks(*, firstiter=None, finalizer=None)

Set a finalizer for async generators objects.r  h]r  }r  (h}r  (hhX
   arg_formatr  X   *r  u}r	  (hX	   firstiterr
  huX   Noner  u}r  (hX	   finalizerr  huX   Noner  u�r  hX-   Set a finalizer for async generators objects.r  uauuX   __doc__r  }r  (hhh	}r  h]r  (hhesuX
   __loader__r  }r  (hX   typerefr  h	]r  X   _frozen_importlibr  X   BuiltinImporterr  �r  auX   _gitr  }r  (hhh	}r  h]r  hX   tupler   �r!  asuX   getswitchintervalr"  }r#  (hhh	}r$  (hXO   getswitchinterval() -> current thread switch interval; see setswitchinterval().r%  h]r&  (}r'  (h)hX   ().r(  hy]r)  h{X   currentr*  �r+  au}r,  (hP]r-  hWahR)ueuuX	   _xoptionsr.  }r/  (hhh	}r0  h]r1  (hjhlesuX   getrecursionlimitr2  }r3  (hhh	}r4  (hX�   getrecursionlimit()

Return the current value of the recursion limit, the maximum depth
of the Python interpreter stack.  This limit prevents infinite
recursion from causing an overflow of the C stack and crashing Python.r5  h]r6  (}r7  (h)hX�   Return the current value of the recursion limit, the maximum depth
of the Python interpreter stack.  This limit prevents infinite
recursion from causing an overflow of the C stack and crashing Python.r8  u}r9  (h"]r:  h+ah&)ueuuX   getfilesystemencodeerrorsr;  }r<  (hhh	}r=  (hX}   getfilesystemencodeerrors() -> string

Return the error mode used to convert Unicode filenames in
operating system filenames.r>  h]r?  }r@  (h)hXV   Return the error mode used to convert Unicode filenames in
operating system filenames.rA  hy]rB  hX   strrC  �rD  auauuX   stdinrE  }rF  (hhh	}rG  h]rH  (X   _iorI  X   TextIOWrapperrJ  �rK  hX   filerL  �rM  esuX   call_tracingrN  }rO  (hhh	}rP  (hX�   call_tracing(func, args) -> object

Call func(*args), while tracing is enabled.  The tracing state is
saved, and restored afterwards.  This is intended to be called from
a debugger from a checkpoint, to recursively debug some other code.rQ  h]rR  (}rS  (h}rT  hX   funcrU  s}rV  hX   argsrW  s�rX  hX�   Call func(*args), while tracing is enabled.  The tracing state is
saved, and restored afterwards.  This is intended to be called from
a debugger from a checkpoint, to recursively debug some other code.rY  hy]rZ  hX   objectr[  �r\  au}r]  (h"]r^  h%ah&}r_  (h(]r`  h�ah,X   funcra  u}rb  (h(]rc  hdah,X   argsrd  u�re  ueuuX	   copyrightrf  }rg  (hhh	}rh  h]ri  (hhesuX   stderrrj  }rk  (hhh	}rl  h]rm  (jK  jM  esuX   get_asyncgen_hooksrn  }ro  (hhh	}rp  (hXl   get_asyncgen_hooks()

Return a namedtuple of installed asynchronous generators hooks (firstiter, finalizer).rq  h]rr  }rs  (h)hXV   Return a namedtuple of installed asynchronous generators hooks (firstiter, finalizer).rt  uauuX	   meta_pathru  }rv  (hhh	}rw  h]rx  (h4h6esuX   settracery  }rz  (hhh	}r{  (hX�   settrace(function)

Set the global debug tracing function.  It will be called on each
function call.  See the debugger chapter in the library manual.r|  h]r}  (}r~  (h}r  hX   functionr�  s�r�  hX�   Set the global debug tracing function.  It will be called on each
function call.  See the debugger chapter in the library manual.r�  u}r�  (h"]r�  h%ah&}r�  (h(]r�  h�ah,X   or�  u�r�  ueuuX
   __stderr__r�  }r�  (hhh	}r�  h]r�  (jK  jM  esuX	   getsizeofr�  }r�  (hhh	}r�  (hXF   getsizeof(object, default) -> int

Return the size of object in bytes.r�  h]r�  (}r�  (h}r�  hX   objectr�  s}r�  hX   defaultr�  s�r�  hX#   Return the size of object in bytes.r�  hy]r�  h�au}r�  (h"]r�  h+ah&}r�  (h(]r�  h�ah,j�  u�r�  ueuuX   maxsizer�  }r�  (hhh	}r�  h]r�  (h�h+esuX	   callstatsr�  }r�  (hhh	}r�  (hX�  callstats() -> tuple of integers

Return a tuple of function call statistics, if CALL_PROFILE was defined
when Python was built.  Otherwise, return None.

When enabled, this function returns detailed, implementation-specific
details about the number of function calls executed. The return value is
a 11-tuple where the entries in the tuple are counts of:
0. all function calls
1. calls to PyFunction_Type objects
2. PyFunction calls that do not create an argument tuple
3. PyFunction calls that do not create an argument tuple
   and bypass PyEval_EvalCodeEx()
4. PyMethod calls
5. PyMethod calls on bound methods
6. PyType calls
7. PyCFunction calls
8. generator calls
9. All other calls
10. Number of stack pops performed by call_function()r�  h]r�  (}r�  (h)hX�  Return a tuple of function call statistics, if CALL_PROFILE was defined
when Python was built.  Otherwise, return None.

When enabled, this function returns detailed, implementation-specific
details about the number of function calls executed. The return value is
a 11-tuple where the entries in the tuple are counts of:
0. all function calls
1. calls to PyFunction_Type objects
2. PyFunction calls that do not create an argument tuple
3. PyFunction calls that do not create an argument tuple
   and bypass PyEval_EvalCodeEx()
4. PyMethod calls
5. PyMethod calls on bound methods
6. PyType calls
7. PyCFunction calls
8. generator calls
9. All other calls
10. Number of stack pops performed by call_function()r�  hy]r�  hX   tupler�  �r�  au}r�  (h"]r�  h�ah&)ueuuX   getdefaultencodingr�  }r�  (hhh	}r�  (hXo   getdefaultencoding() -> string

Return the current default string encoding used by the Unicode 
implementation.r�  h]r�  (}r�  (h)hXO   Return the current default string encoding used by the Unicode 
implementation.r�  hy]r�  jD  au}r�  (h"]r�  hah&)ueuuX
   hexversionr�  }r�  (hhh	}r�  h]r�  (h�h+esuX
   executabler�  }r�  (hhh	}r�  h]r�  (hhesuX   base_exec_prefixr�  }r�  (hhh	}r�  h]r�  hasuX   api_versionr�  }r�  (hhh	}r�  h]r�  (h�h+esuX
   float_infor�  }r�  (hhh	}r�  h]r�  (h�X
   float_infor�  �r�  X   sysr�  X   sys.float_infor�  �r�  esuX   int_infor�  }r�  (hhh	}r�  h]r�  (h�X   int_infor�  �r�  X   sysr�  X   int_infor�  �r�  esuX   setrecursionlimitr�  }r�  (hhh	}r�  (hX�   setrecursionlimit(n)

Set the maximum depth of the Python interpreter stack to n.  This
limit prevents infinite recursion from causing an overflow of the C
stack and crashing Python.  The highest possible limit is platform-
dependent.r�  h]r�  (}r�  (h}r�  hhs�r�  hX�   Set the maximum depth of the Python interpreter stack to n.  This
limit prevents infinite recursion from causing an overflow of the C
stack and crashing Python.  The highest possible limit is platform-
dependent.r�  u}r�  (h"]r�  h%ah&}r�  (h(]r�  h+ah,X   limitr�  u�r�  ueuuX   path_importer_cacher�  }r�  (hhh	}r�  h]r�  (hjhlesuX   __package__r�  }r�  (hhh	}r�  h]r�  (hh%esuX
   __stdout__r�  }r�  (hhh	}r�  h]r�  (jK  jM  esuX   builtin_module_namesr�  }r�  (hhh	}r�  h]r�  (j!  hdesuX	   hash_infor�  }r�  (hhh	}r�  h]r�  (h�X	   hash_infor�  �r�  X   sysr   X	   hash_infor  �r  esuX   flagsr  }r  (hhh	}r  h]r  (h�X   flagsr  �r  j�  X   flagsr	  �r
  esuX	   __stdin__r  }r  (hhh	}r  h]r  (jK  jM  esuX   gettracer  }r  (hhh	}r  (hX{   gettrace()

Return the global debug tracing function set with sys.settrace.
See the debugger chapter in the library manual.r  h]r  (}r  (h)hXo   Return the global debug tracing function set with sys.settrace.
See the debugger chapter in the library manual.r  u}r  (h"]r  h�ah&)ueuuX   __spec__r  }r  (hhh	}r  h]r  j  X
   ModuleSpecr  �r  asuX   __interactivehook__r  }r  (hhh	}r   hNsuX   _current_framesr!  }r"  (hhh	}r#  (hX�   _current_frames() -> dictionary

Return a dictionary mapping each current thread T's thread id to T's
current stack frame.

This function should be used for specialized purposes only.r$  h]r%  }r&  (h)hX�   Return a dictionary mapping each current thread T's thread id to T's
current stack frame.

This function should be used for specialized purposes only.r'  hy]r(  hX   dictr)  �r*  auauuX   base_prefixr+  }r,  (hhh	}r-  h]r.  hasuX   getfilesystemencodingr/  }r0  (hhh	}r1  (hXw   getfilesystemencoding() -> string

Return the encoding used to convert Unicode filenames in
operating system filenames.r2  h]r3  (}r4  (h)hXT   Return the encoding used to convert Unicode filenames in
operating system filenames.r5  hy]r6  jD  au}r7  (h"]r8  h�ah&)ueuuX   exc_infor9  }r:  (hhh	}r;  (hX�   exc_info() -> (type, value, traceback)

Return information about the most recent exception caught by an except
clause in the current stack frame or in an older stack frame.r<  h]r=  (}r>  (h)hX�   (type, value, traceback)

Return information about the most recent exception caught by an except
clause in the current stack frame or in an older stack frame.r?  hy]r@  h{h{�rA  au}rB  (h"]rC  hdah&)ueuuX
   excepthookrD  }rE  (hhh	}rF  (hXt   excepthook(exctype, value, traceback) -> None

Handle an exception by displaying it with a traceback on sys.stderr.
rG  h]rH  (}rI  (h}rJ  hX   exctyperK  s}rL  hX   valuerM  s}rN  hX	   tracebackrO  s�rP  hXE   Handle an exception by displaying it with a traceback on sys.stderr.
rQ  hy]rR  h�au}rS  (h"]rT  h%ah&}rU  (h(]rV  h�ah,h�u}rW  (h(]rX  h�ah,h�u}rY  (h(]rZ  h�ah,h�u�r[  ueuuX	   dllhandler\  }r]  (hhh	}r^  h]r_  (h�h+esuX   pathr`  }ra  (hhh	}rb  h]rc  (h4h6esuX   implementationrd  }re  (hhh	}rf  h]rg  (X   typesrh  X   SimpleNamespaceri  �rj  hX   sys.implementationrk  �rl  esuX   internrm  }rn  (hhh	}ro  (hX   intern(string) -> string

``Intern'' the given string.  This enters the string in the (global)
table of interned strings whose purpose is to speed up dictionary lookups.
Return the string itself or the previously interned string object with the
same value.rp  h]rq  (}rr  (h}rs  hX   stringrt  s�ru  hX�   Intern'' the given string.  This enters the string in the (global)
table of interned strings whose purpose is to speed up dictionary lookups.
Return the string itself or the previously interned string object with the
same value.rv  hy]rw  h{X
   string

``rx  �ry  au}rz  (hP]r{  hahR}r|  (hT]r}  hahXX   stringr~  u�r  ueuuX   __name__r�  }r�  (hhh	}r�  h]r�  (hhesuX   platformr�  }r�  (hhh	}r�  h]r�  (hhesuX   float_repr_styler�  }r�  (hhh	}r�  h]r�  hasuX   __displayhook__r�  }r�  (hhh	}r�  (hXZ   displayhook(object) -> None

Print an object to sys.stdout and also save it in builtins._
r�  h]r�  (}r�  (h}r�  hX   objectr�  s�r�  hX=   Print an object to sys.stdout and also save it in builtins._
r�  hy]r�  h�au}r�  (h"]r�  h%ah&}r�  (h(]r�  h�ah,X   valuer�  u�r�  ueuuX   prefixr�  }r�  (hhh	}r�  h]r�  (hhesuX   displayhookr�  }r�  (hhh	}r�  (hXZ   displayhook(object) -> None

Print an object to sys.stdout and also save it in builtins._
r�  h]r�  (}r�  (h}r�  hX   objectr�  s�r�  hX=   Print an object to sys.stdout and also save it in builtins._
r�  hy]r�  h�au}r�  (h"]r�  h%ah&}r�  (h(]r�  h�ah,j�  u�r�  ueuuX   get_coroutine_wrapperr�  }r�  (hhh	}r�  (hXc   get_coroutine_wrapper()

Return the wrapper for coroutine objects set by sys.set_coroutine_wrapper.r�  h]r�  }r�  (h)hXJ   Return the wrapper for coroutine objects set by sys.set_coroutine_wrapper.r�  uauuX	   byteorderr�  }r�  (hhh	}r�  h]r�  (hhesuX   stdoutr�  }r�  (hhh	}r�  h]r�  (jK  jM  esuX   exec_prefixr�  }r�  (hhh	}r�  h]r�  (hhesuX   exitr�  }r�  (hhh	}r�  (hX>  exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).r�  h]r�  (}r�  (h}r�  (hX   statusr�  huhvu�r�  hX.  Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).r�  u}r�  (h"]r�  h%ah&)u}r�  (h"]r�  h%ah&}r�  (h(]r�  h�ah,X   coder�  u�r�  ueuuX   getcheckintervalr�  }r�  (hhh	}r�  (hXE   getcheckinterval() -> current check interval; see setcheckinterval().r�  h]r�  (}r�  (h)hX   ().r�  hy]r�  j+  au}r�  (h"]r�  h+ah&)ueuuX   set_coroutine_wrapperr�  }r�  (hhh	}r�  (hXD   set_coroutine_wrapper(wrapper)

Set a wrapper for coroutine objects.r�  h]r�  }r�  (h}r�  hX   wrapperr�  s�r�  hX$   Set a wrapper for coroutine objects.r�  uauuj  }r�  (hhh	}r�  (X   mror�  ]r�  (j  hX   objectr�  �r�  eX   basesr�  ]r�  j�  ahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �h}r�  (X   __eq__r�  }r�  (hX   methodr�  h	}r�  (hX   Return self==value.r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hX   kwargsr�  j  X   **r�  u�r   hX   Return self==value.r  uauuX   __ne__r  }r  (hj�  h	}r  (hX   Return self!=value.r  h]r  }r  (h}r  (hhj  j  u}r	  (hj�  j  j�  u�r
  hX   Return self!=value.r  uauuX   __le__r  }r  (hj�  h	}r  (hX   Return self<=value.r  h]r  }r  (h}r  (hhj  j  u}r  (hj�  j  j�  u�r  hX   Return self<=value.r  uauuX   create_moduler  }r  (hhh	}r  (hX   Create a built-in moduler  h]r  }r  (h}r  (hhj  j  u}r  (hj�  j  j�  u�r  hX   Create a built-in moduler  uauuX   __new__r   }r!  (hhh	}r"  (hXG   Create and return a new object.  See help(type) for accurate signature.r#  h]r$  }r%  (h}r&  (hhj  j  u}r'  (hj�  j  j�  u�r(  hXG   Create and return a new object.  See help(type) for accurate signature.r)  uauuX
   __module__r*  }r+  (hhh	}r,  h]r-  hasuX   __ge__r.  }r/  (hj�  h	}r0  (hX   Return self>=value.r1  h]r2  }r3  (h}r4  (hhj  j  u}r5  (hj�  j  j�  u�r6  hX   Return self>=value.r7  uauuX
   __sizeof__r8  }r9  (hj�  h	}r:  (hX6   __sizeof__() -> int
size of object in memory, in bytesr;  h]r<  }r=  (h}r>  (h]r?  j\  ahX   selfr@  u�rA  hX"   size of object in memory, in bytesrB  hy]rC  h�auauuX   exec_modulerD  }rE  (hhh	}rF  (hX   Exec a built-in modulerG  h]rH  }rI  (h}rJ  (hhj  j  u}rK  (hj�  j  j�  u�rL  hX   Exec a built-in modulerM  uauuX   __hash__rN  }rO  (hj�  h	}rP  (hX   Return hash(self).rQ  h]rR  }rS  (h}rT  (hhj  j  u}rU  (hj�  j  j�  u�rV  hX   Return hash(self).rW  uauuX   __setattr__rX  }rY  (hj�  h	}rZ  (hX%   Implement setattr(self, name, value).r[  h]r\  }r]  (h}r^  (hhj  j  u}r_  (hj�  j  j�  u�r`  hX%   Implement setattr(self, name, value).ra  uauuX   get_coderb  }rc  (hhh	}rd  (hX9   Return None as built-in modules do not have code objects.re  h]rf  }rg  (h}rh  (hhj  j  u}ri  (hj�  j  j�  u�rj  hX9   Return None as built-in modules do not have code objects.rk  uauuX   __dict__rl  }rm  (hhh	}rn  h]ro  hX   mappingproxyrp  �rq  asuj  }rr  (hhh	}rs  h]rt  hasuX   __lt__ru  }rv  (hj�  h	}rw  (hX   Return self<value.rx  h]ry  }rz  (h}r{  (hhj  j  u}r|  (hj�  j  j�  u�r}  hX   Return self<value.r~  uauuX   __init_subclass__r  }r�  (hhh	}r�  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  uauuX   __str__r�  }r�  (hj�  h	}r�  (hX   Return str(self).r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX   Return str(self).r�  uauuX   module_reprr�  }r�  (hhh	}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  uauuX   __dir__r�  }r�  (hj�  h	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h]r�  }r�  (h}r�  (h]r�  j\  ahj@  u�r�  hX   default dir() implementationr�  hy]r�  hX   listr�  �r�  auauuX   __weakref__r�  }r�  (hX   propertyr�  h	}r�  (hX2   list of weak references to the object (if defined)r�  h]r�  j�  auuX   __init__r�  }r�  (hj�  h	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r�  uauuX
   get_sourcer�  }r�  (hhh	}r�  (hX8   Return None as built-in modules do not have source code.r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX8   Return None as built-in modules do not have source code.r�  uauuX   __gt__r�  }r�  (hj�  h	}r�  (hX   Return self>value.r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX   Return self>value.r�  uauuX	   __class__r�  }r�  (hj  h	]r�  hX   typer�  �r�  auX   __repr__r�  }r�  (hj�  h	}r�  (hX   Return repr(self).r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX   Return repr(self).r�  uauuX   __subclasshook__r�  }r�  (hhh	}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  uauuX   __delattr__r�  }r�  (hj�  h	}r�  (hX   Implement delattr(self, name).r�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX   Implement delattr(self, name).r�  uauuX
   __format__r�  }r�  (hj�  h	}r�  (hX   default object formatterr�  h]r�  }r�  (h}r�  (hhj  j  u}r�  (hj�  j  j�  u�r�  hX   default object formatterr�  uauuX
   __reduce__r�  }r�  (hj�  h	}r�  (hX   helper for pickler�  h]r�  }r   (h}r  (hhj  j  u}r  (hj�  j  j�  u�r  hX   helper for pickler  uauuX   __reduce_ex__r  }r  (hj�  h	}r  (hX   helper for pickler  h]r	  }r
  (h}r  (hhj  j  u}r  (hj�  j  j�  u�r  hX   helper for pickler  uauuX	   find_specr  }r  (hhh	}r  h]r  hX   methodr  �r  asuX   find_moduler  }r  (hhh	}r  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  h]r  }r  (h}r  (hhj  j  u}r  (hj�  j  j�  u�r  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  uauuX   load_moduler  }r   (hhh	}r!  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r"  h]r#  }r$  (h}r%  (hhj  j  u}r&  (hj�  j  j�  u�r'  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r(  uauuX
   is_packager)  }r*  (hhh	}r+  (hX4   Return False as built-in modules are never packages.r,  h]r-  }r.  (h}r/  (hhj  j  u}r0  (hj�  j  j�  u�r1  hX4   Return False as built-in modules are never packages.r2  uauuuuuuu.