�}q (X   membersq}q(X	   text_typeq}q(X   kindqX   typerefqX   valueq]qX   builtinsq	X   strq
�qauX   getcharq}q(hX   functionqh}q(X   docqX�  Fetches a single character from the terminal and returns it.  This
    will always return a unicode character and under certain rare
    circumstances this might return more than one character.  The
    situations which more than one character is returned is when for
    whatever reason multiple characters end up in the terminal buffer or
    standard input was not actually a terminal.

    Note that this will always read from the terminal, even if something
    is piped into the standard input.

    Note for Windows: in rare cases when typing non-ASCII characters, this
    function might wait for a second character and then return both at once.
    This is because certain Unicode characters look like special-key markers.

    .. versionadded:: 2.0

    :param echo: if set to `True`, the character read will also show up on
                 the terminal.  The default is to not show it.qX   builtinq�X   locationqM%K�qX	   overloadsq]q}q(X   argsq}q(X   nameqX   echoqX   typeqh	X   boolq�qX   default_valueqX   Falseq u�q!X   ret_typeq"huauuX   _ansi_reset_allq#}q$(hX   dataq%h}q&hhsuX   promptq'}q((hhh}q)(hXe  Prompts a user for input.  This is a convenience function that can
    be used to prompt a user for input later.

    If the user aborts the input by sending a interrupt signal, this
    function will catch it and raise a :exc:`Abort` exception.

    .. versionadded:: 7.0
       Added the show_choices parameter.

    .. versionadded:: 6.0
       Added unicode support for cmd.exe on Windows.

    .. versionadded:: 4.0
       Added the `err` parameter.

    :param text: the text to show for the prompt.
    :param default: the default value to use if no input happens.  If this
                    is not given it will prompt until it's aborted.
    :param hide_input: if this is set to true then the input value will
                       be hidden.
    :param confirmation_prompt: asks for confirmation for the value.
    :param type: the type to use to check the value against.
    :param value_proc: if this parameter is provided it's a function that
                       is invoked instead of the type conversion to
                       convert a value.
    :param prompt_suffix: a suffix that should be added to the prompt.
    :param show_default: shows or hides the default value in the prompt.
    :param err: if set to true the file defaults to ``stderr`` instead of
                ``stdout``, the same as with echo.
    :param show_choices: Show or hide choices if the passed type is a Choice.
                         For example if type is a Choice of either day or week,
                         show_choices is true and text is "Group by" then the
                         prompt will be "Group by (day, week): ".q*h�hK7K�q+h]q,}q-(h(}q.(hX   textq/h]q0(h	X   NoneTypeq1�q2heu}q3(hX   defaultq4h]q5(h	X   tupleq6]q7Na�q8h2ehX   Noneq9u}q:(hX
   hide_inputq;hhhh u}q<(hX   confirmation_promptq=hhhh u}q>(hX   typeq?h]q@(X   click.typesqAX	   ParamTypeqB�qCh	X   objectqD�qEh2ehh9u}qF(hX
   value_procqGh]qH(hChAX   BoolParamTypeqI�qJh	h�qKhAX   FuncParamTypeqL�qMh2hAX   FloatParamTypeqN�qOhAX   TupleqP�qQhAX   StringParamTypeqR�qShAX   PathqT�qUhAX   IntParamTypeqV�qWehh9u}qX(hX   prompt_suffixqYhhhX   ': 'qZu}q[(hX   show_defaultq\hhhX   Trueq]u}q^(hX   errq_hhhh u}q`(hX   show_choicesqahhhh]utqbh"]qc(hKhEh2euauuX   _getcharqd}qe(hh%h}qfhh2suX   _build_promptqg}qh(hhh}qi(hNh�hK.K�qjh]qk}ql(h(}qm(hh/h]qn(h2heu}qo(hX   suffixqphhu}qq(hh\hhhh u}qr(hh4h]qs(h8hhh2ehh9u}qt(hhahhhh]u}qu(hh?h]qv(hAX   Choiceqw�qxhEh2ehh9utqyh"]qz(hheuauuX   get_terminal_sizeq{}q|(hhh}q}(hXj   Returns the current size of the terminal as tuple in the form
    ``(width, height)`` in columns and rows.q~h�hK�K�qh]q�}q�(h)h"]q�(h	h6]q�(h	X   intq��q�h�e�q�h	h6]q�(h�h�e�q�h	h6]q�(NNe�q�h	h6]q�(hEhEe�q�euauuX   string_typesq�}q�(hX   multipleq�h}q�h}q�(hh%h}q�hh	h6]q�(hNe�q�su}q�(hh%h}q�hh	h6]q�ha�q�su�q�suX   sechoq�}q�(hhh}q�(hXn  This function combines :func:`echo` and :func:`style` into one
    call.  As such the following two calls are the same::

        click.secho('Hello World!', fg='green')
        click.echo(click.style('Hello World!', fg='green'))

    All keyword arguments are forwarded to the underlying functions
    depending on which one they go with.

    .. versionadded:: 2.0q�h�hM�K�q�h]q�}q�(h(}q�(hX   messageq�h]q�(h2hehh9u}q�(hX   fileq�hh2hh9u}q�(hX   nlq�hhhh]u}q�(hh_hhhh u}q�(hX   colorq�hh2hh9u}q�(X
   arg_formatq�X   **q�hX   stylesq�hh	X   dictq��q�utq�h"NuauuX   structq�}q�(hX	   modulerefq�hX   structq�X    q��q�uX   Pathq�}q�(hhh]q�hUauX   clearq�}q�(hhh}q�(hX�   Clears the terminal screen.  This will have the effect of clearing
    the whole visible space of the terminal and moving the cursor to the
    top left.  This does not do anything if not connected to a terminal.

    .. versionadded:: 2.0q�h�hMbK�q�h]q�}q�(h)h"NuauuX   visible_prompt_funcq�}q�(hh�h}q�h}q�(hX   funcrefq�h}q�X	   func_nameq�X   click._compat.raw_inputq�su}q�(hh�h}q�h�X   builtins.inputq�su�q�suX   convert_typeq�}q�(hh�h}q�h�X   click.types.convert_typeq�suX	   raw_inputq�}q�(hh�h}q�h}q�(hh�h}q�h�h�su}q�(hh�h}q�h�h�su�q�suX   resolve_color_defaultq�}q�(hh�h}q�h�X#   click.globals.resolve_color_defaultq�suX   styleq�}q�(hhh}q�(hX=  Styles a text with ANSI styles and returns the new string.  By
    default the styling is self contained which means that at the end
    of the string a reset code is issued.  This can be prevented by
    passing ``reset=False``.

    Examples::

        click.echo(click.style('Hello World!', fg='green'))
        click.echo(click.style('ATTENTION!', blink=True))
        click.echo(click.style('Some things', reverse=True, fg='cyan'))

    Supported color names:

    * ``black`` (might be a gray)
    * ``red``
    * ``green``
    * ``yellow`` (might be an orange)
    * ``blue``
    * ``magenta``
    * ``cyan``
    * ``white`` (might be light gray)
    * ``bright_black``
    * ``bright_red``
    * ``bright_green``
    * ``bright_yellow``
    * ``bright_blue``
    * ``bright_magenta``
    * ``bright_cyan``
    * ``bright_white``
    * ``reset`` (reset the color code only)

    .. versionadded:: 2.0

    .. versionadded:: 7.0
       Added support for bright colors.

    :param text: the string to style with ansi codes.
    :param fg: if provided this will become the foreground color.
    :param bg: if provided this will become the background color.
    :param bold: if provided this will enable or disable bold mode.
    :param dim: if provided this will enable or disable dim mode.  This is
                badly supported.
    :param underline: if provided this will enable or disable underline.
    :param blink: if provided this will enable or disable blinking.
    :param reverse: if provided this will enable or disable inverse
                    rendering (foreground becomes background and the
                    other way round).
    :param reset: by default a reset-all code is added at the end of the
                  string which means that styles do not carry over.  This
                  can be disabled to compose styles.q�h�hMtK�q�h]q�}q�(h(}q�(hh/h]q�(h2heu}q�(hX   fgq�h]q�(h2hehh9u}q�(hX   bgq�hh2hh9u}q�(hX   boldq�hh2hh9u}q�(hX   dimq�hh2hh9u}q�(hX	   underlineq�hh2hh9u}q�(hX   blinkq�hh2hh9u}q�(hX   reverseq�hh2hh9u}q�(hX   resetq�hhhh]utq�h"huauuX   _ansi_colorsq�}q�(hh%h}q�hh�suX   Abortq�}q�(hhh]q�X   click.exceptionsq�X   Abortq �r  auX   isattyr  }r  (hh�h}r  h�X   click._compat.isattyr  suX   launchr  }r  (hhh}r  (hXv  This function launches the given URL (or filename) in the default
    viewer application for this file type.  If this is an executable, it
    might launch the executable in a new session.  The return value is
    the exit code of the launched application.  Usually, ``0`` indicates
    success.

    Examples::

        click.launch('https://click.palletsprojects.com/')
        click.launch('/my/downloaded/file', locate=True)

    .. versionadded:: 2.0

    :param url: URL or filename of the thing to launch.
    :param wait: waits for the program to stop.
    :param locate: if this is set to `True` then instead of launching the
                   application associated with the URL it will attempt to
                   launch a file manager with the file located.  This
                   might have weird effects if the URL does not point to
                   the filesystem.r	  h�hMK�r
  h]r  }r  (h}r  (hX   urlr  hNu}r  (hX   waitr  hhhh u}r  (hX   locater  hhhh u�r  h"]r  (h2h�euauuX   unstyler  }r  (hhh}r  (hX	  Removes ANSI styling information from a string.  Usually it's not
    necessary to use this function as Click's echo function will
    automatically remove styling if necessary.

    .. versionadded:: 2.0

    :param text: the text to remove style information from.r  h�hM�K�r  h]r  }r  (h}r  (hh/hNu�r  h"huauuX
   UsageErrorr  }r  (hhh]r   h�X
   UsageErrorr!  �r"  auX   DEFAULT_COLUMNSr#  }r$  (hh�h}r%  h}r&  (hh%h}r'  hh�su}r(  (hh%h}r)  hh�su�r*  suX   hidden_prompt_funcr+  }r,  (hhh}r-  (hNh�hK)K�r.  h]r/  }r0  (h}r1  (hh'hhu�r2  h"]r3  (hhEh2euauuX	   itertoolsr4  }r5  (hh�hX	   itertoolsr6  h��r7  uX   raw_terminalr8  }r9  (hhh}r:  (hNh�hM?K�r;  h]r<  }r=  (h)h"NuauuX   progressbarr>  }r?  (hhh}r@  (hX�  This function creates an iterable context manager that can be used
    to iterate over something while showing a progress bar.  It will
    either iterate over the `iterable` or `length` items (that are counted
    up).  While iteration happens, this function will print a rendered
    progress bar to the given `file` (defaults to stdout) and will attempt
    to calculate remaining time and more.  By default, this progress bar
    will not be rendered if the file is not a terminal.

    The context manager creates the progress bar.  When the context
    manager is entered the progress bar is already displayed.  With every
    iteration over the progress bar, the iterable passed to the bar is
    advanced and the bar is updated.  When the context manager exits,
    a newline is printed and the progress bar is finalized on screen.

    No printing must happen or the progress bar will be unintentionally
    destroyed.

    Example usage::

        with progressbar(items) as bar:
            for item in bar:
                do_something_with(item)

    Alternatively, if no iterable is specified, one can manually update the
    progress bar through the `update()` method instead of directly
    iterating over the progress bar.  The update method accepts the number
    of steps to increment the bar with::

        with progressbar(length=chunks.total_bytes) as bar:
            for chunk in chunks:
                process_chunk(chunk)
                bar.update(chunks.bytes)

    .. versionadded:: 2.0

    .. versionadded:: 4.0
       Added the `color` parameter.  Added a `update` method to the
       progressbar object.

    :param iterable: an iterable to iterate over.  If not provided the length
                     is required.
    :param length: the number of items to iterate over.  By default the
                   progressbar will attempt to ask the iterator about its
                   length, which might or might not work.  If an iterable is
                   also provided this parameter can be used to override the
                   length.  If an iterable is not provided the progress bar
                   will iterate over a range of that length.
    :param label: the label to show next to the progress bar.
    :param show_eta: enables or disables the estimated time display.  This is
                     automatically disabled if the length cannot be
                     determined.
    :param show_percent: enables or disables the percentage display.  The
                         default is `True` if the iterable has a length or
                         `False` if not.
    :param show_pos: enables or disables the absolute position display.  The
                     default is `False`.
    :param item_show_func: a function called with the current item which
                           can return a string to show the current item
                           next to the progress bar.  Note that the current
                           item can be `None`!
    :param fill_char: the character to use to show the filled part of the
                      progress bar.
    :param empty_char: the character to use to show the non-filled part of
                       the progress bar.
    :param bar_template: the format string to use as template for the bar.
                         The parameters in it are ``label`` for the label,
                         ``bar`` for the progress bar and ``info`` for the
                         info section.
    :param info_sep: the separator between multiple info items (eta etc.)
    :param width: the width of the progress bar in characters, 0 means full
                  terminal width
    :param file: the file to write to.  If this is not a terminal then
                 only the label is printed.
    :param color: controls if the terminal supports ANSI colors or not.  The
                  default is autodetection.  This is only needed if ANSI
                  codes are included anywhere in the progress bar output
                  which is not the case by default.rA  h�hMK�rB  h]rC  }rD  (h(}rE  (hX   iterablerF  hh2hh9u}rG  (hX   lengthrH  hh2hh9u}rI  (hX   labelrJ  hh2hh9u}rK  (hX   show_etarL  hhhh]u}rM  (hX   show_percentrN  hh2hh9u}rO  (hX   show_posrP  hhhh u}rQ  (hX   item_show_funcrR  hh2hh9u}rS  (hX	   fill_charrT  hhhX   '#'rU  u}rV  (hX
   empty_charrW  hhhX   '-'rX  u}rY  (hX   bar_templaterZ  hhhX    '%(label)s  [%(bar)s]  %(info)s'r[  u}r\  (hX   info_sepr]  hhhX   '  'r^  u}r_  (hX   widthr`  hh�hX   36ra  u}rb  (hh�hh2hh9u}rc  (hh�hh2hh9utrd  h"X   click._termui_implre  X   ProgressBarrf  �rg  uauuX   echo_via_pagerrh  }ri  (hhh}rj  (hX�  This function takes a text and shows it via an environment specific
    pager on stdout.

    .. versionchanged:: 3.0
       Added the `color` flag.

    :param text_or_generator: the text to page, or alternatively, a
                              generator emitting the text to page.
    :param color: controls if the pager supports ANSI colors or not.  The
                  default is autodetection.rk  h�hK�K�rl  h]rm  }rn  (h}ro  (hX   text_or_generatorrp  hhu}rq  (hh�hh2hh9u�rr  h"NuauuX   sysrs  }rt  (hh�hX   sysru  h��rv  uX   editrw  }rx  (hhh}ry  (hX�  Edits the given text in the defined editor.  If an editor is given
    (should be the full path to the executable but the regular operating
    system search path is used for finding the executable) it overrides
    the detected editor.  Optionally, some environment variables can be
    used.  If the editor is closed without changes, `None` is returned.  In
    case a file is edited directly the return value is always `None` and
    `require_save` and `extension` are ignored.

    If the editor cannot be opened a :exc:`UsageError` is raised.

    Note for Windows: to simplify cross-platform usage, the newlines are
    automatically converted from POSIX to Windows and vice versa.  As such,
    the message here will have ``\n`` as newline markers.

    :param text: the text to edit.
    :param editor: optionally the editor to use.  Defaults to automatic
                   detection.
    :param env: environment variables to forward to the editor.
    :param require_save: if this is true, then not saving in the editor
                         will make the return value become `None`.
    :param extension: the extension to tell the editor about.  This defaults
                      to `.txt` but changing this might change syntax
                      highlighting.
    :param filename: if provided it will edit this file instead of the
                     provided text contents.  It will not use a temporary
                     file as an indirection in that case.rz  h�hM�K�r{  h]r|  }r}  (h(}r~  (hh/hh2hh9u}r  (hX   editorr�  h]r�  (h2je  X   Editorr�  �r�  ehh9u}r�  (hX   envr�  hh2hh9u}r�  (hX   require_saver�  hhhh]u}r�  (hX	   extensionr�  hhhX   '.txt'r�  u}r�  (hX   filenamer�  hh2hh9utr�  h"h2uauuX   inspectr�  }r�  (hh�hX   inspectr�  h��r�  uX
   strip_ansir�  }r�  (hh�h}r�  h�X   click._compat.strip_ansir�  suX   osr�  }r�  (hh�hX   osr�  h��r�  uX   pauser�  }r�  (hhh}r�  (hX�  This command stops execution and waits for the user to press any
    key to continue.  This is similar to the Windows batch "pause"
    command.  If the program is not run through a terminal, this command
    will instead do nothing.

    .. versionadded:: 2.0

    .. versionadded:: 4.0
       Added the `err` parameter.

    :param info: the info string to print before pausing.
    :param err: if set to message goes to ``stderr`` instead of
                ``stdout``, the same as with echo.r�  h�hMDK�r�  h]r�  }r�  (h}r�  (hX   infor�  hhhX   'Press any key to continue ...'r�  u}r�  (hh_hhhh u�r�  h"NuauuX   Choicer�  }r�  (hhh]r�  hxauX   get_winterm_sizer�  }r�  (hh�h}r�  h}r�  (hh�h}r�  h�X   click._compat.get_winterm_sizer�  su}r�  (hh%h}r�  hh2su�r�  suh}r�  (hh�h}r�  h�X   click.utils.echor�  suX   confirmr�  }r�  (hhh}r�  (hX�  Prompts for confirmation (yes/no question).

    If the user aborts the input by sending a interrupt signal this
    function will catch it and raise a :exc:`Abort` exception.

    .. versionadded:: 4.0
       Added the `err` parameter.

    :param text: the question to ask.
    :param default: the default for the prompt.
    :param abort: if this is set to `True` a negative answer aborts the
                  exception by raising :exc:`Abort`.
    :param prompt_suffix: a suffix that should be added to the prompt.
    :param show_default: shows or hides the default value in the prompt.
    :param err: if set to true the file defaults to ``stderr`` instead of
                ``stdout``, the same as with echo.r�  h�hK�K�r�  h]r�  }r�  (h(}r�  (hh/h]r�  (h2heu}r�  (hh4h]r�  (h8hh2ehh u}r�  (hX   abortr�  hhhh u}r�  (hhYhhhX   ': 'r�  u}r�  (hh\hhhh]u}r�  (hh_hhhh utr�  h"]r�  (h8hh2euauuX   WINr�  }r�  (hh%h}r�  hhsuuhh�X   childrenr�  ]r�  X   filenamer�  X\   c:\users\stanislaus\source\repos\konjendemo\konjendemo\env\lib\site-packages\click\termui.pyr�  u.